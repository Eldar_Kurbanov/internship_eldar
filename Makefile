# only for CI
build-all:
	go build $(FLAGS) -o /dev/null ./cmd

test:
	go test $(FLAGS_TEST) -failfast -timeout 10m ./... -v

race:
	go test $(FLAGS_TEST) -race -failfast -timeout 10m ./...

coverage:
	bash ./ci/coverage/coverage.sh

coverage-visual: coverage-container
	go tool cover -html=./ci/coverage/coverage.out

lintci:
	golangci-lint run --config ./.golangci.yml

lintci-deps:
	rm -f $(GOPATH)/bin/golangci-lint
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(GOPATH)/bin v1.32.2

custom-lints:
	bash ./ci/splint.sh

run:
	go run $(FLAGS_TEST) cmd/main.go

tidy-check:
	bash ./ci/tidy.sh

tidy:
	go mod tidy

gitlabci: lintci test race coverage custom-lints tidy
