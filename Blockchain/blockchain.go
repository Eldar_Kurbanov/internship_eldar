package bc

import (
	"context"
	"crypto"
	"crypto/ed25519"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sync"
	"sync/atomic"
	"time"
)

const MSGBusLen = 100

func NewNode(key ed25519.PrivateKey, genesis Genesis) (*Node, error) {
	address, err := PubKeyToAddress(key.Public())
	if err != nil {
		return nil, err
	}

	node := &Node{
		key:             key,
		address:         address,
		genesis:         genesis,
		blocks:          ConcurrentBlockSlice{},
		lastBlockNum:    atomic.Value{},
		peers:           make(map[string]connectedPeer, 0),
		state:           &sync.Map{},
		transactionPool: sync.Map{},
		validators:      make([]ed25519.PublicKey, 0),
		isValidator:     false,
	}
	node.blocks.blocks = make([]Block, 0)
	node.lastBlockNum.Store(uint64(0))
	var weAreValidator bool
	var validatorNumber int
	for number, validatorPubKey := range genesis.Validators {
		node.validators = append(node.validators, validatorPubKey.(ed25519.PublicKey))
		validatorAddress, err := PubKeyToAddress(validatorPubKey)
		if err != nil {
			return nil, err
		}
		if validatorAddress == address {
			weAreValidator = true
			validatorNumber = number
		}
	}
	// Genesis processing
	node.blocks.RWMutex.Lock()
	node.blocks.blocks = append(node.blocks.blocks, genesis.ToBlock())
	node.blocks.RWMutex.Unlock()
	for account, money := range genesis.Alloc {
		node.state.Store(account, money)
	}
	if weAreValidator {
		node.isValidator = true
		go node.mineBlocks(validatorNumber)
	}
	return node, nil
}

func (c *Node) NodeIsValidator() bool {
	return c.isValidator
}

func (c *Node) clearTransactionPool() {
	//erase map
	c.transactionPool.Range(func(key interface{}, value interface{}) bool {
		c.transactionPool.Delete(key)
		return true
	})
}

func (c *Node) mineBlocks(validatorNumber int) {
	for {
		if int(c.lastBlockNum.Load().(uint64))%len(c.validators) == validatorNumber && Length(&c.transactionPool) > 0 {
			stateMap := syncMapToMapStringUint64(c.state)
			stateJson, _ := json.Marshal(stateMap)
			c.stateHash, _ = Hash(stateJson)
			transactions := make([]Transaction, 0)
			c.transactionPool.Range(func(key, value interface{}) bool {
				transactions = append(transactions, value.(Transaction))
				return true
			})
			previousBlock := c.GetBlockByNumber(c.lastBlockNum.Load().(uint64))
			block := Block{
				BlockNum:      c.lastBlockNum.Load().(uint64) + 1,
				Timestamp:     time.Now().UnixNano(),
				Transactions:  transactions,
				PrevBlockHash: previousBlock.BlockHash,
				StateHash:     c.stateHash,
			}
			hash, _ := block.Hash()
			block.BlockHash = hash
			blockJson, err := json.Marshal(block)
			if err != nil {
				log.Println("Error marshal block", err)
				continue
			}
			block.Signature = ed25519.Sign(c.key, blockJson)
			err = c.insertBlock(block)
			if err != nil {
				log.Println("Error insert generated block", err)
				continue
			}
			c.SendMessage(block)

		} else {
			time.Sleep(time.Millisecond * 50)
		}
	}
}

type Node struct {
	key          ed25519.PrivateKey
	address      string
	genesis      Genesis
	lastBlockNum atomic.Value

	//state
	blocks ConcurrentBlockSlice
	//peer address - > peer info
	peers map[string]connectedPeer
	//hash(state) - хеш от упорядоченного слайса ключ-значение
	//state      map[string]uint64
	stateHash  string
	state      *sync.Map
	validators []ed25519.PublicKey

	//transaction hash - > transaction
	transactionPoolMutex sync.RWMutex
	transactionPool      sync.Map

	isValidator bool
}

func (c *Node) NodeKey() crypto.PublicKey {
	return c.key.Public()
}

func (c *Node) Connection(address string, in chan Message, out chan Message) chan Message {
	if out == nil {
		out = make(chan Message, MSGBusLen)
	}
	ctx, cancel := context.WithCancel(context.Background())
	c.peers[address] = connectedPeer{
		Address: address,
		Out:     out,
		In:      in,
		cancel:  cancel,
	}

	go c.peerLoop(ctx, c.peers[address])
	return c.peers[address].Out
}

func (c *Node) checkConnected(remoteAddress string) bool {
	_, ok := c.peers[remoteAddress]
	return ok
}

func (c *Node) AddPeer(peer Blockchain) error {
	remoteAddress, err := PubKeyToAddress(peer.NodeKey())
	if err != nil {
		return err
	}

	if c.address == remoteAddress {
		return errors.New("self connection")
	}
	if c.checkConnected(remoteAddress) {
		return nil
	}
	out := make(chan Message, MSGBusLen)
	in := peer.Connection(c.address, out, nil)
	c.Connection(remoteAddress, in, out)
	return nil
}

func (c *Node) peerLoop(ctx context.Context, peer connectedPeer) {
	peer.Send(ctx, Message{
		From: c.address,
		Data: c.NodeInfo(),
	})
	for {
		select {
		case <-ctx.Done():
			c.SendMessage(MessageNodeShutdown)
			return
		case msg := <-peer.In:
			needToBroadcast, err := c.processMessage(peer.Address, msg)
			if err != nil {
				log.Println("Process peer error", err)
				continue
			}

			//broadcast to connected peers
			if needToBroadcast {
				c.Broadcast(ctx, msg)
			}
		}
	}
}

func printTransactionPool(transactions *sync.Map) string {
	str := ""
	transactions.Range(func(key, value interface{}) bool {
		str += fmt.Sprintf("%s, ", key.(string))
		return true
	})
	return str
}

func (c *Node) processMessage(address string, msg Message) (bool, error) {
	switch m := msg.Data.(type) {
	//example
	case NodeInfoResp:
		needSync := c.lastBlockNum.Load().(uint64) < m.BlockNum
		fmt.Println(c.address, "connected to ", address, "need sync", needSync)
		if needSync {
			c.SendMessage(TextMessage{MessageGiveMeYourBlocks})
		}
		return false, nil
	case TextMessage:
		switch m.message {
		case MessageGiveMeYourBlocks:
			c.blocks.RWMutex.RLock()
			c.SendMessage(c.blocks.blocks)
			c.blocks.RWMutex.RUnlock()
			return false, nil
		case MessageNodeShutdown:
			delete(c.peers, address)
			return false, nil
		default:
			fmt.Println("unknown message")
			return false, errors.New(ErrorUnknownTextMessage)
		}
	case []Block:
		for _, block := range m {
			err := c.insertBlock(block)
			if err != nil {
				if err.Error() == ErrorWrongNumberOfInsertBlock {
					continue
				} else {
					return false, err
				}
			}
		}
		c.blocks.RWMutex.Lock()
		c.blocks.blocks = m
		c.blocks.RWMutex.Unlock()
		fmt.Println(c.address, "accept the blocks from", address)
		return false, nil
	case Transaction:
		//log.Println("get transaction", c.address, m.From)
		err := c.AddTransaction(m)
		if err != nil {
			//log.Println(err)
			//transactionHash, _ := m.Hash()
			//log.Println(c.address, printTransactionPool(&c.transactionPool), transactionHash)
			if err.Error() == ErrorTransactionAlreadyExists {
				return false, nil
			} else {
				return false, err
			}
		} else {
			fmt.Println(c.address, "got transaction from", address)
			return true, nil
		}
	case Block:
		err := c.insertBlock(m)
		if err != nil {
			if err.Error() == ErrorWrongNumberOfInsertBlock {
				return false, nil
			} else {
				return false, err
			}
		} else {
			fmt.Println(c.address, "got block", m.BlockNum, "from", address)
			return true, nil
		}
	default:
		fmt.Println("unknown message")
		return false, errors.New(ErrorUnknownMessageType)
	}
}

func (c *Node) Broadcast(ctx context.Context, msg Message) {
	for _, v := range c.peers {
		if v.Address != c.address {
			v.Send(ctx, msg)
		}
	}
}

func (c *Node) RemovePeer(peer Blockchain) error {
	if peer.NodeIsValidator() {
		return errors.New(ErrorCantDeleteValidator)
	} else {
		remoteAddress, err := PubKeyToAddress(peer.NodeKey())
		if err != nil {
			return err
		}

		if c.address == remoteAddress {
			return errors.New("self delete")
		}
		if !c.checkConnected(remoteAddress) {
			return nil
		}
		c.peers[remoteAddress].cancel()
		return nil
	}
}

func (c *Node) GetBalance(account string) (uint64, error) {
	balance, isAccountExists := c.state.Load(account)
	if !isAccountExists {
		return 0, errors.New(ErrorAccountDoesntExists)
	}
	return balance.(uint64), nil
}

func (c *Node) AddTransaction(transaction Transaction) error {
	c.transactionPoolMutex.Lock()
	defer c.transactionPoolMutex.Unlock()
	transactionHash, err := transaction.Hash()
	if err != nil {
		return err
	}
	_, transactionAlreadyExists := c.transactionPool.Load(transactionHash)
	if transactionAlreadyExists {
		return errors.New(ErrorTransactionAlreadyExists)
	}
	c.transactionPool.Store(transactionHash, transaction)
	c.SendMessage(transaction)
	return nil
}

func (c *Node) SendMessage(message interface{}) {
	ctx := context.Background()
	c.Broadcast(ctx, Message{
		c.address,
		message,
	})
}

func (c *Node) GetBlockByNumber(ID uint64) Block {
	c.blocks.RLock()
	defer func() {
		c.blocks.RUnlock()
	}()
	return c.blocks.blocks[ID]
}

func (c *Node) NodeInfo() NodeInfoResp {
	return NodeInfoResp{
		NodeName: c.address,
		BlockNum: c.lastBlockNum.Load().(uint64),
	}
}

func (c *Node) NodeAddress() string {
	return c.address
}

func (c *Node) SignTransaction(transaction Transaction) (Transaction, error) {
	b, err := transaction.Bytes()
	if err != nil {
		return Transaction{}, err
	}

	transaction.Signature = ed25519.Sign(c.key, b)
	return transaction, nil
}

func (c *Node) insertBlock(b Block) (err error) {
	c.transactionPoolMutex.Lock()
	defer c.transactionPoolMutex.Unlock()
	if b.BlockNum != c.lastBlockNum.Load().(uint64)+1 {
		return errors.New(ErrorWrongNumberOfInsertBlock)
	}
	validatorID := int(b.BlockNum-1) % len(c.validators)
	validatorPublicKey := c.genesis.Validators[validatorID]
	validatorAddress, err := PubKeyToAddress(validatorPublicKey)
	if err != nil {
		return errors.New(ErrorCantCastPubKeyToAddress)
	}

	blockJson, err := json.Marshal(b)
	if err != nil {
		return err
	}

	isBlockVerified := ed25519.Verify(validatorPublicKey.(ed25519.PublicKey), blockJson, b.Signature)
	if !isBlockVerified {
		return errors.New(ErrorBlockNotVerified)
	}

	copyState := CopySyncMap(c.state)
	transactionHashes := make([]string, 0)
	defer func() {
		if err != nil {
			c.state = copyState
		}
	}()
	for _, t := range b.Transactions {
		total := t.Fee + t.Amount
		balanceFrom, senderExists := c.state.Load(t.From)
		if !senderExists {
			err = errors.New(ErrorSenderDoesntExists)
			return
		}
		if total > balanceFrom.(uint64) {
			err = errors.New(ErrorOutOfMoney)
			return
		}
		c.state.Store(t.From, balanceFrom.(uint64)-total)
		balanceTo, recipientExists := c.state.Load(t.To)
		if !recipientExists {
			err = errors.New(ErrorRecipientDoesntExists)
			return
		}
		c.state.Store(t.To, balanceTo.(uint64)+t.Amount)
		balanceValidator, validatorExists := c.state.Load(validatorAddress)
		if !validatorExists {
			err = errors.New(ErrorValidatorDoesntExists)
			return
		}
		c.state.Store(validatorAddress, balanceValidator.(uint64)+t.Fee)
		transactionHash, err2 := t.Hash()
		if err2 != nil {
			return err2
		}
		transactionHashes = append(transactionHashes, transactionHash)
	}
	for _, transactionHash := range transactionHashes {
		c.transactionPool.Delete(transactionHash)
	}
	c.blocks.RWMutex.Lock()
	c.blocks.blocks = append(c.blocks.blocks, b)
	c.blocks.RWMutex.Unlock()
	c.lastBlockNum.Store(c.lastBlockNum.Load().(uint64) + 1)
	return nil
}
