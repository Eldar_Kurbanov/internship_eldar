package bc

import (
	"bytes"
	"context"
	"crypto"
	"crypto/ed25519"
	"encoding/gob"
	"encoding/json"
	"errors"
	"log"
	"sort"
	"sync"
	"time"
)

type Block struct {
	BlockNum      uint64
	Timestamp     int64
	Transactions  []Transaction
	BlockHash     string `json:"-"`
	PrevBlockHash string
	StateHash     string
	Signature     []byte `json:"-"`
}

func (bl *Block) Hash() (string, error) {
	if bl == nil {
		return "", errors.New("empty block")
	}
	b, err := Bytes(bl)
	if err != nil {
		return "", err
	}
	return Hash(b)
}

type Transaction struct {
	From   string
	To     string
	Amount uint64
	Fee    uint64
	PubKey ed25519.PublicKey

	Signature []byte `json:"-"`
}

func (t Transaction) Hash() (string, error) {
	b, err := Bytes(t)
	if err != nil {
		return "", err
	}
	return Hash(b)
}

func (t Transaction) Bytes() ([]byte, error) {
	b := bytes.NewBuffer(nil)
	err := gob.NewEncoder(b).Encode(t)
	if err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}

// first block with blockchain settings
type Genesis struct {
	//Account -> funds
	Alloc map[string]uint64
	//list of validators public keys
	Validators []crypto.PublicKey
}

func (g Genesis) ToBlock() Block {
	location, _ := time.LoadLocation("Europe/Volgograd")

	// Это имелось ввиду под алфавитным порядком genesis.Alloc?
	keys := make([]string, 0, len(g.Alloc))
	for key := range g.Alloc {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	sortedAlloc := make(map[string]uint64)
	for _, key := range keys {
		sortedAlloc[key] = g.Alloc[key]
	}
	g.Alloc = sortedAlloc

	stateJson, _ := json.Marshal(g.Alloc)
	hashStateJson, _ := Hash(stateJson)
	block := Block{
		BlockNum:      0,
		Timestamp:     time.Date(2020, 9, 1, 0, 0, 0, 0, location).UnixNano(),
		Transactions:  make([]Transaction, 0),
		PrevBlockHash: "",
		StateHash:     hashStateJson,
	}
	hash, _ := block.Hash()
	block.BlockHash = hash
	return block
}

type Message struct {
	From string
	Data interface{}
}

type NodeInfoResp struct {
	NodeName string
	BlockNum uint64
}

type TextMessage struct {
	message string
}

type ConcurrentBlockSlice struct {
	sync.RWMutex
	blocks []Block
}

type connectedPeer struct {
	Address string
	In      chan Message
	Out     chan Message
	cancel  context.CancelFunc
}

func (cp connectedPeer) Send(ctx context.Context, m Message) {
	select {
	case cp.Out <- m:
		return
	case <-ctx.Done():
		return
	case <-time.After(TimeoutSend * time.Second):
		log.Println("Timeout send message to", cp.Address)
		return
	}
	//cp.Out <- m
}

const (
	TimeoutSend = 5 // 5 seconds

	MessageGiveMeYourBlocks = "GiveMeYourBlocks"
	MessageNodeShutdown     = "NodeShutdown"

	ErrorTransactionAlreadyExists = "TransactionAlreadyExists"
	ErrorAccountDoesntExists      = "AccountDoesntExists"
	ErrorUnknownMessageType       = "UnknownMessageType"
	ErrorOutOfMoney               = "OutOfMoney"
	ErrorBlockNotVerified         = "BlockNotVerified"
	ErrorCantCastPubKeyToAddress  = "CantCastPubKeyToAddress"
	ErrorWrongNumberOfInsertBlock = "WrongNumberOfInsertBlock"
	ErrorUnknownTextMessage       = "UnknownTextMessage"
	ErrorCantDeleteValidator      = "CantDeleteValidator"
	ErrorSenderDoesntExists       = "SenderDoesntExists"
	ErrorRecipientDoesntExists    = "RecipientDoesntExists"
	ErrorValidatorDoesntExists    = "ValidatorDoesntExists"
)
