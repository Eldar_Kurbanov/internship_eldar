package bc

import (
	"crypto"
	"crypto/ed25519"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"sync"
)

func PubKeyToAddress(key crypto.PublicKey) (string, error) {
	if v, ok := key.(ed25519.PublicKey); ok {
		b := sha256.Sum256(v)
		return hex.EncodeToString(b[:]), nil
	}
	return "", errors.New("incorrect key")
}

func Hash(b []byte) (string, error) {
	hash := sha256.Sum256(b)
	return hex.EncodeToString(hash[:]), nil
}

func Bytes(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func Length(syncMap *sync.Map) int {
	length := 0
	syncMap.Range(func(_, _ interface{}) bool {
		length++

		return true
	})
	return length
}

func syncMapToMapStringUint64(syncMap *sync.Map) map[string]uint64 {
	resultMap := make(map[string]uint64)
	syncMap.Range(func(key, value interface{}) bool {
		resultMap[key.(string)] = value.(uint64)
		return true
	})
	return resultMap
}

func mapStringUint64ToSyncMap(m map[string]uint64) *sync.Map {
	resultSyncMap := sync.Map{}
	for key, value := range m {
		resultSyncMap.Store(key, value)
	}
	return &resultSyncMap
}

func CopySyncMap(m *sync.Map) *sync.Map {
	var cp sync.Map

	m.Range(func(k, v interface{}) bool {
		vm, ok := v.(sync.Map)
		if ok {
			cp.Store(k, CopySyncMap(&vm))
		} else {
			cp.Store(k, v)
		}

		return true
	})

	return &cp
}
