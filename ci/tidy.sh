#!/bin/bash

tidyOutput=$(go mod tidy -v 2>&1)
if [[ $tidyOutput == *"unused"* ]];
then
      echo "go.mod needs tidy"
      exit 1;
else
      exit 0;
fi