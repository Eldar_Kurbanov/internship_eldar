#!/bin/sh

MIN_COVERAGE=50.0

# Recursively check directories and
# create an empty test file in packages
# where tests are missing
PACKAGES=$(find $(pwd) -type d | grep -v "\.$(find $(pwd) -type f -name '*_test.go' -printf '\\|^%h$')" | grep -e "$(find $(pwd) -type f -name '*.go' -printf '^%h$\\|') | rev | cut -c 2- | rev")
for pkg in ${PACKAGES[@]}
do
  pkg_name="$(grep -h '^package .*' `find $pkg -maxdepth 1 -type f` | head -1)"
  echo $pkg_name > "$pkg/empty_test.go"
done

# Calculate coverage
go test -ldflags="${LDFLAGS_TEST}" -short -coverprofile=./ci/coverage/coverage.out.tmp -covermode atomic ./...
cat ./ci/coverage/coverage.out.tmp | grep -v "_generated.go\|pb.go\|e2e" > ./ci/coverage/coverage.out
COVER_PERCENT=$(go tool cover -func ./ci/coverage/coverage.out | grep total: | awk '{print substr($3, 1, length($3)-1)}')
$GOPATH/bin/gocover-cobertura < ./ci/coverage/coverage.out > ./ci/coverage/coverage.xml

# Remove generated test files
for pkg in ${PACKAGES[@]}
do
  rm "$pkg/empty_test.go"
done
if (( $(echo "$COVER_PERCENT < $MIN_COVERAGE" |bc -l) )); then
  echo "Coverage $COVER_PERCENT% less min percent $MIN_COVERAGE%";
  exit 1;
else
  echo "Coverage $COVER_PERCENT% successful";
fi