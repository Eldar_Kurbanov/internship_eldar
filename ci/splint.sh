#!/bin/bash
OUTPUT=""
FALSE="false"
function ignore_files
{
IGNORE_LIST=()
FILE=$1

GENERATED=$(grep -h -r "Code generated" $FILE)
if [[ -n $GENERATED ]]; then
     return
fi

for IGNORE in "${IGNORE_LIST[@]}"
do
   if [[ ${FILE} == *"$IGNORE"* ]]; then
     return
   fi
done
echo FALSE
}

function ignore_funcs
{
IGNORE_LIST=("New")
FUNC=$@
for IGNORE in ${IGNORE_LIST[@]}
do
   if [[ ${FUNC} == *"$IGNORE"* ]]; then
     return
   fi
done
echo FALSE
}

for file in `find ./ -type f -name "*.go" -not -path '*/\.*'`
  do
     ignore_file=$(ignore_files ${file})
     if [[ $ignore_file == FALSE ]]; then
       FUNC=$(splint -s=500 -p=6 -r=3 -b  ${file})
       ignore_func=$(ignore_funcs ${FUNC})
       if [[ -n $FUNC ]] && [[ $ignore_func == FALSE ]]; then
        echo "${FUNC}"
        OUTPUT+="${FUNC}"
       fi
     fi
  done

if [[ -n $OUTPUT ]];then
  exit 2
fi
