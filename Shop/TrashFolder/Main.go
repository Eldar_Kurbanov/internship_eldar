package main

import (
	"errors"
	"fmt"
	"github.com/rkusa/gm/math32"
	"log"
	"sort"
)

// Вспомогательные функции
func addProduct(products *map[string]float32, name string, price float32) {
	log.Println(fmt.Sprintf("В базу товаров добавлен товар %q с ценой %v", name, price))
	(*products)[name] = price
}

// Задание 1: Пользователь даёт список заказа, программа должна посчитать наименованиями товаров и ценами, посчитать сумму заказа
func calculateSumOrder(products map[string]float32, order []string) float32 {
	var sum float32
	for i := range order {
		sum += products[order[i]]
	}
	log.Println(fmt.Sprintf("Подсчитан заказ с товарами %v на сумму %v", order, sum))
	return sum
}

// Задание 2: И сделать метод добавления, обновления и удаления товаров
func removeProduct(products *map[string]float32, name string) {
	delete(*products, name)
	log.Println(fmt.Sprintf("Из базы удалён товар %q", name))
}

func changePriceProduct(products *map[string]float32, name string, newPrice float32) {
	log.Println(fmt.Sprintf("Изменена цена продукта %q с %v на %v", name, (*products)[name], newPrice))
	(*products)[name] = newPrice
}

func changeNameProduct(products *map[string]float32, oldName string, newName string) {
	price := (*products)[oldName]
	delete(*products, oldName)
	(*products)[newName] = price
	log.Println(fmt.Sprintf("Изменено имя продукта %q на %q", oldName, newName))
}

// Задание 3: Пользовательские аккаунты со счетом типа "вася: 300р, петя: 30000000р". Нужно уметь регистрировать аккаунты, добавлять к ним деньги и смотреть баланс
func registerAccount(accounts *map[string]float32, name string) {
	(*accounts)[name] = 0
	log.Println(fmt.Sprintf("Зарегистрирован аккаунт %q.", name))
}

func addMoney(accounts *map[string]float32, name string, addableMoney float32) {
	(*accounts)[name] += addableMoney
	log.Println(fmt.Sprintf("К аккаунту %q добавлено %v денег.", name, addableMoney))
}

func lookupBalance(accounts map[string]float32, name string) float32 {
	log.Println(fmt.Sprintf("Запрошен баланс аккаунта %q. Он равен %v денег.", name, accounts[name]))
	return accounts[name]
}

// Задание 4: Сделать процедуру покупки. Если есть ли деньги у пользователя, то списать сумму заказа, если нет - вернуть ошибку
const float32EqualityThreshold = 1e-6

func almostEqual(a, b float32) bool {
	return math32.Abs(a-b) <= float32EqualityThreshold
}

func buyOrder(products map[string]float32, accounts *map[string]float32, order []string, nameUser string) error {
	log.Println(fmt.Sprintf("Обрабатывается заказ пользователя %q со следующими товарами: %v", nameUser, order))
	var sumOrder float32
	for i := range order {
		sumOrder += products[order[i]]
	}
	userBalance := (*accounts)[nameUser]
	if almostEqual(userBalance, sumOrder) {
		// Если сумма покупки равна балансу пользователя, то обнуляем
		(*accounts)[nameUser] = 0
	} else if userBalance > sumOrder {
		(*accounts)[nameUser] -= sumOrder
	} else {
		return errors.New(fmt.Sprintf("Заказ отклонён! На счету пользователя %q находится %v денег. Для заказа требовалось %v денег.", nameUser, userBalance, sumOrder))
	}
	log.Println(fmt.Sprintf("Заказ на сумму %v выполнен! На счету пользователя %q осталось %v денег.", sumOrder, nameUser, (*accounts)[nameUser]))
	return nil
}

// Задание 5: Надо вывести аккаунты в отсортированном по: имени в алфавитном порядке, по имени в обратном порядке, по количеству денег по убыванию
type Pair struct {
	Key   string
	Value float32
}

type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Конец заимствования из https://stackoverflow.com/a/18695740

func sortMapTask5(sortableMap map[string]float32) {
	keysProducts := make([]string, 0)
	for k, _ := range sortableMap {
		keysProducts = append(keysProducts, k)
	}
	sort.Strings(keysProducts)
	fmt.Println("По имени в алфавитном порядке:")
	for i := range keysProducts {
		fmt.Println(keysProducts[i], sortableMap[keysProducts[i]])
	}
	fmt.Println("По имени в обратном порядке:")
	sort.Sort(sort.Reverse(sort.StringSlice(keysProducts)))
	for i := range keysProducts {
		fmt.Println(keysProducts[i], sortableMap[keysProducts[i]])
	}
	pairList := make(PairList, len(sortableMap))
	i := 0
	for key, value := range sortableMap {
		pairList[i] = Pair{key, value}
		i++
	}
	sort.Sort(sort.Reverse(pairList))
	fmt.Println("По количеству денег по убыванию:")
	fmt.Println(pairList)
}

// Задание 6: Премиум товары и пользователи
type void struct{}

var member void

func calculatePremiumMarkup(products map[string]float32, premiumProducts map[string]void, premiumUsers map[string]void, nameUser string, nameProduct string) float32 {
	_, isUserPremium := premiumUsers[nameUser]
	_, isProductPremium := premiumProducts[nameProduct]
	if isUserPremium {
		if isProductPremium {
			return products[nameProduct] * (-0.2)
		}
		return products[nameProduct] * 0.5
	}
	if isProductPremium {
		return products[nameProduct] * (-0.05)
	}
	return 0
}

func buyOrderWithPremium(products map[string]float32, accounts *map[string]float32, premiumProducts map[string]void, premiumUsers map[string]void, order []string, nameUser string) error {
	log.Println(fmt.Sprintf("Обрабатывается заказ пользователя %q c учётом премиум наценок и скидок со следующими товарами: %v", nameUser, order))
	var sumOrder float32
	for i := range order {
		premiumMarkup := calculatePremiumMarkup(products, premiumProducts, premiumUsers, nameUser, order[i])
		log.Println(fmt.Sprintf("Премиум наценка / скидка для товара %q составила %v", order[i], premiumMarkup))
		sumOrder += products[order[i]] + premiumMarkup
	}
	userBalance := (*accounts)[nameUser]
	if almostEqual(userBalance, sumOrder) {
		// Если сумма покупки равна балансу пользователя, то обнуляем
		(*accounts)[nameUser] = 0
	} else if userBalance > sumOrder {
		(*accounts)[nameUser] -= sumOrder
	} else {
		return errors.New(fmt.Sprintf("Заказ отклонён! На счету пользователя %q находится %v денег. Для заказа требовалось %v денег.", nameUser, userBalance, sumOrder))
	}
	log.Println(fmt.Sprintf("Заказ на сумму %v выполнен! На счету пользователя %q осталось %v денег.", sumOrder, nameUser, (*accounts)[nameUser]))
	return nil
}

func main() {
	// Задание 1: Пользователь даёт список заказа, программа должна посчитать наименованиями товаров и ценами, посчитать сумму заказа:
	products := make(map[string]float32)
	fmt.Println("Задание 1: Пользователь даёт список заказа, программа должна посчитать наименованиями товаров и ценами, посчитать сумму заказа:")
	fmt.Println("Добавим немного товаров в базу товаров...")
	addProduct(&products, "Кукуруза вареная в/у, 450 г", 59.99)
	addProduct(&products, "Баклажаны грунтовые, 1 кг", 49.89)
	addProduct(&products, "Арбуз Чёрный Принц, 1 кг", 49.99)
	addProduct(&products, "Фасоль стручковая резаная зам, 1 кг Импорт", 103.19)
	fmt.Println()

	fmt.Println("Соберём и подсчитаем заказ:")
	order := []string{"Кукуруза вареная в/у, 450 г", "Кукуруза вареная в/у, 450 г", "Баклажаны грунтовые, 1 кг"}
	calculateSumOrder(products, order)
	fmt.Println()

	fmt.Println("Задание 2: И сделать метод добавления, обновления и удаления товаров:")
	changeNameProduct(&products, "Фасоль стручковая резаная зам, 1 кг Импорт", "Фасоль стручковая резаная зам, 1 кг Российская")
	changePriceProduct(&products, "Фасоль стручковая резаная зам, 1 кг Российская", 99.99)
	removeProduct(&products, "Фасоль стручковая резаная зам, 1 кг Российская")
	addProduct(&products, "Фасоль стручковая резаная зам, 1 кг Импорт", 103.19)
	fmt.Println(products)
	fmt.Println()

	fmt.Println("Задание 3: Пользовательские аккаунты со счетом типа \"вася: 300р, петя: 30000000р\". Нужно уметь регистрировать аккаунты, добавлять к ним деньги и смотреть баланс:")
	accounts := make(map[string]float32)
	registerAccount(&accounts, "Вася")
	registerAccount(&accounts, "Петя")
	addMoney(&accounts, "Вася", 300)
	addMoney(&accounts, "Петя", 30000000)
	lookupBalance(accounts, "Вася")
	fmt.Println()

	fmt.Println("Задание 4: Сделать процедуру покупки. Если есть ли деньги у пользователя, то списать сумму заказа, если нет - вернуть ошибку:")
	orderVasya := []string{"Фасоль стручковая резаная зам, 1 кг Импорт", "Баклажаны грунтовые, 1 кг", "Баклажаны грунтовые, 1 кг", "Фасоль стручковая резаная зам, 1 кг Импорт"}
	err := buyOrder(products, &accounts, orderVasya, "Вася")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Вася решил отказаться от одной банки фасоли...")
	orderVasya = []string{"Фасоль стручковая резаная зам, 1 кг Импорт", "Баклажаны грунтовые, 1 кг", "Баклажаны грунтовые, 1 кг"}
	err = buyOrder(products, &accounts, orderVasya, "Вася")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()

	fmt.Println("Задание 5: Надо вывести аккаунты в отсортированном по: имени в алфавитном порядке, по имени в обратном порядке, по количеству денег по убыванию:")
	sortMapTask5(accounts)
	fmt.Println()

	fmt.Println("Задание 6: Премиум товары и пользователи:")
	premiumProducts := make(map[string]void)
	premiumProducts["Фасоль стручковая резаная зам, 1 кг Импорт"] = member
	fmt.Println("Фасоль стручковая резаная зам, 1 кг Импорт стала премиум товаром!")
	premiumProducts["Кукуруза вареная в/у, 450 г"] = member
	fmt.Println("Кукуруза вареная в/у, 450 г стала премиум товаром!")

	premiumUsers := make(map[string]void)
	premiumUsers["Петя"] = member
	fmt.Println("Петя стал премиум пользователем!")
	fmt.Println()

	fmt.Println("Петя решил отовариться...")
	orderPetya := []string{"Арбуз Чёрный Принц, 1 кг", "Арбуз Чёрный Принц, 1 кг", "Фасоль стручковая резаная зам, 1 кг Импорт"}
	err = buyOrderWithPremium(products, &accounts, premiumProducts, premiumUsers, orderPetya, "Петя")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()
	fmt.Println("Вася хоть и не премиум пользователь, но своей скидкой 5% на премиум товар он воспользуется с радостью!")
	orderVasya = []string{"Кукуруза вареная в/у, 450 г"}
	err = buyOrderWithPremium(products, &accounts, premiumProducts, premiumUsers, orderVasya, "Вася")
}
