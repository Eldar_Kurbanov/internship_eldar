package main

import (
	"errors"
	"fmt"
	"log"
	"sort"
)

// No used
type Orders map[string][]*Order

func (shopVault ShopVault) PlaceOrder(username string, order Order) error {
	sumOrder, err := shopVault.CalculateOrder(username, order)
	if err != nil {
		// Logs will be printed in the callable function
		return err
	}

	// Check if account exists appears in CalculateOrder
	account := shopVault.SVaccounts[username]

	if almostEqual(sumOrder, account.Balance) {
		account.Balance = 0
	} else if account.Balance > sumOrder {
		account.Balance -= sumOrder
	} else {
		log.Printf("Произошла ошибка при списании суммы заказа! На аккаунте %q не хватает денежных средств. Для покупки требовалось %v, а на счету пользователя оказалось %v", account.Name, sumOrder, account.Balance)
		return errors.New("AccountNotEnoughMoney")
	}
	log.Printf("Списание суммы заказа равное %v у пользователя %q произошло успешно. На счету пользователя осталось %v денег", sumOrder, account.Name, account.Balance)
	return nil
}

func (shopVault ShopVault) CalculateOrder(username string, order Order) (float32, error) {
	// Check the input
	if len(order.Bundles) == 0 && len(order.Products) == 0 {
		log.Printf("Пользователь %q сделал попытку подсчёта пустого заказа %v!", username, order)
		return 0, errors.New("EmptyOrder")
	}

	account, isAccountExists := shopVault.SVaccounts[username]
	if !isAccountExists {
		log.Printf("Произошла ошибка при подсчёте суммы заказа! Аккаунта %q не существует", username)
		return 0, errors.New("AccountNotExists")
	}

	keyOrder := generateKeyOrder(*account, order)
	cachedSum, isCachedSumExists := shopVault.SVordersCache[keyOrder]
	if isCachedSumExists {
		log.Printf("Выдан кэшированный результат %v для заказа %q", cachedSum, order)
		return cachedSum, nil
	}

	var sum float32
	for i := range order.Products {
		product, isProductExist := shopVault.SVproducts[order.Products[i]]
		if !isProductExist {
			log.Printf("Произошла ошибка при подсчёте суммы заказа! Товара %q не существует", order.Products[i])
			return 0, errors.New("ProductNotExists")
		}

		if product.Type == ProductSample {
			log.Printf("Произошла ошибка при подсчёте суммы заказа! Товар %q является пробником, который нельзя покупать отдельно", product.Name)
			return 0, errors.New("ProductSampleIsNotInBundle")
		}

		sumMarkup, err := calculateMarkup(*account, *product)
		if err != nil {
			// Logs will be printed in the callable function
			return 0, err
		}
		log.Printf("Наценка/скидка для товара %q со стоимостью %v для пользователя %q составляет %v", product.Name, product.Price, account.Name, sumMarkup)

		sum += product.Price + sumMarkup
	}
	for i := range order.Bundles {
		bundle, isBundleExists := shopVault.SVbundles[order.Bundles[i]]
		if !isBundleExists {
			log.Printf("Произошла ошибка при подсчёте суммы заказа! Набор товаров %q не найден", order.Bundles[i])
			return 0, errors.New("BundleNotExists")
		}
		mainProductPrice := shopVault.SVproducts[bundle.MainProduct].Price
		sum += mainProductPrice + mainProductPrice*(bundle.Discount/-100)
		if bundle.Type == BundleNormal {
			for j := range bundle.AdditionalProducts {
				additionalProductPrice := shopVault.SVproducts[bundle.AdditionalProducts[j]].Price
				sum += additionalProductPrice + additionalProductPrice*(bundle.Discount/-100)
			}
		} else if bundle.Type == BundleSample {
			log.Printf("Пользователю %q к основному товару %q нужно выдать пробник %q", account.Name, shopVault.SVproducts[bundle.MainProduct].Name, bundle.AdditionalProducts[0])
		}
	}
	shopVault.SVordersCache[keyOrder] = sum
	log.Printf("Подсчитана сумма заказа для пользователя %q и она составляет %v", account.Name, sum)
	return sum, nil
}

func calculateMarkup(account Account, product Product) (float32, error) {
	if account.AccountType == AccountPremium {
		if product.Type == ProductPremium {
			return product.Price * (-0.2), nil
		} else if product.Type == ProductNormal {
			return product.Price * 0.5, nil
		} else {
			log.Printf("Произошла ошибка при подсчёте наценки для товара %q! Неверный тип товара", product.Name)
			return 0, errors.New("ProductWrongType")
		}
	} else if account.AccountType == AccountNormal {
		if product.Type == ProductPremium {
			return product.Price * (-0.05), nil
		} else if product.Type == ProductNormal {
			return 0, nil
		} else {
			log.Printf("Произошла ошибка при подсчёте наценки для товара %q! Неверный тип аккаунта %q", product.Name, account.AccountType)
			return 0, errors.New("ProductWrongType")
		}
	} else {
		log.Printf("Произошла ошибка при подсчёте наценки для товара %q! Неверный тип аккаунта %q", product.Name, account.Name)
		return 0, errors.New("AccountWrongType")
	}
}

func generateKeyOrder(account Account, order Order) string {
	// First - account type
	keyOrder := fmt.Sprintf("%v", account.AccountType)
	keyOrder += "-"
	// Second - SVproducts
	sort.Strings(order.Products)
	for _, productName := range order.Products {
		keyOrder += productName
		keyOrder += ","
	}
	// Removing last ',' if it is exists
	if last := len(keyOrder) - 1; last >= 0 && keyOrder[last] == ',' {
		keyOrder = keyOrder[:last]
	}
	// Third - SVbundles
	keyOrder += "-"
	for _, bundleName := range order.Bundles {
		keyOrder += bundleName
		keyOrder += ","
	}
	// Removing last ',' if it is exists
	if last := len(keyOrder) - 1; last >= 0 && keyOrder[last] == ',' {
		keyOrder = keyOrder[:last]
	}
	log.Printf("Ключ %q для заказа %q был создан", keyOrder, order)
	return keyOrder
}

// Should be called when bundle or product has changed
func crashTheCache(shopVault ShopVault) {
	shopVault.SVordersCache = make(map[string]float32)
	log.Println("Произошла очистка кэша сумм заказов")
}
