package main

import (
	"errors"
	"log"
	"sort"
)

type Accounts map[string]*Account

func (shopVault ShopVault) Register(username string, t AccountType) error {
	// Check input
	if len(username) == 0 {
		log.Printf("Произведена попытка регистрации пользователя без имени")
		return errors.New("UsernameIsEmpty")
	}
	if t != AccountNormal && t != AccountPremium {
		log.Printf("Произведена попытка передать неправильный тип аккаунта")
		return errors.New("WrongAccountType")
	}

	_, accountAlreadyExists := shopVault.SVaccounts[username]
	if accountAlreadyExists {
		log.Printf("Не удалось добавить аккаунт %q! Такой аккаунт уже существует", username)
		return errors.New("AccountAlreadyExists")
	}

	shopVault.SVaccounts[username] = &Account{Name: username, Balance: 0, AccountType: t}
	log.Printf("Аккаунт %q с типом %q добавлен", username, t)
	return nil
}

func (shopVault ShopVault) AddBalance(username string, sum float32) error {
	_, accountExists := shopVault.SVaccounts[username]
	if !accountExists {
		log.Printf("Не удалось внести средства на аккаунт %q! Такой аккаунт уже существует", username)
		return errors.New("AccountNotExists")
	}

	if almostEqual(sum, 0) || sum < 0 {
		log.Printf("Не удалось внести средства на аккаунт %q! Вносится нулевая или отрицательная сумма %v", username, sum)
		return errors.New("AccountAddableSumIsBad")
	}

	shopVault.SVaccounts[username].Balance += sum
	log.Printf("На аккаунт %q были внесены %v денег", username, sum)
	return nil
}

func (shopVault ShopVault) Balance(username string) (float32, error) {
	account, accountExists := shopVault.SVaccounts[username]
	if !accountExists {
		log.Printf("Не удалось просмотреть средства на аккаунте %q! Такой аккаунт уже существует", username)
		return 0, errors.New("AccountNotExists")
	}

	log.Printf("Выполнен запрос остатка средств. На аккаунте %q осталось %v денег", username, account.Balance)
	return account.Balance, nil
}

func (shopVault ShopVault) GetAccounts(sortType AccountSortType) []Account {
	accounts := shopVault.SVaccounts

	if len(accounts) <= 0 {
		log.Fatalf("Не удалось отсортировать аккаунты, так как их нет")
		return nil
	}

	switch sortType {
	case SortByName:
		userNames := make([]string, 0)
		for k, _ := range accounts {
			userNames = append(userNames, k)
		}
		sort.Strings(userNames)
		sortedAccounts := make([]Account, 0)
		for i := range userNames {
			sortedAccounts = append(sortedAccounts, *accounts[userNames[i]])
		}
		log.Printf("Запрос на сортировку по имени выполнен! Выданы отсортированные аккаунты %v", sortedAccounts)
		return sortedAccounts
	case SortByNameReverse:
		userNames := make([]string, 0)
		for k, _ := range accounts {
			userNames = append(userNames, k)
		}
		sort.Sort(sort.Reverse(sort.StringSlice(userNames)))
		sortedAccounts := make([]Account, 0)
		for i := range userNames {
			sortedAccounts = append(sortedAccounts, *accounts[userNames[i]])
		}
		log.Printf("Запрос на сортировку по имени в обратном порядке выполнен! Выданы отсортированные аккаунты %v", sortedAccounts)
		return sortedAccounts
	case SortByBalance:
		userNames := make([]string, 0)
		for k, _ := range accounts {
			userNames = append(userNames, k)
		}
		pairList := make(PairList, len(accounts))
		i := 0
		for key := range accounts {
			pairList[i] = Pair{key, accounts[userNames[i]].Balance}
			i++
		}
		sort.Sort(sort.Reverse(pairList))
		sortedAccounts := make([]Account, 0)
		for i := range pairList {
			sortedAccounts = append(sortedAccounts, *accounts[pairList[i].Key])
		}
		log.Printf("Запрос на сортировку по остатку средств по убыванию выполнен! Выданы отсортированные аккаунты %v", sortedAccounts)
		return sortedAccounts
	default:
		log.Fatalf("Не удалось отсортировать аккаунты, так как запрашиваемый тип сортировки %q не реализован", sortType)
		return nil
	}
}

type Pair struct {
	Key   string
	Value float32
}

type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
