package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestShop_PlaceOrder_Success(t *testing.T) {
	var shop ShopVault

	shop.SVaccounts = make(Accounts, 0)
	shop.SVproducts = make(Products, 0)
	shop.SVorders = make(Orders, 0)
	shop.SVbundles = make(Bundles, 0)
	shop.SVordersCache = make(map[string]float32, 0)

	bananaProduct := "banana"
	err := shop.AddProduct(bananaProduct, Product{
		bananaProduct,
		10,
		ProductNormal,
	})
	if err != nil {
		t.Fatal(err)
	}

	userName := "Vasya"
	err = shop.Register(userName, AccountPremium)
	if err != nil {
		t.Fatal(err)
	}

	err = shop.AddBalance(userName, 1000)
	if err != nil {
		t.Fatal(err)
	}

	bananaBundleName := "bananaBundle"
	err = shop.AddBundle(bananaBundleName, bananaProduct, 50, bananaProduct)
	if err != nil {
		t.Fatal(err)
	}

	res, err := shop.CalculateOrder(userName, Order{
		Products: []string{bananaProduct, bananaProduct, bananaProduct},
		Bundles:  []string{bananaBundleName},
	})
	if err != nil {
		t.Fatal(err)
	}
	if res != 15+15+15+5+5 {
		t.Fatal("incorrect order calculation")
	}

	err = shop.PlaceOrder(userName, Order{
		Products: []string{bananaProduct, bananaProduct, bananaProduct},
		Bundles:  []string{bananaBundleName},
	})
	if err != nil {
		t.Fatal(err)
	}

	balance, err := shop.Balance(userName)
	if err != nil {
		t.Fatal(err)
	}
	if balance != 945 {
		t.Fatal("incorrect user balance")
	}
}

func TestShop_ImportExport_Success(t *testing.T) {
	var shop1, shop2 ShopVault

	shop1.SVaccounts = make(Accounts, 0)
	shop1.SVproducts = make(Products, 0)
	shop1.SVorders = make(Orders, 0)
	shop1.SVbundles = make(Bundles, 0)
	shop1.SVordersCache = make(map[string]float32, 0)

	err := shop1.AddProduct("Мороженое",
		Product{Name: "Мороженое", Price: 19.99, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}
	err = shop1.AddProduct("Капсулы для стирки",
		Product{Name: "Капсулы для стирки", Price: 359.99, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}
	err = shop1.AddProduct("Кофе растворимый",
		Product{Name: "Кофе растворимый", Price: 158.89, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}
	err = shop1.AddProduct("Свинина корейка",
		Product{Name: "Свинина корейка", Price: 259.99, Type: ProductPremium})
	if err != nil {
		fmt.Println(err)
	}
	err = shop1.AddProduct("Шоколад Темный с фундуком и изюмом",
		Product{Name: "Шоколад Темный с фундуком и изюмом", Price: 59.29, Type: ProductPremium})
	if err != nil {
		fmt.Println(err)
	}
	err = shop1.AddProduct("Квас",
		Product{Name: "Квас", Price: 37.00, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}

	v, err := shop1.Export()
	if err != nil {
		t.Fatal()
	}
	err = shop2.Import(v)
	if err != nil {
		t.Fatal()
	}
	if !reflect.DeepEqual(shop1, shop2) {
		t.Fatal("shops not equal")
	}
}
