package main

import (
	"errors"
	"log"
)

type Bundles map[string]*Bundle

func (shopVault ShopVault) AddBundle(name string, mainProductName string, discount float32, additionalProductNames ...string) error {
	// Check input
	if len(name) == 0 {
		log.Printf("Произведена попытка добавления набора товаров без имени")
		return errors.New("BundleNameIsEmpty")
	}

	_, isBundleAlreadyExists := shopVault.SVbundles[name]
	if isBundleAlreadyExists {
		log.Printf("Произошла ошибка при добавлении набора товаров! Набор товаров %q уже существует", name)
		return errors.New("BundleAlreadyExists")
	}
	mainProduct, isMainProductExists := shopVault.SVproducts[mainProductName]
	if !isMainProductExists {
		log.Printf("Произошла ошибка при добавлении набора товаров! Основного товара %q не существует в списке товаров!", mainProductName)
		return errors.New("MainProductNotExists")
	}
	if mainProduct.Type == ProductSample {
		log.Printf("Произошла ошибка при добавлении набора товаров! Основной товар %q является пробником!", mainProductName)
		return errors.New("MainProductNotExists")
	}
	bundleType := BundleNormal
	for i := range additionalProductNames {
		additionalProduct, isAdditionalProductExists := shopVault.SVproducts[additionalProductNames[i]]
		if !isAdditionalProductExists {
			log.Printf("Произошла ошибка при добавлении набора товаров! Дополнительного товара %q не существует в списке товаров!", additionalProductNames[i])
			return errors.New("AdditionalProductNotExists")
		}
		if additionalProduct.Type == ProductSample {
			bundleType = BundleSample
			if len(additionalProductNames) > 1 {
				log.Printf("Произошла ошибка при добавлении набора товаров %q! В наборе товаров %q с пробником присутствуют другие товары/пробники", name, additionalProductNames)
				return errors.New("AdditionalProductsWithSample")
			}
		}
	}
	// Check input discount
	if bundleType == BundleNormal {
		if discount < 1 || discount > 99 {
			log.Printf("Произошла ошибка при добавлении набора товаров! Размер скидки %v меньше 1 или больше 99 процентов", discount)
			return errors.New("DiscountWrongValue")
		}
	} else if bundleType == BundleSample {
		if discount != 0 {
			log.Printf("Произошла ошибка при добавлении набора товаров! Размер скидки для набора с пробником должен быть равен 0, а сейчас он равен %v", discount)
			return errors.New("DiscountWrongValue")
		}
	}
	shopVault.SVbundles[name] = &Bundle{MainProduct: mainProductName, AdditionalProducts: additionalProductNames, Type: bundleType, Discount: discount}
	log.Printf("Набор товаров %q с типом %q, состоящий из основного товара %q и дополнительных товаров %q со скидкой %v добавлен", name, bundleType, mainProductName, additionalProductNames, discount)
	return nil
}

func (shopVault ShopVault) ChangeDiscount(name string, discount float32) error {
	bundle, isBundleExists := shopVault.SVbundles[name]
	if !isBundleExists {
		log.Printf("Произошла ошибка при изменении скидки на набор товаров! Набора товаров %q не существует", name)
		return errors.New("BundleNotExists")
	}
	if bundle.Type == BundleNormal {
		if discount < 1 || discount > 99 {
			log.Printf("Произошла ошибка при изменении скидки на набор товаров! Размер скидки %v меньше 1 или больше 99 процентов", discount)
			return errors.New("DiscountWrongValue")
		}
	} else if bundle.Type == BundleSample {
		if discount != 0 {
			log.Printf("Произошла ошибка при изменении скидки на набор товаров! Размер скидки для набора с пробником должен быть равен 0, а сейчас он равен %v", discount)
			return errors.New("DiscountWrongValue")
		}
	}
	log.Printf("Скидка на набор товаров %q была изменена с %v на %v", name, bundle.Discount, discount)
	shopVault.SVbundles[name].Discount = discount
	crashTheCache(shopVault)
	return nil
}

func (shopVault ShopVault) GetBundle(name string) (Bundle, error) {
	bundle, isBundleExists := shopVault.SVbundles[name]
	if !isBundleExists {
		log.Printf("Произошла ошибка при получении набора товаров! Набора товаров %q не существует", name)
		return Bundle{}, errors.New("BundleNotExists")
	}
	log.Printf("Получен набор товаров %q", name)
	return *bundle, nil
}

func (shopVault ShopVault) RemoveBundle(name string) error {
	_, isBundleExists := shopVault.SVbundles[name]
	if !isBundleExists {
		log.Printf("Произошла ошибка при удалении набора товаров! Набора товаров %q не существует", name)
		return errors.New("BundleNotExists")
	}
	delete(shopVault.SVbundles, name)
	log.Printf("Удалён набор товаров %q", name)
	crashTheCache(shopVault)
	return nil
}
