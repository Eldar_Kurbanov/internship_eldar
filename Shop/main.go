package main

import (
	"fmt"
	"github.com/rkusa/gm/math32"
	"io/ioutil"
)

type ShopVault struct {
	SVproducts    Products
	SVorders      Orders
	SVaccounts    Accounts
	SVbundles     Bundles
	SVordersCache map[string]float32
}

const float32EqualityThreshold = 1e-6

func almostEqual(a, b float32) bool {
	return math32.Abs(a-b) <= float32EqualityThreshold
}

func init() {
	// Uncomment this to disable logs in this project
	//log.SetFlags(0)
	//log.SetOutput(ioutil.Discard)
}

func main() {
	var shopVault ShopVault
	fmt.Println("*********** НАЧАЛО РАБОТЫ МАГАЗИНА ************")
	fmt.Println()
	shopVault.SVaccounts = make(Accounts, 0)
	shopVault.SVproducts = make(Products, 0)
	shopVault.SVorders = make(Orders, 0)
	shopVault.SVbundles = make(Bundles, 0)
	shopVault.SVordersCache = make(map[string]float32, 0)

	fmt.Println("Наполняем магазин содержимым:")
	fmt.Println()
	err := shopVault.AddProduct("Мороженое",
		Product{Name: "Мороженое", Price: 19.99, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddProduct("Капсулы для стирки",
		Product{Name: "Капсулы для стирки", Price: 359.99, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddProduct("Кофе растворимый",
		Product{Name: "Кофе растворимый", Price: 158.89, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddProduct("Свинина корейка",
		Product{Name: "Свинина корейка", Price: 259.99, Type: ProductPremium})
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddProduct("Шоколад Темный с фундуком и изюмом",
		Product{Name: "Шоколад Темный с фундуком и изюмом", Price: 59.29, Type: ProductPremium})
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddProduct("Квас",
		Product{Name: "Квас", Price: 37.00, Type: ProductNormal})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()
	fmt.Println("Теперь список продуктов выглядит так:")
	fmt.Println(shopVault.SVproducts)
	fmt.Println()

	fmt.Println("Попробуем изменить один товар:")
	err = shopVault.ModifyProduct("Капсулы для стирки",
		Product{Name: "Капсулы для стирки", Price: 858.39, Type: ProductPremium})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()
	fmt.Println("Теперь список продуктов выглядит так:")
	fmt.Println(shopVault.SVproducts)
	fmt.Println()

	fmt.Println("Теперь попробуем удалить товар:")
	err = shopVault.RemoveProduct("Квас")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()
	fmt.Println("Теперь список продуктов выглядит так:")
	fmt.Println(shopVault.SVproducts)
	fmt.Println()

	fmt.Println("Зарегистрируем парочку аккаунтов:")
	err = shopVault.Register("Вася", AccountNormal)
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.Register("Петя", AccountPremium)
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.Register("Ваня", AccountNormal)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("Добавим им средств на счёт:")
	err = shopVault.AddBalance("Вася", 300.00)
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddBalance("Петя", 30000000.00)
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddBalance("Ваня", 1000.00)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("Проверим, что деньги дошли:")
	balance, err := shopVault.Balance("Вася")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("На счету Васи %v денег\n", balance)
	balance, err = shopVault.Balance("Петя")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("На счету Пети %v денег\n", balance)
	balance, err = shopVault.Balance("Ваня")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("На счету Вани %v денег\n", balance)

	fmt.Println()
	fmt.Println("Выведем все аккаунты в разных сортировках:")
	fmt.Println()
	fmt.Println("Сортировка по имени:")
	fmt.Println(shopVault.GetAccounts(SortByName))
	fmt.Println("Сортировка по имени в обратном порядке:")
	fmt.Println(shopVault.GetAccounts(SortByNameReverse))
	fmt.Println("Сортировка по остатку на счёте по убыванию:")
	fmt.Println(shopVault.GetAccounts(SortByBalance))
	fmt.Println()

	fmt.Println("Ваня решил что-то заказать...")
	vanyaOrder := Order{Products: []string{"Мороженое", "Капсулы для стирки"}}
	err = shopVault.PlaceOrder("Ваня", vanyaOrder)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("Петя тоже решил отовариться...")
	petyaOrder := Order{Products: []string{"Мороженое", "Капсулы для стирки", "Кофе растворимый", "Свинина корейка"}}
	err = shopVault.PlaceOrder("Петя", petyaOrder)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("И ещё разочек...")
	petyaOrder = Order{Products: []string{"Мороженое", "Капсулы для стирки", "Кофе растворимый", "Свинина корейка"}}
	err = shopVault.PlaceOrder("Петя", petyaOrder)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	balance, err = shopVault.Balance("Петя")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("На счету Пети осталось %v денег\n", balance)
	balance, err = shopVault.Balance("Ваня")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("На счету Вани осталось %v денег\n", balance)

	fmt.Println()
	fmt.Println("Добавим набор товаров Семейное мороженое")
	err = shopVault.AddBundle("Семейное мороженое", "Мороженое", 25, "Мороженое", "Мороженое", "Мороженое")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()

	fmt.Println("Также добавим бесплатный пробник шоколадки к кофе")
	err = shopVault.AddProduct("Маленькая шоколадка", Product{Name: "Маленькая шоколадка", Type: ProductSample})
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.AddBundle("Кофейный компаньон", "Кофе растворимый", 0, "Маленькая шоколадка")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("Вася решил отовариться...")
	err = shopVault.AddBalance("Вася", 300)
	if err != nil {
		fmt.Println(err)
	}
	vasyaOrder := Order{Products: []string{"Шоколад Темный с фундуком и изюмом"}, Bundles: []string{"Семейное мороженое", "Кофейный компаньон"}}
	err = shopVault.PlaceOrder("Вася", vasyaOrder)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("Поменяем скидку на набор товаров и снова сделаем заказ для Васи")
	err = shopVault.ChangeDiscount("Семейное мороженое", 50)
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.PlaceOrder("Вася", vasyaOrder)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("Попробуем получить набор товаров")
	bundle, err := shopVault.GetBundle("Семейное мороженое")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Скидка на данный набор товаров: %v", bundle.Discount)

	fmt.Println()
	fmt.Println("Удалим набор товаров")
	err = shopVault.RemoveBundle("Семейное мороженое")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("И попробуем снова его получить")
	bundle, err = shopVault.GetBundle("Семейное мороженое")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()
	fmt.Println("Запишем магазин в файл shop.json")
	shopSerialized, err := shopVault.Export()
	fmt.Println(shopSerialized)
	err = ioutil.WriteFile("shop.json", shopSerialized, 0666)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println()
	fmt.Println("Прочитаем магазин из файла shop.json...")
	shopSerialized, err = ioutil.ReadFile("shop.json")
	if err != nil {
		fmt.Println(err)
	}
	err = shopVault.Import(shopSerialized)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()
	fmt.Println("Баланс Васи:")
	fmt.Println(shopVault.Balance("Вася"))

	fmt.Println()
	fmt.Println("************ КОНЕЦ РАБОТЫ МАГАЗИНА *************")
}
