package main

import (
	"errors"
	"log"
)

type Products map[string]*Product

func (shopVault ShopVault) AddProduct(name string, product Product) error {
	// Check input
	if len(name) == 0 {
		log.Printf("Произведена попытка добавить товар без имени")
		return errors.New("UsernameIsEmpty")
	}
	if product.Type != ProductNormal && product.Type != ProductPremium && product.Type != ProductSample {
		log.Printf("Произведена попытка передать неправильный тип товара!")
		return errors.New("WrongProductType")
	}
	// А нужна ли вообще эта проверка??
	if name != product.Name {
		log.Printf("Переданное имя товара %q не соответствует имени товара, описанному в Product %q!", name, product.Name)
		return errors.New("ProductNamesMismatch")
	}

	_, productAlreadyExists := shopVault.SVproducts[name]
	if productAlreadyExists {
		log.Printf("Товар %q не был добавлен в список товаров, так как он уже существует", name)
		return errors.New("ProductAlreadyExists")
	}

	if product.Type == ProductSample {
		if product.Price != 0 {
			log.Printf("Пробник %q не был добавлен в список товаров, так как его цена не нулевая", product.Name)
			return errors.New("ProductSampleIsNotFree")
		}
	} else {
		if almostEqual(product.Price, 0) || product.Price <= 0 {
			log.Printf("Товар %q не был добавлен в список товаров, так как его цена %v нулевая или отрицательная", name, product.Price)
			return errors.New("ProductPriceIsBad")
		}
	}

	shopVault.SVproducts[name] = &product
	log.Printf("Товар %q был добавлен в список товаров", name)
	return nil
}

func (shopVault ShopVault) ModifyProduct(name string, product Product) error {
	// Check input
	if len(name) == 0 {
		log.Printf("Произведена попытка изменить товар без имени!")
		return errors.New("UsernameIsEmpty")
	}
	if product.Type != ProductNormal && product.Type != ProductPremium && product.Type != ProductSample {
		log.Printf("Произведена попытка передать неправильный тип товара!")
		return errors.New("WrongProductType")
	}
	// А нужна ли вообще эта проверка??
	if name != product.Name {
		log.Printf("Переданное имя товара %q не соответствует имени товара, описанному в Product %q! Если вы хотите изменить имя товара, то удалите его, а затем добавьте новый...", name, product.Name)
		return errors.New("ProductNamesMismatch")
	}

	_, productExists := shopVault.SVproducts[name]
	if !productExists {
		log.Printf("Товар %q не был изменён, так как он не существует!", name)
		return errors.New("ProductNotExists")
	}

	if product.Type == ProductSample {
		if product.Price != 0 {
			log.Printf("Пробник %q не был изменён в списке товаров, так как его цена не нулевая", product.Name)
			return errors.New("ProductSampleIsNotFree")
		}
	} else {
		if almostEqual(product.Price, 0) || product.Price <= 0 {
			log.Printf("Товар %q не был изменён в списке товаров, так как его цена %v нулевая или отрицательная", name, product.Price)
			return errors.New("ProductPriceIsBad")
		}
	}

	shopVault.SVproducts[name] = &product
	log.Printf("Товар %q был изменён", name)
	crashTheCache(shopVault)
	return nil
}

func (shopVault ShopVault) RemoveProduct(name string) error {
	_, productExists := shopVault.SVproducts[name]
	if !productExists {
		log.Printf("Товар %q не был удалён, так как он не существует!", name)
		return errors.New("ProductNotExists")
	}

	for i := range shopVault.SVbundles {
		if shopVault.SVbundles[i].MainProduct == name {
			log.Printf("Товар %q не был удалён, так как он является главным товаром набора %q. Сначала удалите этот набор", name, i)
			return errors.New("ProductExistsInBundle")
		}
		for j := range shopVault.SVbundles[i].AdditionalProducts {
			if shopVault.SVbundles[i].AdditionalProducts[j] == name {
				log.Printf("Товар %q не был удалён, так как он является одним из дополнительных товаров набора %q. Сначала удалите этот набор", name, i)
				return errors.New("ProductExistsInBundle")
			}
		}
	}

	delete(shopVault.SVproducts, name)

	log.Printf("Товар %q был удалён", name)
	crashTheCache(shopVault)
	return nil
}
