package main

import (
	"encoding/json"
	"fmt"
	"log"
)

func (shopVault ShopVault) Export() ([]byte, error) {
	shopSerialized, err := json.Marshal(shopVault)
	if err != nil {
		log.Printf("Произошла ошибка при преобразовании данных магазина %v к строке", shopVault)
		return nil, err
	}
	return shopSerialized, nil
}

func (shopVault *ShopVault) Import(data []byte) error {
	fmt.Println(data)
	err := json.Unmarshal(data, &shopVault)
	if err != nil {
		log.Printf("Произошла ошибка при сборке магазина из текста %v", data)
		return err
	}
	return nil
}
