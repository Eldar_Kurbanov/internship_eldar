package lecture2

import (
	"reflect"
	"testing"
)

func Test_accounts_addAccount(t *testing.T) {
	type args struct {
		name    string
		balance money
	}

	tests := []struct {
		name    string
		a       accounts
		args    args
		result  accounts
		wantErr bool
	}{
		{
			"regular product",
			accounts{},
			args{"account", toMoney(1)},
			accounts{"account": money(100)},
			false,
		},
		{
			"existing account",
			accounts{"account": money(100)},
			args{"account", money(50)},
			accounts{"account": money(100)},
			true,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			err := tt.a.addAccount(tt.args.name, tt.args.balance)

			if tt.wantErr && err == nil {
				t.Fatal()
			}

			if !tt.wantErr && err != nil {
				t.Fatal()
			}

			if !reflect.DeepEqual(tt.a, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_accounts_removeAccount(t *testing.T) {
	type args struct {
		name string
	}

	tests := []struct {
		name    string
		a       accounts
		args    args
		result  accounts
		wantErr bool
	}{
		{
			"Remove existing account",
			accounts{"account": money(100)},
			args{name: "account"},
			accounts{},
			false,
		},
		{
			"Remove not existing account",
			accounts{"account2": money(100)},
			args{name: "account"},
			accounts{"account2": money(100)},
			true,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			err := tt.a.removeAccount(tt.args.name)

			if tt.wantErr && err == nil {
				t.Fatal()
			}

			if !tt.wantErr && err != nil {
				t.Fatal()
			}

			if !reflect.DeepEqual(tt.a, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_accounts_changeName(t *testing.T) {
	type args struct {
		old string
		new string
	}

	tests := []struct {
		name    string
		a       accounts
		args    args
		wantErr bool
		result  accounts
	}{
		{
			"Existing account",
			accounts{"account": money(100)},
			args{old: "account", new: "accountNew"},
			false,
			accounts{"accountNew": money(100)},
		},
		{
			"Not existing account",
			accounts{"account": money(100)},
			args{old: "account2", new: "accountNew"},
			true,
			accounts{"account": money(100)},
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if err := tt.a.changeName(tt.args.old, tt.args.new); (err != nil) != tt.wantErr {
				t.Errorf("changeName() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.a, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_accounts_spendMoney(t *testing.T) {
	type args struct {
		name  string
		spend money
	}

	tests := []struct {
		name    string
		p       accounts
		args    args
		wantErr bool
		result  accounts
	}{
		{
			"Regular spend",
			accounts{"account": money(100)},
			args{"account", money(45)},
			false,
			accounts{"account": money(55)},
		},
		{
			"No account",
			accounts{"account2": money(100)},
			args{"account", money(45)},
			true,
			accounts{"account2": money(100)},
		},
		{
			"No spend",
			accounts{"account": money(44)},
			args{"account", money(45)},
			true,
			accounts{"account": money(44)},
		},
		{
			"Negative money",
			accounts{"account": money(100)},
			args{"account", money(-45)},
			true,
			accounts{"account": money(100)},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if err := tt.p.spendMoney(tt.args.name, tt.args.spend); (err != nil) != tt.wantErr {
				t.Errorf("spendMoney() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.p, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_accounts_addMoney(t *testing.T) {
	type args struct {
		name  string
		spend money
	}

	tests := []struct {
		name    string
		p       accounts
		args    args
		wantErr bool
		result  accounts
	}{
		{
			"Regular add",
			accounts{"account": money(100)},
			args{"account", money(45)},
			false,
			accounts{"account": money(145)},
		},
		{
			"No account",
			accounts{"account2": money(100)},
			args{"account", money(45)},
			true,
			accounts{"account2": money(100)},
		},
		{
			"Negative money",
			accounts{"account": money(100)},
			args{"account", money(-45)},
			true,
			accounts{"account": money(100)},
		},
	}

	for _, tt := range tests {
		tt := tt
		
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.p.addMoney(tt.args.name, tt.args.spend); (err != nil) != tt.wantErr {
				t.Errorf("addMoney() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.p, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_accounts_sortNormal(t *testing.T) {
	a := accounts{"ahmed": money(146), "eldar": money(35), "igor": money(948), "dmitry": money(166), "aleksandr": money(77)}
	a.sortNormal()
}

func Test_accounts_sortReverse(t *testing.T) {
	a := accounts{"ahmed": money(146), "eldar": money(35), "igor": money(948), "dmitry": money(166), "aleksandr": money(77)}
	a.sortReverse()
}

func Test_accounts_sortByMoneyReverse(t *testing.T) {
	a := accounts{"ahmed": money(146), "eldar": money(35), "igor": money(948), "dmitry": money(166), "aleksandr": money(77)}
	a.sortByMoneyReverse()
}
