package lecture2

import (
	"internship_eldar/SecondTry/myerror"
	"sort"
)

const accountExists = myerror.MyError("AccountExists")
const accountNotExists = myerror.MyError("AccountNotExists")
const accountNoMoney = myerror.MyError("AccountNoEnoughMoney")
const accountNegativeMoney = myerror.MyError("AccountNegativeMoney")

type accounts map[string]money

func (p *accounts) addAccount(name string, balance money) error {
	_, exists := (*p)[name]
	if exists {
		return accountExists
	}

	(*p)[name] = balance

	return nil
}

func (p *accounts) removeAccount(name string) error {
	_, exists := (*p)[name]
	if !exists {
		return accountNotExists
	}

	delete(*p, name)

	return nil
}

func (p *accounts) changeBalance(name string, balance money) error {
	if balance < 0 {
		return accountNegativeMoney
	}

	_, exists := (*p)[name]
	if !exists {
		return accountNotExists
	}

	(*p)[name] = balance

	return nil
}

func (p *accounts) spendMoney(name string, spend money) error {
	if spend < 0 {
		return accountNegativeMoney
	}

	balance, exists := (*p)[name]
	if !exists {
		return accountNotExists
	}

	remainder := balance - spend

	if remainder < 0 {
		return accountNoMoney
	}

	(*p)[name] = remainder

	return nil
}

func (p *accounts) addMoney(name string, pay money) error {
	if pay < 0 {
		return accountNegativeMoney
	}

	balance, exists := (*p)[name]
	if !exists {
		return accountNotExists
	}

	(*p)[name] = balance + pay

	return nil
}

func (p *accounts) changeName(old string, new string) error {
	_, exists := (*p)[old]
	if !exists {
		return accountNotExists
	}

	balance := (*p)[old]
	delete(*p, old)
	(*p)[new] = balance

	return nil
}

func (p *accounts) sortNormal() []string {
	keys := make([]string, 0, len(*p))
	for k := range *p {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	return keys
}

func (p *accounts) sortReverse() []string {
	keys := make([]string, 0, len(*p))
	for k := range *p {
		keys = append(keys, k)
	}

	sort.Sort(sort.Reverse(sort.StringSlice(keys)))

	return keys
}

func (p *accounts) sortByMoney() []string {
	keys := make([]string, len(*p))
	pl := make(PairList, len(*p))

	i := 0

	for k, v := range *p {
		pl[i] = Pair{k, v}
		i++
	}

	sort.Sort(pl)

	for i, k := range pl {
		keys[i] = k.Key
	}

	return keys
}

func (p *accounts) sortByMoneyReverse() []string {
	keys := make([]string, len(*p))
	pl := make(PairList, len(*p))

	i := 0

	for k, v := range *p {
		pl[i] = Pair{k, v}
		i++
	}

	sort.Sort(sort.Reverse(pl))

	for i, k := range pl {
		keys[i] = k.Key
	}

	return keys
}
