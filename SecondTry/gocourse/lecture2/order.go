package lecture2

import (
	"sort"
	"strings"
)

func (p *products) calculateOrder(prs []string) (float64, error) {
	var sum money = 0

	for _, v := range prs {
		product, exists := (*p)[v]

		if !exists {
			return sum.float64(), productNotExists
		}

		sum += product
	}

	return sum.float64(), nil
}

func (p *products) calculateOrderWithCache() func([]string) (float64, bool, error) {
	cache := make(map[string]float64)

	return func(prs []string) (float64, bool, error) {
		sort.Strings(prs)

		str := strings.Join(prs, ";")
		cacheSum, exists := cache[str]

		if exists {
			return cacheSum, true, nil
		}

		sum, err := p.calculateOrder(prs)
		if err != nil {
			return sum, false, err
		}

		cache[str] = sum

		return sum, false, err
	}
}
