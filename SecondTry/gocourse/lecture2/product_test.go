package lecture2

import (
	"reflect"
	"testing"
)

func Test_products_addProduct(t *testing.T) {
	type args struct {
		name  string
		price money
	}

	tests := []struct {
		name    string
		p       products
		args    args
		result  products
		wantErr bool
	}{
		{
			"regular product",
			products{},
			args{"products", toMoney(1)},
			products{"products": money(100)},
			false,
		},
		{
			"existing product",
			products{"Product": money(100)},
			args{"Product", money(50)},
			products{"Product": money(100)},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.p.addProduct(tt.args.name, tt.args.price)

			if tt.wantErr && err == nil {
				t.Fatal()
			}

			if !tt.wantErr && err != nil {
				t.Fatal()
			}

			if !reflect.DeepEqual(tt.p, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_products_removeProduct(t *testing.T) {
	type args struct {
		name string
	}

	tests := []struct {
		name    string
		p       products
		args    args
		result  products
		wantErr bool
	}{
		{
			"Remove existing product",
			products{"Product": money(100)},
			args{name: "Product"},
			products{},
			false,
		},
		{
			"Remove not existing product",
			products{"Product2": money(100)},
			args{name: "Product"},
			products{"Product2": money(100)},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.p.removeProduct(tt.args.name)

			if tt.wantErr && err == nil {
				t.Fatal()
			}

			if !tt.wantErr && err != nil {
				t.Fatal()
			}

			if !reflect.DeepEqual(tt.p, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_products_changePrice(t *testing.T) {
	type args struct {
		name  string
		price money
	}

	tests := []struct {
		name    string
		p       products
		args    args
		result  products
		wantErr bool
	}{
		{
			"Existing product",
			products{"Product": money(100)},
			args{name: "Product", price: money(150)},
			products{"Product": money(150)},
			false,
		},
		{
			"Not existing product",
			products{"Product": money(100)},
			args{name: "Product2", price: money(150)},
			products{"Product": money(100)},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.p.changePrice(tt.args.name, tt.args.price); (err != nil) != tt.wantErr {
				t.Errorf("changePrice() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.p, tt.result) {
				t.Fatal()
			}
		})
	}
}

func Test_products_changeName(t *testing.T) {
	type args struct {
		old string
		new string
	}

	tests := []struct {
		name    string
		p       products
		args    args
		wantErr bool
		result  products
	}{
		{
			"Existing product",
			products{"Product": money(100)},
			args{old: "Product", new: "ProductNew"},
			false,
			products{"ProductNew": money(100)},
		},
		{
			"Not existing product",
			products{"Product": money(100)},
			args{old: "Product2", new: "ProductNew"},
			true,
			products{"Product": money(100)},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.p.changeName(tt.args.old, tt.args.new); (err != nil) != tt.wantErr {
				t.Errorf("changeName() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.p, tt.result) {
				t.Fatal()
			}
		})
	}
}
