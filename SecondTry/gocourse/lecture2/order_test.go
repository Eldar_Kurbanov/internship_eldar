package lecture2

import "testing"

func Test_products_calculateOrder(t *testing.T) {
	type args struct {
		prs []string
	}

	tests := []struct {
		name string
		p    products
		args args
		want float64
		err  error
	}{
		{
			"Regular order",
			products{"Product1": money(100), "Product2": money(50), "Product3": money(200)},
			args{prs: []string{"Product1", "Product1", "Product2", "Product3"}},
			4.5,
			nil,
		},
		{
			"Order with unexisting product",
			products{"Product1": money(100), "Product2": money(50), "Product3": money(200)},
			args{prs: []string{"Product1", "Product1", "Product2", "Product3", "Product4"}},
			4.5,
			productNotExists,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.p.calculateOrder(tt.args.prs)

			if got != tt.want {
				t.Errorf("calculateOrder() = %v, want %v", got, tt.want)
			}

			if err != tt.err {
				t.Fatal()
			}
		})
	}
}

func Test_products_calculateOrderWithCache(t *testing.T) {
	prs := products{"Product1": money(100), "Product2": money(50), "Product3": money(200)}
	fun := prs.calculateOrderWithCache()

	type args struct {
		prs []string
	}

	tests := []struct {
		name string
		args args
		want float64
		err  error
	}{
		{
			"Regular order",
			args{prs: []string{"Product1", "Product1", "Product2", "Product3"}},
			4.5,
			nil,
		},
		{
			"Order with unexisting product",
			args{prs: []string{"Product1", "Product1", "Product2", "Product3", "Product4"}},
			4.5,
			productNotExists,
		},
		{
			"First order repeat",
			args{prs: []string{"Product3", "Product1", "Product2", "Product1"}},
			4.5,
			nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _, err := fun(tt.args.prs)

			if got != tt.want {
				t.Errorf("calculateOrderWithCache() = %v, want %v", got, tt.want)
			}

			if err != tt.err {
				t.Fatal()
			}
		})
	}
}
