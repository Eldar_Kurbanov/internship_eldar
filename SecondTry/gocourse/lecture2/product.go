package lecture2

import (
	"internship_eldar/SecondTry/myerror"
)

const productExists = myerror.MyError("ProductExists")
const productNotExists = myerror.MyError("ProductNotExists")

type products map[string]money

func (p *products) addProduct(name string, price money) error {
	_, exists := (*p)[name]
	if exists {
		return productExists
	}

	(*p)[name] = price

	return nil
}

func (p *products) removeProduct(name string) error {
	_, exists := (*p)[name]
	if !exists {
		return productNotExists
	}

	delete(*p, name)

	return nil
}

func (p *products) changePrice(name string, price money) error {
	_, exists := (*p)[name]
	if !exists {
		return productNotExists
	}

	(*p)[name] = price

	return nil
}

func (p *products) changeName(old string, new string) error {
	_, exists := (*p)[old]
	if !exists {
		return productNotExists
	}

	price := (*p)[old]
	delete(*p, old)
	(*p)[new] = price

	return nil
}
