package lecture2

import (
	"fmt"
	"testing"
)

func Test_toMoney(t *testing.T) {
	type args struct {
		f float64
	}

	tests := []struct {
		name string
		args args
		want money
	}{
		{
			"toMoney",
			args{f: 1.5},
			money(150),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := toMoney(tt.args.f); got != tt.want {
				t.Errorf("toMoney() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_money_float64(t *testing.T) {
	tests := []struct {
		name string
		m    money
		want float64
	}{
		{
			"convert money to float 64",
			money(150),
			1.5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.m.float64(); got != tt.want {
				t.Errorf("float64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_money_multiply(t *testing.T) {
	type args struct {
		f float64
	}

	tests := []struct {
		name string
		m    money
		args args
		want money
	}{
		{
			"regular multiply",
			money(150),
			args{f: 2},
			money(300),
		},
		{
			"safe multiply",
			money(139),
			args{f: 1.39},
			money(193),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var m = 1.39
			fmt.Printf("Unsafe multiply: %v\n", m*m)

			if got := tt.m.multiply(tt.args.f); got != tt.want {
				t.Errorf("multiply() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_money_String(t *testing.T) {
	tests := []struct {
		name string
		m    money
		want string
	}{
		{
			"Example",
			money(125),
			"1.25",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.m.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
