package lecture2

import (
	"errors"
	"reflect"
	"testing"
)

func TestSortByName(t *testing.T) {
	testCases := []struct {
		name     string
		input    []string
		expected []string
	}{
		{
			"basic case. nil slice",
			nil,
			[]string{},
		},
		{
			"basic case. empty slice",
			[]string{},
			[]string{},
		},
		{
			"basic case. single element slice",
			[]string{"a"},
			[]string{"a"},
		},
		{
			"single duplicate element. same price",
			[]string{"a", "a"},
			[]string{"a"},
		},
		{
			"single duplicate element with one in the middle. different price",
			[]string{"a", "b", "a"},
			[]string{"a", "b"},
		},
		{
			"five elements with duplicates",
			[]string{"a", "b", "c", "a", "c"},
			[]string{"a", "b", "c"},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			a := accounts{}

			for _, v := range tc.input {
				a[v] = money(0)
			}

			res := a.sortNormal()

			if !reflect.DeepEqual(res, tc.expected) {
				t.Fatalf("got\t\t%v\nwant\t%v", res, tc.expected)
			}
		})
	}
}

//func TestHashOrder(t *testing.T) {
//	testCases := []struct {
//		name     string
//		input    []string
//		expected uint64
//	}{
//		{
//			"basic case. nil slice",
//			nil,
//			14695981039346656037,
//		},
//		{
//			"basic case. empty slice",
//			[]string{},
//			14695981039346656037,
//		},
//		{
//			"basic case. single element slice",
//			[]string{"a"},
//			12638187200555641996,
//		},
//		{
//			"single duplicate element",
//			[]string{"a", "a"},
//			620444549055354551,
//		},
//		{
//			"single duplicate element with one in the middle. different price",
//			[]string{"a", "b", "a"},
//			16653391238245862383,
//		},
//		{
//			"five elements with duplicates",
//			[]string{"a", "b", "c", "a", "c"},
//			1040503207626252133,
//		},
//	}
//
//	for _, tc := range testCases {
//		tc := tc
//		t.Run(tc.name, func(t *testing.T) {
//			t.Parallel()
//
//			res := hashOrder(tc.input)
//
//			if !reflect.DeepEqual(res, tc.expected) {
//				t.Fatalf("got\t\t%v\nwant\t%v", res, tc.expected)
//			}
//		})
//	}
//}

func TestCalculateOrder(t *testing.T) {
	testCases := []struct {
		name               string
		shop               products
		order              []string
		expectedTotalPrice uint64
		expectedError      error
	}{
		{
			"basic case. nil slice",
			map[string]money{},
			nil,
			0,
			nil,
		},
		{
			"basic case. empty slice",
			map[string]money{},
			[]string{},
			0,
			nil,
		},
		{
			"basic case. single element slice",
			map[string]money{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			[]string{"a"},
			1,
			nil,
		},
		{
			"basic case. two element slice",
			map[string]money{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			[]string{"b", "a"},
			11,
			nil,
		},
		{
			"basic case. single unknown item",
			map[string]money{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			[]string{"xxx"},
			0,
			productNotExists,
		},
		{
			"basic case. single unknown item in between ",
			map[string]money{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			[]string{"a", "xxx", "b"},
			1,
			productNotExists,
		},
		{
			"partial match",
			map[string]money{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			[]string{"aa"},
			0,
			productNotExists,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			res, err := tc.shop.calculateOrder(tc.order)

			if !errors.Is(err, tc.expectedError) {
				t.Fatalf("got\t\t%v\nwant\t%v", err, tc.expectedError)
			}
			if !reflect.DeepEqual(uint64(res), tc.expectedTotalPrice) {
				t.Fatalf("got\t\t%v\nwant\t%v", res, tc.expectedTotalPrice)
			}
		})
	}
}

func TestCalculateOrderWithCache(t *testing.T) {
	shop := products{
		"a": toMoney(1),
		"b": toMoney(10),
	}

	f := shop.calculateOrderWithCache()

	testCases := []struct {
		name               string
		order              []string
		expectedTotalPrice uint64
		expectedError      error
		shouldUseCache     bool
	}{
		{
			"basic case. nil slice",
			nil,
			0,
			nil,
			false,
		},
		{
			"basic case. empty slice",
			[]string{},
			0,
			nil,
			false,
		},
		{
			"basic case. single element slice",
			[]string{"a"},
			1,
			nil,
			false,
		},
		{
			"basic case. two element slice",
			[]string{"b", "a"},
			11,
			nil,
			false,
		},
		{
			"basic case. should use cache",
			[]string{"a", "b"},
			11,
			nil,
			true,
		},
		{
			"basic case. single unknown item",
			[]string{"xxx"},
			0,
			productNotExists,
			false,
		},
		{
			"basic case. single unknown item in between ",
			[]string{"a", "xxx", "b"},
			11,
			productNotExists,
			false,
		},
		{
			"partial match",
			[]string{"aa"},
			0,
			productNotExists,
			false,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			for i := 0; i < 5; i++ {
				res, cacheUsed, err := f(tc.order)

				if !errors.Is(err, tc.expectedError) {
					t.Fatalf("got\t\t%v\nwant\t%v", err, tc.expectedError)
				}
				if tc.expectedError == nil && i > 0 {
					if !cacheUsed {
						t.Fatalf("cache was not used: %v", tc.order)
					}
					if res != float64(tc.expectedTotalPrice) {
						t.Fatalf("got in cache\t\t%v\nwant\t%v", res, tc.expectedTotalPrice)
					}
				}
				if !reflect.DeepEqual(uint64(res), tc.expectedTotalPrice) {
					t.Fatalf("got\t\t%v\nwant\t%v", res, tc.expectedTotalPrice)
				}
			}
		})
	}
}

func TestAddItem(t *testing.T) {
	testCases := []struct {
		name          string
		shop          products
		item          string
		price         money
		expectedShop  products
		expectedError error
	}{
		{
			"basic case. empty Item",
			products{},
			"",
			toMoney(0),
			products{"": 0},
			nil,
		},
		{
			"basic case. empty item name",
			products{},
			"",
			toMoney(10),
			products{"": toMoney(10)},
			nil,
		},
		{
			"basic case. already exists",
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"a",
			toMoney(10),
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			productExists,
		},
		{
			"basic case. correct item",
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"xxx",
			toMoney(100),
			products{
				"a":   toMoney(1),
				"b":   toMoney(10),
				"xxx": toMoney(100),
			},
			nil,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			err := tc.shop.addProduct(tc.item, tc.price)

			if !errors.Is(err, tc.expectedError) {
				t.Errorf("got\t\t%v\nwant\t%v", err, tc.expectedError)
			}
			if !reflect.DeepEqual(tc.shop, tc.expectedShop) {
				t.Fatalf("got\t\t%v\nwant\t%v", tc.shop, tc.expectedShop)
			}
		})
	}
}

func TestChangePrice(t *testing.T) {
	testCases := []struct {
		name          string
		shop          products
		item          string
		price         money
		expectedShop  products
		expectedError error
	}{
		{
			"basic case. empty Item",
			products{},
			"",
			toMoney(0),
			products{},
			productNotExists,
		},
		{
			"basic case. empty item name",
			products{},
			"",
			toMoney(10),
			products{},
			productNotExists,
		},
		{
			"basic case. correct price change",
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"a",
			toMoney(10),
			products{
				"a": toMoney(10),
				"b": toMoney(10),
			},
			nil,
		},
		{
			"basic case. correct price change",
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"xxx",
			toMoney(10),
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			productNotExists,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			err := tc.shop.changePrice(tc.item, tc.price)

			if !errors.Is(err, tc.expectedError) {
				t.Errorf("got\t\t%v\nwant\t%v", err, tc.expectedError)
			}
			if !reflect.DeepEqual(tc.shop, tc.expectedShop) {
				t.Fatalf("got\t\t%v\nwant\t%v", tc.shop, tc.expectedShop)
			}
		})
	}
}

func TestChangeName(t *testing.T) {
	testCases := []struct {
		name          string
		shop          products
		itemName      string
		newItemName   string
		expectedShop  products
		expectedError error
	}{
		{
			"basic case. empty Item",
			products{},
			"a",
			"",
			products{},
			productNotExists,
		},
		{
			"basic case. empty item name",
			products{
				"a": toMoney(10),
			},
			"a",
			"",
			products{
				"": toMoney(10),
			},
			nil,
		},
		{
			"basic case. already exists",
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"a",
			"aa",
			products{
				"aa": toMoney(1),
				"b":  toMoney(10),
			},
			nil,
		},
		{
			"basic case. already exists",
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"xxx",
			"aa",
			products{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			productNotExists,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			err := tc.shop.changeName(tc.itemName, tc.newItemName)

			if !errors.Is(err, tc.expectedError) {
				t.Errorf("got\t\t%v\nwant\t%v", err, tc.expectedError)
			}
			if !reflect.DeepEqual(tc.shop, tc.expectedShop) {
				t.Fatalf("got\t\t%v\nwant\t%v", tc.shop, tc.expectedShop)
			}
		})
	}
}

func TestAddAccount(t *testing.T) {
	testCases := []struct {
		name             string
		accounts         accounts
		account          string
		balance          money
		expectedAccounts accounts
		expectedError    error
	}{
		{
			"basic case. empty Account",
			accounts{},
			"",
			toMoney(0),
			accounts{"": toMoney(0)},
			nil,
		},
		{
			"basic case. empty Account name",
			accounts{},
			"",
			toMoney(10),
			accounts{"": toMoney(10)},
			nil,
		},
		{
			"basic case. already exists",
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"a",
			toMoney(10),
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			accountExists,
		},
		{
			"basic case. correct item",
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"xxx",
			toMoney(100),
			accounts{
				"a":   toMoney(1),
				"b":   toMoney(10),
				"xxx": toMoney(100),
			},
			nil,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			err := tc.accounts.addAccount(tc.account, tc.balance)

			if !errors.Is(err, tc.expectedError) {
				t.Errorf("got\t\t%v\nwant\t%v", err, tc.expectedError)
			}
			if !reflect.DeepEqual(tc.accounts, tc.expectedAccounts) {
				t.Fatalf("got\t\t%v\nwant\t%v", tc.account, tc.expectedAccounts)
			}
		})
	}
}

func TestChangeBalance(t *testing.T) {
	testCases := []struct {
		name             string
		accounts         accounts
		account          string
		balance          money
		expectedAccounts accounts
		expectedError    error
	}{
		{
			"basic case. empty Account",
			accounts{},
			"",
			toMoney(0),
			accounts{},
			accountNotExists,
		},
		{
			"basic case. empty Account name",
			accounts{},
			"",
			toMoney(10),
			accounts{},
			accountNotExists,
		},
		{
			"basic case. correct balance change",
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"a",
			toMoney(10),
			accounts{
				"a": toMoney(10),
				"b": toMoney(10),
			},
			nil,
		},
		{
			"basic case. correct balance change. the same balance",
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"a",
			toMoney(1),
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			nil,
		},
		{
			"unknown",
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			"xxx",
			toMoney(1),
			accounts{
				"a": toMoney(1),
				"b": toMoney(10),
			},
			accountNotExists,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			err := tc.accounts.changeBalance(tc.account, tc.balance)

			if !errors.Is(err, tc.expectedError) {
				t.Errorf("got\t\t%v\nwant\t%v", err, tc.expectedError)
			}
			if !reflect.DeepEqual(tc.accounts, tc.expectedAccounts) {
				t.Fatalf("got\t\t%v\nwant\t%v", tc.accounts, tc.expectedAccounts)
			}
		})
	}
}

type SortBy uint8

const SortByNameAsc = 0
const SortByNameDesc = 1
const SortByBalanceAsc = 2
const SortByBalanceDesc = 3

func TestSortAccounts(t *testing.T) {
	testCases := []struct {
		name             string
		accounts         accounts
		sortBy           SortBy
		expectedAccounts []string
	}{
		// name
		{
			"SortByNameAsc. empty Accounts",
			accounts{},
			SortByNameAsc,
			[]string{},
		},
		{
			"SortByNameAsc. single Account ",
			accounts{
				"a": toMoney(10),
			},
			SortByNameAsc,
			[]string{"a"},
		},
		{
			"SortByNameAsc. already sorted",
			accounts{
				"a":     toMoney(1),
				"b":     toMoney(10),
				"d":     toMoney(12),
				"d10":   toMoney(11),
				"xxx_1": toMoney(22),
			},
			SortByNameAsc,
			[]string{
				"a",
				"b",
				"d",
				"d10",
				"xxx_1",
			},
		},
		{
			"SortByNameAsc. already sorted in reverse order",
			accounts{
				"xxx_1": toMoney(22),
				"d10":   toMoney(11),
				"d":     toMoney(12),
				"b":     toMoney(10),
				"a":     toMoney(1),
			},
			SortByNameAsc,
			[]string{
				"a",
				"b",
				"d",
				"d10",
				"xxx_1",
			},
		},
		{
			"SortByNameAsc. random order",
			accounts{
				"d10":   toMoney(11),
				"a":     toMoney(1),
				"xxx_1": toMoney(22),
				"b":     toMoney(10),
				"d":     toMoney(12),
			},
			SortByNameAsc,
			[]string{
				"a",
				"b",
				"d",
				"d10",
				"xxx_1",
			},
		},

		{
			"SortByNameDesc. empty Accounts",
			accounts{},
			SortByNameDesc,
			[]string{},
		},
		{
			"SortByNameDesc. single Account ",
			accounts{
				"a": toMoney(10),
			},
			SortByNameDesc,
			[]string{"a"},
		},
		{
			"SortByNameDesc. already sorted",
			accounts{
				"xxx_1": toMoney(22),
				"d10":   toMoney(11),
				"d":     toMoney(12),
				"b":     toMoney(10),
				"a":     toMoney(1),
			},
			SortByNameDesc,
			[]string{
				"xxx_1",
				"d10",
				"d",
				"b",
				"a",
			},
		},
		{
			"SortByNameDesc. already sorted in reverse order",
			accounts{
				"a":     toMoney(1),
				"b":     toMoney(10),
				"d":     toMoney(12),
				"d10":   toMoney(11),
				"xxx_1": toMoney(22),
			},
			SortByNameDesc,
			[]string{
				"xxx_1",
				"d10",
				"d",
				"b",
				"a",
			},
		},
		{
			"SortByNameDesc. random order",
			accounts{
				"d10":   toMoney(11),
				"a":     toMoney(1),
				"xxx_1": toMoney(22),
				"b":     toMoney(10),
				"d":     toMoney(12),
			},
			SortByNameDesc,
			[]string{
				"xxx_1",
				"d10",
				"d",
				"b",
				"a",
			},
		},

		// balance
		{
			"SortByBalanceAsc. empty Accounts",
			accounts{},
			SortByBalanceAsc,
			[]string{},
		},
		{
			"SortByBalanceAsc. single Account",
			accounts{
				"a": toMoney(10),
			},
			SortByBalanceAsc,
			[]string{"a"},
		},
		{
			"SortByBalanceAsc. already sorted",
			accounts{
				"a":     toMoney(1),
				"b":     toMoney(10),
				"d10":   toMoney(11),
				"d":     toMoney(12),
				"xxx_1": toMoney(22),
			},
			SortByBalanceAsc,
			[]string{
				"a",
				"b",
				"d10",
				"d",
				"xxx_1",
			},
		},
		{
			"SortByBalanceAsc. already sorted with duplicates",
			accounts{
				"a":     toMoney(1),
				"b":     toMoney(10),
				"d10":   toMoney(11),
				"d11":   toMoney(11),
				"d":     toMoney(12),
				"xxx_1": toMoney(22),
				"xxx_2": toMoney(22),
			},
			SortByBalanceAsc,
			[]string{
				"a",
				"b",
				"d10",
				"d11",
				"d",
				"xxx_1",
				"xxx_2",
			},
		},
		{
			"SortByBalanceAsc. already sorted in reverse order",
			accounts{
				"xxx_1": toMoney(22),
				"d":     toMoney(12),
				"d10":   toMoney(11),
				"b":     toMoney(10),
				"a":     toMoney(1),
			},
			SortByBalanceAsc,
			[]string{
				"a",
				"b",
				"d10",
				"d",
				"xxx_1",
			},
		},
		{
			"SortByBalanceAsc. already sorted in reverse order with duplicated",
			accounts{
				"xxx_2": toMoney(22),
				"xxx_1": toMoney(22),
				"d":     toMoney(12),
				"d11":   toMoney(11),
				"d10":   toMoney(11),
				"b":     toMoney(10),
				"a":     toMoney(1),
			},
			SortByBalanceAsc,
			[]string{
				"a",
				"b",
				"d10",
				"d11",
				"d",
				"xxx_1",
				"xxx_2",
			},
		},
		{
			"SortByBalanceAsc. random order",
			accounts{
				"d10":   toMoney(11),
				"a":     toMoney(1),
				"xxx_1": toMoney(22),
				"b":     toMoney(10),
				"d":     toMoney(12),
			},
			SortByBalanceAsc,
			[]string{
				"a",
				"b",
				"d10",
				"d",
				"xxx_1",
			},
		},
		{
			"SortByBalanceAsc. random order with duplicates",
			accounts{
				"d10":   toMoney(11),
				"a":     toMoney(1),
				"a1":    toMoney(1),
				"xxx_1": toMoney(22),
				"xxx_2": toMoney(22),
				"b":     toMoney(10),
				"d":     toMoney(12),
			},
			SortByBalanceAsc,
			[]string{
				"a",
				"a1",
				"b",
				"d10",
				"d",
				"xxx_1",
				"xxx_2",
			},
		},

		{
			"SortByBalanceDesc. empty Accounts",
			accounts{},
			SortByBalanceDesc,
			[]string{},
		},
		{
			"SortByBalanceDesc. single Account",
			accounts{
				"a": toMoney(10),
			},
			SortByBalanceDesc,
			[]string{"a"},
		},
		{
			"SortByBalanceDesc. already sorted",
			accounts{
				"xxx_1": toMoney(22),
				"d":     toMoney(12),
				"d10":   toMoney(11),
				"b":     toMoney(10),
				"a":     toMoney(1),
			},
			SortByBalanceDesc,
			[]string{
				"xxx_1",
				"d",
				"d10",
				"b",
				"a",
			},
		},
		{
			"SortByBalanceDesc. already sorted with duplicates",
			accounts{
				"xxx_1": toMoney(22),
				"xxx_2": toMoney(22),
				"d":     toMoney(12),
				"d11":   toMoney(11),
				"d10":   toMoney(11),
				"b":     toMoney(10),
				"a":     toMoney(1),
			},
			SortByBalanceDesc,
			[]string{
				"xxx_2",
				"xxx_1",
				"d",
				"d11",
				"d10",
				"b",
				"a",
			},
		},
		{
			"SortByBalanceDesc. already sorted in reverse order",
			accounts{
				"a":     toMoney(1),
				"b":     toMoney(10),
				"d10":   toMoney(11),
				"d":     toMoney(12),
				"xxx_1": toMoney(22),
			},
			SortByBalanceDesc,
			[]string{
				"xxx_1",
				"d",
				"d10",
				"b",
				"a",
			},
		},
		{
			"SortByBalanceDesc. already sorted in reverse order with duplicated",
			accounts{
				"a":     toMoney(1),
				"b":     toMoney(1),
				"d10":   toMoney(11),
				"d11":   toMoney(11),
				"d":     toMoney(12),
				"xxx_1": toMoney(22),
				"xxx_2": toMoney(22),
			},
			SortByBalanceDesc,
			[]string{
				"xxx_2",
				"xxx_1",
				"d",
				"d11",
				"d10",
				"b",
				"a",
			},
		},
		{
			"SortByBalanceDesc. random order",
			accounts{
				"d10":   toMoney(11),
				"a":     toMoney(1),
				"xxx_1": toMoney(22),
				"b":     toMoney(10),
				"d":     toMoney(12),
			},
			SortByBalanceDesc,
			[]string{
				"xxx_1",
				"d",
				"d10",
				"b",
				"a",
			},
		},
		{
			"SortByBalanceDesc. random order with duplicates",
			accounts{
				"d10":   toMoney(11),
				"a":     toMoney(1),
				"a1":    toMoney(1),
				"xxx_1": toMoney(22),
				"xxx_2": toMoney(22),
				"b":     toMoney(10),
				"d":     toMoney(12),
			},
			SortByBalanceDesc,
			[]string{
				"xxx_2",
				"xxx_1",
				"d",
				"d10",
				"b",
				"a1",
				"a",
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			var result []string

			switch tc.sortBy {
			case SortByNameAsc:
				result = tc.accounts.sortNormal()
			case SortByNameDesc:
				result = tc.accounts.sortReverse()
			case SortByBalanceAsc:
				result = tc.accounts.sortByMoney()
			case SortByBalanceDesc:
				result = tc.accounts.sortByMoneyReverse()
			}

			if !reflect.DeepEqual(result, tc.expectedAccounts) {
				t.Fatalf("got\t\t%v\nwant\t%v", result, tc.expectedAccounts)
			}
		})
	}
}
