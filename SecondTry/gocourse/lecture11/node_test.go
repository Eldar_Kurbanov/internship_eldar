package bc

import (
	"crypto/ed25519"
	"fmt"
	"reflect"
	"testing"
	"time"
)

func createValidatorAndNode(gen *Genesis) (*Node, *Node, error) {
	addr, key, err := ed25519.GenerateKey(nil)
	if err != nil {
		return nil, nil, err
	}

	gen.Validators = append(gen.Validators, addr)

	vd, err := NewNode(key, *gen)
	if err != nil {
		return nil, nil, err
	}

	_, key, err = ed25519.GenerateKey(nil)
	if err != nil {
		return nil, nil, err
	}

	nd, err := NewNode(key, *gen)
	if err != nil {
		return nil, nil, err
	}

	err = vd.AddPeer(nd)
	if err != nil {
		return nil, nil, err
	}

	return vd, nd, nil
}

func signTx(tx Transaction) (Transaction, error) {
	pub, pr, err := ed25519.GenerateKey(nil)
	if err != nil {
		return Transaction{}, nil
	}

	tx.PubKey = pub

	b, err := tx.Bytes()
	if err != nil {
		return Transaction{}, err
	}

	if pr == nil {
		return Transaction{}, ErrNoPrivateKey
	}

	tx.Signature = ed25519.Sign(pr, b)

	return tx, nil
}

/* -- Blockchain ---------------------------------------------------------------------------------------------------- */
func TestNode_InsertBlockSuccess(t *testing.T) {
	gen := &Genesis{}
	gen.Alloc = map[string]uint64{
		"one":   20,
		"two":   30,
		"three": 40,
	}

	vd, nd, err := createValidatorAndNode(gen)
	if err != nil {
		t.Fatal(err)
	}

	tr := Transaction{
		From:   "one",
		To:     "two",
		Amount: 10,
		Fee:    1,
	}

	tr, err = signTx(tr)
	if err != nil {
		t.Fatal(err)
	}

	err = vd.AddTransaction(tr)

	if err != nil {
		t.Fatal(err)
	}

	res := <-nd.txProcessedChan
	if !res {
		t.Fatal("Transaction must be succeed")
	}

	if v, _ := nd.GetBalance("one"); v != 9 {
		t.Fatalf("wrong 'one' state: get=%v want=%v'", v, 9)
	}

	if v, _ := nd.GetBalance("two"); v != 40 {
		t.Fatalf("wrong 'two' state: get=%v want=%v'", v, 40)
	}

	if v, _ := nd.GetBalance(vd.NodeAddress()); v != 1 {
		t.Fatalf("wrong 'val' state: get=%v want=%v'", v, 1)
	}
}

func TestNode_ApplyTransactionSuccess(t *testing.T) {
	gen := Genesis{}
	gen.Alloc = map[string]uint64{
		"one":   20,
		"two":   30,
		"three": 40,
	}

	vd, nd, err := createValidatorAndNode(&gen)
	if err != nil {
		t.Fatal(err)
	}

	tr := Transaction{
		From:   "one",
		To:     "two",
		Amount: 10,
		Fee:    1,
	}

	tr, err = signTx(tr)
	if err != nil {
		t.Fatal(err)
	}

	err = vd.AddTransaction(tr)
	if err != nil {
		t.Fatal(err)
	}

	res := <-nd.txProcessedChan
	if !res {
		t.Fatal("Transaction must be succeed")
	}

	if v, _ := nd.GetBalance("one"); v != 9 {
		t.Fatalf("wrong 'one' state: get=%v want=%v'", v, 9)
	}

	if v, _ := nd.GetBalance("two"); v != 40 {
		t.Fatalf("wrong 'two' state: get=%v want=%v'", v, 40)
	}

	if v, _ := nd.GetBalance(vd.NodeAddress()); v != 1 {
		t.Fatalf("wrong 'val' state: get=%v want=%v'", v, 1)
	}
}

func TestNode_IsTransactionSuccess(t *testing.T) {
	gen := Genesis{}
	gen.Alloc = map[string]uint64{
		"one":   20,
		"two":   30,
		"three": 40,
	}

	vd, _, err := createValidatorAndNode(&gen)
	if err != nil {
		t.Fatal(err)
	}

	tr := Transaction{
		From:   "two",
		To:     "one",
		Amount: 10,
		Fee:    1,
	}

	tr, err = signTx(tr)
	if err != nil {
		t.Fatal(err)
	}

	//two success transactions
	for i := 0; i < 2; i++ {
		err = vd.AddTransaction(tr)
		if err != nil {
			t.Fatal(err)
		}

		res := <-vd.txProcessedChan
		if !res {
			t.Fatal("Transaction must be succeed")
		}
	}

	//two failure blocks
	for i := 0; i < 2; i++ {
		err = vd.AddTransaction(tr)
		if err != nil {
			t.Fatal(err)
		}

		res := <-vd.txProcessedChan
		if res {
			t.Fatal("Transaction must be failure")
		}
	}
}

func TestNode_SyncBlockStopBroadcasting(t *testing.T) {
	gen := Genesis{}
	gen.Alloc = map[string]uint64{
		"one":   20,
		"two":   30,
		"three": 40,
	}

	vd, nd, err := createValidatorAndNode(&gen)
	if err != nil {
		t.Fatal(err)
	}

	block, err := vd.MineBlock(0)
	if err != nil {
		t.Fatal(err)
	}

	tr := Transaction{
		From:   "two",
		To:     "one",
		Amount: 10,
		Fee:    1,
	}

	tr, err = signTx(tr)
	if err != nil {
		t.Fatal(err)
	}

	block.Transactions = append(block.Transactions, tr)

	//broadcast after receiving block
	vd.peers[nd.address].In <- Message{From: "abc", Data: *block}

	time.Sleep(1 * time.Second)

	//send same block
	vd.peers[nd.address].In <- Message{From: "abc", Data: *block}

	time.Sleep(1 * time.Second)

	select {
	case b := <-vd.peers[nd.address].Out:
		if reflect.DeepEqual(b, block) {
			t.Fatalf("endless broadcasting")
		}

		t.Fatalf("received phantom message: %v", b)
	case <-time.After(time.Millisecond * 5):
		//no messages - test passed
		return
	}
}

///* -- Network ------------------------------------------------------------------------------------------------------- */
func TestNode_SyncTwoNodes(t *testing.T) {
	gen := Genesis{}
	gen.Alloc = map[string]uint64{
		"one": 200,
		"two": 50,
	}

	_, key, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Fatal(err)
	}

	vd, nd2, err := createValidatorAndNode(&gen)
	if err != nil {
		t.Fatal(err)
	}

	nd1, err := NewNode(key, gen)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("node1 %v", nd1.NodeAddress())
	t.Logf("node2 %v", nd2.NodeAddress())

	tr := Transaction{
		From:   "one",
		To:     "two",
		Amount: 100,
		Fee:    10,
	}

	tr, err = signTx(tr)
	if err != nil {
		t.Fatal(err)
	}

	err = vd.AddTransaction(tr)
	if err != nil {
		t.Errorf("add transaction error: %v", err)
	}

	res := <-vd.txProcessedChan
	if !res {
		t.Fatal("Transaction must be succeed")
	}

	if err := nd1.AddPeer(nd2); err != nil {
		t.Fatalf("add peer err: %v", err)
	}

	if err := nd1.AddPeer(vd); err != nil {
		t.Fatalf("add peer err: %v", err)
	}

	time.Sleep(1 * time.Second)

	res = <-nd2.txProcessedChan
	if !res {
		t.Fatal("Transaction must be succeed")
	}

	if len(nd2.blocks) != 2 {
		t.Fatalf("no blocks was synced")
	}

	nd1.stateMtx.RLock()
	defer nd1.stateMtx.RUnlock()

	nd2.stateMtx.RLock()
	defer nd2.stateMtx.RUnlock()

	if !reflect.DeepEqual(nd1.state, nd2.state) {
		fmt.Println(nd1.state, nd2.state)
		t.Fatalf("wrong synced state")
	}
}
