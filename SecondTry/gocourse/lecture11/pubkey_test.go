package bc

import (
	"crypto/ed25519"
	"math/rand"
	"testing"
)

func TestPubKeyToAddress(t *testing.T) {
	pubKey, _, _ := ed25519.GenerateKey(rand.New(rand.NewSource(1)))

	addr, err := PubKeyToAddress(pubKey)
	if err != nil {
		t.Fatalf("can't get address from valid key")
	}

	if addr != "171e68f02e6f66bf9ff65c13c75d9b2b492c2f40ed61e06507cb8b227c3970d5" {
		t.Fatalf("wrong address")
	}

	addr, err = PubKeyToAddress([]byte("aaa"))
	if err == nil {
		t.Fatalf("get addr '%v' from invalid key '%v'", addr, []byte("aaa"))
	}
}
