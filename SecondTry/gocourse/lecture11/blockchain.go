package bc

import (
	"context"
	"crypto"
	"crypto/ed25519"
	"errors"
	"fmt"
	"log"
	"reflect"
	"sync"
	"time"
)

const MSGBusLen = 100

func NewNode(key ed25519.PrivateKey, genesis Genesis) (*Node, error) {
	address, err := PubKeyToAddress(key.Public())
	if err != nil {
		return nil, err
	}

	var blocks []Block

	state := make(map[string]uint64)
	state[address] = 0

	node := &Node{
		key:             key,
		address:         address,
		genesis:         genesis,
		blocks:          blocks,
		lastBlockNum:    0,
		peers:           make(map[string]connectedPeer),
		state:           state,
		transactionPool: make(map[string]Transaction),
		txProcessedChan: make(chan bool),
	}

	genesisBlock := genesis.ToBlock()

	err = node.insertBlock(&genesisBlock)

	if err != nil {
		return nil, err
	}

	go node.MineLoop(context.Background())

	return node, err
}

type Node struct {
	key     ed25519.PrivateKey
	address string
	genesis Genesis

	lastBlockNum    uint64
	lastBlockNumMtx sync.RWMutex

	//state
	blocks    []Block
	blocksMtx sync.RWMutex

	//peer address - > peer info
	peers    map[string]connectedPeer
	peersMtx sync.RWMutex

	//hash(state) - хеш от упорядоченного слайса ключ-значение
	state     map[string]uint64
	hashState string
	stateMtx  sync.RWMutex

	//transaction hash - > transaction
	transactionPool    map[string]Transaction
	transactionPoolMtx sync.RWMutex
	txProcessedChan    chan bool

	syncMtx sync.RWMutex
}

func (c *Node) NodeKey() crypto.PublicKey {
	return c.key.Public()
}

func (c *Node) Connection(address string, in chan Message, out chan Message, pubKey crypto.PublicKey) chan Message {
	if out == nil {
		out = make(chan Message, MSGBusLen)
	}

	ctx, cancel := context.WithCancel(context.Background())

	c.peersMtx.Lock()
	c.peers[address] = connectedPeer{
		Address: address,
		Out:     out,
		In:      in,
		cancel:  cancel,
		PubKey:  pubKey,
	}
	c.peersMtx.Unlock()

	c.peersMtx.RLock()
	defer c.peersMtx.RUnlock()

	c.stateMtx.RLock()
	_, ok := c.state[address]
	c.stateMtx.RUnlock()

	if !ok {
		c.stateMtx.Lock()
		c.state[address] = 0
		c.stateMtx.Unlock()
	}

	go c.peerLoop(ctx, c.peers[address])

	return c.peers[address].Out
}

func (c *Node) MineLoop(ctx context.Context) {
	var validatorNum = -1

	for i, pubKey := range c.genesis.Validators {
		address, err := PubKeyToAddress(pubKey)
		if err != nil {
			panic("Genesis contains wrong public key")
		}

		if c.address == address {
			validatorNum = i
		}
	}

	if validatorNum == -1 {
		// we are not validator
		return
	}

	fmt.Printf("Validator %v starts mining loop\n", c.address)

	for {
		//c.syncMtx.RLock()
		select {
		case <-ctx.Done():
			return
		default:
			c.syncMtx.RLock()
			c.lastBlockNumMtx.RLock()
			c.transactionPoolMtx.RLock()
			if c.lastBlockNum%uint64(len(c.genesis.Validators)) != uint64(validatorNum) ||
				len(c.transactionPool) == 0 {
				c.lastBlockNumMtx.RUnlock()
				c.transactionPoolMtx.RUnlock()
				c.syncMtx.RUnlock()
				time.Sleep(MineBlockTimeout)

				continue
			}
			c.lastBlockNumMtx.RUnlock()
			c.transactionPoolMtx.RUnlock()

			block, err := c.MineBlock(uint64(validatorNum))
			if err != nil {
				log.Println("Failed to mine block: " + err.Error())
				c.syncMtx.RUnlock()

				continue
			}

			log.Println(c.address, "mined block", block.BlockNum)

			err = c.insertBlock(block)
			if err != nil {
				log.Println("Failed to insert mined block: " + err.Error())
				c.syncMtx.RUnlock()

				continue
			}

			c.Broadcast(ctx, Message{c.address, *block})
			c.syncMtx.RUnlock()
		}
		//c.syncMtx.RUnlock()
	}
}

func (c *Node) MineBlock(validatorNum uint64) (*Block, error) {
	c.lastBlockNumMtx.Lock()
	defer c.lastBlockNumMtx.Unlock()
	c.blocksMtx.Lock()
	defer c.blocksMtx.Unlock()
	c.transactionPoolMtx.Lock()
	defer c.transactionPoolMtx.Unlock()
	c.stateMtx.RLock()
	defer c.stateMtx.RUnlock()

	if c.lastBlockNum%uint64(len(c.genesis.Validators)) != validatorNum {
		return nil, ErrValidatorWrongOrder
	}

	transactions := make([]Transaction, len(c.transactionPool))

	var i = 0

	for _, tx := range c.transactionPool {
		transactions[i] = tx
		i++
	}

	block := Block{
		BlockNum:      c.lastBlockNum + 1,
		Timestamp:     time.Now().Unix(),
		Transactions:  transactions,
		PrevBlockHash: c.blocks[c.lastBlockNum].BlockHash,
		StateHash:     c.hashState,
		Validator:     c.address,
		PubKey:        c.key.Public(),
	}

	blockBytes, err := Bytes(block)
	if err != nil {
		return nil, err
	}

	block.Signature = ed25519.Sign(c.key, blockBytes)

	block.BlockHash, err = block.Hash()
	if err != nil {
		return nil, err
	}

	return &block, nil
}

//func (c *Node) verifyBlock(b *Block) bool {
//	blockBytes, err := Bytes(*b)
//	if err != nil {
//		return false
//	}
//
//	return ed25519.Verify(b.PubKey.(ed25519.PublicKey), blockBytes, b.Signature)
//}

func (c *Node) checkConnected(remoteAddress string) bool {
	c.peersMtx.RLock()
	defer c.peersMtx.RUnlock()

	_, ok := c.peers[remoteAddress]

	return ok
}

func (c *Node) AddPeer(peer Blockchain) error {
	remoteAddress, err := PubKeyToAddress(peer.NodeKey())
	if err != nil {
		return err
	}

	if c.address == remoteAddress {
		return ErrSelfConnection
	}

	if c.checkConnected(remoteAddress) {
		return nil
	}

	//_, ok := c.state[remoteAddress]
	//if !ok {
	//	c.state[remoteAddress] = 0
	//}

	out := make(chan Message, MSGBusLen)
	in := peer.Connection(c.address, out, nil, c.NodeKey())

	c.Connection(remoteAddress, in, out, peer.NodeKey())

	return nil
}

func (c *Node) sendNodeInfoResp(ctx context.Context, peer connectedPeer) {
	c.lastBlockNumMtx.RLock()
	c.stateMtx.RLock()
	defer c.lastBlockNumMtx.RUnlock()
	defer c.stateMtx.RUnlock()
	peer.Send(ctx, Message{
		From: c.address,
		Data: NodeInfoResp{
			c.address,
			c.lastBlockNum,
			&c.genesis,
			c.hashState,
		},
	})
}

func (c *Node) handShake(ctx context.Context, peer connectedPeer) error {
	c.sendNodeInfoResp(ctx, peer)

	for {
		select {
		case <-ctx.Done():
			return context.Canceled
		case msg := <-peer.In:
			switch m := msg.Data.(type) {
			case NodeInfoResp:
				result := c.processNodeInfoResp(m, peer)
				peer.Send(ctx, Message{
					c.address,
					result,
				})

				if result.err != nil {
					return result.err
				}
			case NodeInfoHandShakeResult:
				return m.err
			}
		}
	}
}

func (c *Node) peerLoop(ctx context.Context, peer connectedPeer) {
	c.syncMtx.Lock()

	err := c.handShake(ctx, peer)
	if err != nil {
		fmt.Printf("Failed handshake with %v. err = %v", peer.Address, err)
		return
	}

	c.lastBlockNumMtx.RLock()
	if c.lastBlockNum > peer.lastBlockNum {
		c.blocksMtx.RLock()
		for i := peer.lastBlockNum + 1; i <= c.lastBlockNum; i++ {
			peer.Send(ctx, Message{c.address, c.blocks[i]})
		}
		c.blocksMtx.RUnlock()
	}
	c.lastBlockNumMtx.RUnlock()

	c.syncMtx.Unlock()

	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-peer.In:
			err := c.processMessage(msg)
			if err != nil {
				if !errors.Is(err, ErrBlockAlreadyApplied) {
					log.Println("Process peer error", err)
				}

				if errors.Is(err, ErrNeedHandshake) {
					err := c.handShake(ctx, peer)
					if err != nil {
						fmt.Printf("Failed handshake with %v. err = %v", peer.Address, err)
						return
					}
				}

				continue
			}

			//broadcast to connected peers
			c.Broadcast(ctx, msg)
		}
	}
}

func (c *Node) processMessage(msg Message) error {
	switch m := msg.Data.(type) {
	case Block:
		//if m.BlockNum == 0 {
		//	fmt.Println("ALARM")
		//}
		return c.insertBlock(&m)
	default:
		fmt.Printf("got %T: ", m)
		return ErrNoHandlerMsg
	}
	//return true, nil, ErrNoHandlerMsg
}

func (c *Node) processNodeInfoResp(nodeInfo NodeInfoResp, peer connectedPeer) NodeInfoHandShakeResult {
	if !reflect.DeepEqual(*nodeInfo.genesis, c.genesis) {
		fmt.Printf("Genesis of %v are not the same with node %v\n", nodeInfo.NodeName, c.address)
		fmt.Printf("Genesis 1: %v\n Genesis 2: %v\n", *nodeInfo.genesis, c.genesis)

		return NodeInfoHandShakeResult{ErrDiffGenesis}
	}

	peer.lastBlockNum = nodeInfo.BlockNum

	return NodeInfoHandShakeResult{nil}
}

func (c *Node) Broadcast(ctx context.Context, msg Message) {
	c.peersMtx.RLock()
	defer c.peersMtx.RUnlock()

	for _, v := range c.peers {
		if v.Address != c.address {
			v.Send(ctx, msg)
		}
	}
}

func (c *Node) RemovePeer(peer Blockchain) error {
	c.peersMtx.Lock()
	defer c.peersMtx.Unlock()

	conPeer, ok := c.peers[peer.NodeAddress()]
	if !ok {
		return ErrNoPeerFound
	}

	// FIXME Throw error when we try remove an validator

	conPeer.cancel()

	delete(c.peers, peer.NodeAddress())

	return nil
}

func (c *Node) GetBalance(account string) (uint64, error) {
	c.stateMtx.RLock()
	defer c.stateMtx.RUnlock()

	balance, ok := c.state[account]
	if !ok {
		return 0, ErrNoPeerFound
	}

	return balance, nil
}

// TODO validate transaction
func (c *Node) AddTransaction(transaction Transaction) error {
	hash, err := transaction.Hash()
	if err != nil {
		return err
	}

	c.transactionPoolMtx.Lock()
	defer c.transactionPoolMtx.Unlock()

	_, exists := c.transactionPool[hash]
	if exists {
		return ErrTxAlreadyInPool
	}

	c.transactionPool[hash] = transaction

	return nil
}

func (c *Node) GetBlockByNumber(ID uint64) Block {
	c.blocksMtx.RLock()
	defer c.blocksMtx.RUnlock()

	if uint64(len(c.blocks)) > ID {
		return c.blocks[ID]
	}

	return Block{}
}

func (c *Node) NodeInfo() NodeInfoResp {
	c.lastBlockNumMtx.RLock()
	c.stateMtx.RLock()

	defer c.lastBlockNumMtx.RUnlock()
	defer c.stateMtx.RUnlock()

	return NodeInfoResp{
		c.address,
		c.lastBlockNum,
		&c.genesis,
		c.hashState,
	}
}

func (c *Node) NodeAddress() string {
	return c.address
}

func (c *Node) SignTransaction(transaction Transaction) (Transaction, error) {
	b, err := transaction.Bytes()
	if err != nil {
		return Transaction{}, err
	}

	if c.key == nil {
		return Transaction{}, ErrNoPrivateKey
	}

	transaction.Signature = ed25519.Sign(c.key, b)

	return transaction, nil
}

func (c *Node) insertBlock(b *Block) (err error) {
	c.lastBlockNumMtx.Lock()
	defer c.lastBlockNumMtx.Unlock()
	c.blocksMtx.Lock()
	defer c.blocksMtx.Unlock()
	c.transactionPoolMtx.Lock()
	defer c.transactionPoolMtx.Unlock()

	if b.BlockNum != 0 && c.lastBlockNum >= b.BlockNum {
		return ErrBlockAlreadyApplied
	}

	c.stateMtx.Lock()
	defer c.stateMtx.Unlock()

	stateCopy := make(map[string]uint64)
	for s, u := range c.state {
		stateCopy[s] = u
	}

	txPoolCopy := make(map[string]Transaction)
	for h, tx := range c.transactionPool {
		txPoolCopy[h] = tx
	}

	defer func() {
		if err != nil && !errors.Is(err, ErrNoAppliedTxs) {
			c.state = stateCopy
			c.transactionPool = txPoolCopy
		}
	}()

	skippedTxs := make([]int, 0)
	appliedTxCount := 0

	for i, tx := range b.Transactions {
		if b.BlockNum != 0 {
			currBalanceS, ok := c.state[tx.From]
			if !ok && b.BlockNum != 0 {
				return ErrAccountNotExists
			}

			resultBalanceS := currBalanceS - tx.Amount - tx.Fee

			txBytes, err := tx.Bytes()
			if err != nil {
				return err
			}

			c.peersMtx.RLock()
			sender, ok := c.peers[tx.From]
			c.peersMtx.RUnlock()

			//FIXME Potentially security risk: when don't know about sender - we trust the transaction public key
			pubKey := (map[bool]crypto.PublicKey{true: sender.PubKey, false: tx.PubKey})[ok].(ed25519.PublicKey)

			signed := ed25519.Verify(pubKey, txBytes, tx.Signature)
			if !signed {
				err = ErrInvalidSignTx
			}

			if currBalanceS < resultBalanceS {
				err = ErrAccountNoMoney
			}

			if err != nil {
				fmt.Printf("%v\n", err)

				txHash, err := tx.Hash()
				if err != nil {
					return err
				}

				skippedTxs = append(skippedTxs, i)

				delete(c.transactionPool, txHash)

				continue
			}

			currBalanceV, ok := c.state[b.Validator]
			if !ok && b.BlockNum != 0 {
				return ErrValidatorNotFound
			}

			resultBalanceV := currBalanceV + tx.Fee

			c.state[tx.From] = resultBalanceS
			c.state[b.Validator] = resultBalanceV
		}

		currBalanceG, ok := c.state[tx.To]
		if !ok && b.BlockNum != 0 {
			return ErrAccountNotExists
		}

		resultBalanceG := currBalanceG + tx.Amount

		c.state[tx.To] = resultBalanceG

		txHash, err := tx.Hash()
		if err != nil {
			return err
		}

		delete(c.transactionPool, txHash)

		appliedTxCount++
	}

	for _, i := range skippedTxs {
		b.Transactions = append(b.Transactions[:i], b.Transactions[i+1:]...)

		c.notifyTxChan(1, false)
	}

	if appliedTxCount == 0 {
		// all transaction was not applied. we shouldn't create the block
		return ErrNoAppliedTxs
	}

	if b.BlockNum != 0 {
		c.lastBlockNum++
	}

	//if uint64(len(c.blocks)) != b.BlockNum {
	//	fmt.Println("Block num fail")
	//}

	c.blocks = append(c.blocks, *b)

	fmt.Println(c.address, "applied the block number", b.BlockNum)

	if b.BlockNum != 0 {
		c.notifyTxChan(appliedTxCount, true)
	}

	return nil
}

func (c *Node) notifyTxChan(quantity int, result bool) {
	go func() {
		for i := 0; i < quantity; i++ {
			c.txProcessedChan <- result
		}
	}()
}
