package bc

import (
	"context"
	"crypto"
	"crypto/ed25519"
	"encoding/json"
	"fmt"
	"internship_eldar/SecondTry/myerror"
	"sort"
	"time"
)

const (
	SendTimeout      = 1 * time.Second
	MineBlockTimeout = 100 * time.Microsecond

	ErrDiffGenesis     = myerror.MyError("DifferentGenesis")
	ErrYouNeedBlocks   = myerror.MyError("Hey, you need a blocks!")
	ErrNotEnoughBlocks = myerror.MyError("NotEnoughBlocks")
	ErrDiffHashState   = myerror.MyError("DifferentHashState")

	ErrNoHandlerMsg = myerror.MyError("No handler for this message type")
	ErrNoPeerFound  = myerror.MyError("No peer found")

	ErrTxAlreadyInPool = myerror.MyError("Transaction already in pool")
	ErrNoSuchBlocks    = myerror.MyError("No such block in blockchain")

	ErrBlockAlreadyApplied = myerror.MyError("Block already applied")
	ErrAccountNotExists    = myerror.MyError("Account not exists")
	ErrAccountNoMoney      = myerror.MyError("Account has no enough money")
	ErrValidatorNotFound   = myerror.MyError("Validator not found")
	ErrValidatorWrongOrder = myerror.MyError("Wrong order of validators when mine block")

	ErrNoPrivateKey  = myerror.MyError("No private key")
	ErrNeedHandshake = myerror.MyError("Handshake is needed")

	ErrNoAppliedTxs  = myerror.MyError("No applied transactions in this block")
	ErrInvalidSignTx = myerror.MyError("Invalid sign on transaction!")

	ErrEmptyBlock     = myerror.MyError("Empty block")
	ErrSelfConnection = myerror.MyError("Node self connection")
	ErrIncorrectKey   = myerror.MyError("Incorrect key")
)

type Block struct {
	BlockNum      uint64
	Timestamp     int64
	Transactions  []Transaction
	BlockHash     string `json:"-"`
	PrevBlockHash string
	StateHash     string
	Signature     []byte `json:"-"`
	Validator     string
	PubKey        crypto.PublicKey
}

func (bl *Block) Hash() (string, error) {
	if bl == nil {
		return "", ErrEmptyBlock
	}

	b, err := Bytes(bl)
	if err != nil {
		return "", err
	}

	return Hash(b)
}

type Transaction struct {
	From   string
	To     string
	Amount uint64
	Fee    uint64
	PubKey ed25519.PublicKey

	Signature []byte `json:"-"`
}

func (t Transaction) Hash() (string, error) {
	b, err := Bytes(t)
	if err != nil {
		return "", err
	}

	return Hash(b)
}

func (t Transaction) Bytes() ([]byte, error) {
	return json.Marshal(t)
}

// first block with blockchain settings
type Genesis struct {
	//Account -> funds
	Alloc map[string]uint64
	//list of validators public keys
	Validators []crypto.PublicKey
}

func (g Genesis) ToBlock() Block {
	transactions := make([]Transaction, 0, len(g.Alloc))

	keys := make([]string, 0, len(g.Alloc))
	for k := range g.Alloc {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, account := range keys {
		tx := Transaction{
			"0",
			account,
			g.Alloc[account],
			0,
			nil,
			nil,
		}
		transactions = append(transactions, tx)
	}

	return Block{
		0,
		0,
		transactions,
		"",
		"",
		"",
		nil,
		"",
		nil,
	}
	// алфавитный порядок порядок genesis.Alloc
	//return Block{}
}

type Message struct {
	From string
	Data interface{}
}

type NodeInfoResp struct {
	NodeName  string
	BlockNum  uint64
	genesis   *Genesis
	hashState string
}

type NodeInfoHandShakeResult struct {
	err error
}

type connectedPeer struct {
	Address      string
	In           chan Message
	Out          chan Message
	cancel       context.CancelFunc
	lastBlockNum uint64
	PubKey       crypto.PublicKey
}

func (cp connectedPeer) Send(ctx context.Context, m Message) {
	select {
	case <-ctx.Done():
		fmt.Printf("Context done when send %v from %v to %v", m.Data, m.From, cp.Address)
	case <-time.After(SendTimeout):
		fmt.Printf("Timeout to send %v from %v to %v", m.Data, m.From, cp.Address)
	case cp.Out <- m:
		return
	}
}
