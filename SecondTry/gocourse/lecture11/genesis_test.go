package bc

import (
	"reflect"
	"testing"
)

func TestGenesis_ToBlock2(t *testing.T) {
	var tests = []struct {
		name    string
		genesis Genesis
		block   Block
	}{
		{
			name: "Simple",
			genesis: Genesis{
				Alloc: map[string]uint64{
					"c": 2,
					"a": 1,
					"b": 3,
				},
			},
			block: Block{Transactions: []Transaction{
				{
					From:   "0",
					To:     "a",
					Amount: 1,
				},
				{
					From:   "0",
					To:     "b",
					Amount: 3,
				},
				{
					From:   "0",
					To:     "c",
					Amount: 2,
				},
			}},
		},
		{
			name: "Empty Alloc",
			genesis: Genesis{
				Alloc: map[string]uint64{},
			},
			block: Block{Transactions: []Transaction{}},
		},
		{
			name: "nil Alloc",
			genesis: Genesis{
				Alloc: nil,
			},
			block: Block{Transactions: []Transaction{}},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			block := test.genesis.ToBlock()
			if !reflect.DeepEqual(block.Transactions, test.block.Transactions) {
				t.Fatal("genesis not equal: ", block.Transactions, " != ", test.block.Transactions)
			}
		})
	}
}

func TestGenesis_BlockSame(t *testing.T) {
	gen1 := Genesis{}
	gen1.Alloc = map[string]uint64{
		"one": 200,
		"two": 50,
	}

	gen2 := Genesis{}
	gen2.Alloc = map[string]uint64{
		"two": 50,
		"one": 200,
	}

	block1 := gen1.ToBlock()
	block2 := gen2.ToBlock()

	if !reflect.DeepEqual(block1, block2) {
		t.Fatalf("genesis blocks difference: \n%v vs %v", block1, block2)
	}
}
