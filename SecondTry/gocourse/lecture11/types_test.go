package bc

import (
	"context"
	"crypto"
	"crypto/ed25519"
	"reflect"
	"testing"
	"time"
)

func TestGenesis_ToBlock(t *testing.T) {
	type fields struct {
		Alloc      map[string]uint64
		Validators []crypto.PublicKey
	}

	tests := []struct {
		name   string
		fields fields
		want   Block
	}{
		{
			"General test",
			fields{map[string]uint64{"aa": 20}, []crypto.PublicKey{ed25519.PublicKey("ok")}},
			Block{
				0,
				0,
				[]Transaction{
					{
						"0",
						"aa",
						20,
						0,
						nil,
						nil,
					},
				},
				"",
				"",
				"",
				nil,
				"",
				nil,
			},
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			g := Genesis{
				Alloc:      tt.fields.Alloc,
				Validators: tt.fields.Validators,
			}

			if got := g.ToBlock(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToBlock() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_connectedPeer_Send(t *testing.T) {
	type fields struct {
		Address string
		In      chan Message
		Out     chan Message
		cancel  context.CancelFunc
	}

	type args struct {
		ctx context.Context
		m   Message
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			"Just send it",
			fields{
				"To",
				make(chan Message),
				make(chan Message),
				nil,
			},
			args{context.Background(), Message{From: "From", Data: "data"}},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			tt.args.ctx, tt.fields.cancel = context.WithCancel(tt.args.ctx)

			cp := connectedPeer{
				Address: tt.fields.Address,
				In:      tt.fields.In,
				Out:     tt.fields.Out,
				cancel:  tt.fields.cancel,
			}

			go cp.Send(tt.args.ctx, tt.args.m)

			select {
			case a := <-tt.fields.Out:
				if !reflect.DeepEqual(a, tt.args.m) {
					t.Fatal("Data are not same")
				}
			case <-time.After(SendTimeout):
				t.Fatal("Timeout")
			}
		})
	}
}
