package bc

import (
	"crypto/ed25519"
	"errors"
	"testing"
)

func TestNode_CheckTransaction(t *testing.T) {
	validatorPublicKey, _, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Fatal(err)
	}

	validatorPublicKey2, _, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name  string
		trans Transaction
		res   error
	}{
		{
			name: "OK",
			trans: Transaction{
				From:      "a",
				To:        "b",
				Amount:    uint64(500),
				Fee:       uint64(10),
				PubKey:    validatorPublicKey,
				Signature: nil,
			},
			res: nil,
		},
		{
			name: "NotHasTo",
			trans: Transaction{
				From:      "a",
				To:        "",
				Amount:    uint64(500),
				Fee:       uint64(10),
				PubKey:    validatorPublicKey,
				Signature: nil,
			},
			res: ErrAccountNotExists,
		},
		{
			name: "NotHasFrom",
			trans: Transaction{
				From:      "",
				To:        "b",
				Amount:    uint64(500),
				Fee:       uint64(10),
				PubKey:    validatorPublicKey,
				Signature: nil,
			},
			res: ErrAccountNotExists,
		},
		{
			name: "NotHasAmount",
			trans: Transaction{
				From:      "a",
				To:        "b",
				Amount:    0,
				Fee:       10,
				PubKey:    validatorPublicKey,
				Signature: nil,
			},
			res: nil,
		},
		{
			name: "NotHasSignature",
			trans: Transaction{
				From:      "a",
				To:        "b",
				Amount:    500,
				Fee:       10,
				PubKey:    validatorPublicKey,
				Signature: []byte{'a'},
			},
			res: nil,
		},
		{
			name: "VerifyFail",
			trans: Transaction{
				From:      "a",
				To:        "b",
				Amount:    500,
				Fee:       10,
				PubKey:    validatorPublicKey2,
				Signature: nil,
			},
			res: nil,
		},
		{
			name: "NotSelectPubKey",
			trans: Transaction{
				From:      "a",
				To:        "b",
				Amount:    500,
				Fee:       10,
				PubKey:    nil,
				Signature: nil,
			},
			res: nil,
		},
		{
			name: "NotNotHasBeSum",
			trans: Transaction{
				From:      "ErrTransNotHasNeedSum",
				To:        "b",
				Amount:    500,
				Fee:       10,
				PubKey:    validatorPublicKey,
				Signature: nil,
			},
			res: ErrAccountNoMoney,
		},
		{
			name: "NotNotHasBeSum2",
			trans: Transaction{
				From:      "a",
				To:        "b",
				Amount:    100_000,
				Fee:       10,
				PubKey:    validatorPublicKey,
				Signature: nil,
			},
			res: ErrAccountNoMoney,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			node := &Node{
				state: make(map[string]uint64),
				key:   ed25519.NewKeyFromSeed([]byte("testtesttesttesttesttesttesttest")),
			}

			node.state["a"] = 1000
			node.state["b"] = 500
			node.state["c"] = 10000
			node.state["d"] = 5000

			if test.trans.Signature == nil {
				test.trans, err = node.SignTransaction(test.trans)

				if err != nil && !errors.Is(err, test.res) {
					t.Fatal(err)
				}
			} else {
				test.trans.Signature = nil
			}
		})
	}
}
