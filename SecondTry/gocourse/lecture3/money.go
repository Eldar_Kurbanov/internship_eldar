package lecture3

import "fmt"

// money represents US dollar amount in terms of cents
type money int64

const AddedNumber = 0.5

// toMoney converts a float64 to money
// e.g. 1.23 to $1.23, 1.345 to $1.35
func toMoney(f float64) money {
	return money((f * 100) + AddedNumber)
}

// float64 converts a money to float64
func (m money) float64() float64 {
	x := float64(m)
	x = x / 100

	return x
}

// multiply safely multiplies a money value by a float64, rounding
// to the nearest cent.
func (m money) multiply(f float64) money {
	x := (float64(m) * f) + AddedNumber

	return money(x)
}

// String returns a formatted money value
func (m money) String() string {
	x := float64(m)
	x = x / 100

	return fmt.Sprintf("%.2f", x)
}
