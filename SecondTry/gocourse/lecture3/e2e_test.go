package lecture3

import (
	"reflect"
	"testing"
)

func TestShop_PlaceOrder_Success(t *testing.T) {
	var shop Shop = NewShop()

	const bananaProduct = "banana"

	err := shop.AddProduct(bananaProduct, &Product{
		"banana",
		10,
		ProductNormal,
	})
	if err != nil {
		t.Fatal(err)
	}

	const userName = "Vasya"

	err = shop.Register(userName, AccountNormal)
	if err != nil {
		t.Fatal(err)
	}

	err = shop.AddBalance(userName, 1000)
	if err != nil {
		t.Fatal(err)
	}

	const bananaBundleName = "bananaBundle"

	err = shop.AddBundle(bananaBundleName, bananaProduct, 50, bananaProduct)
	if err != nil {
		t.Fatal(err)
	}

	res, err := shop.CalculateOrder(userName, &Order{
		Products: []string{bananaProduct, bananaProduct, bananaProduct},
		Bundles:  []string{bananaBundleName},
	})
	if err != nil {
		t.Fatal(err)
	}

	if res != 10+10+10+5+5 {
		t.Fatalf("incorrect order calculation. res = %v", res)
	}

	err = shop.PlaceOrder(userName, &Order{
		Products: []string{bananaProduct, bananaProduct, bananaProduct},
		Bundles:  []string{bananaBundleName},
	})
	if err != nil {
		t.Fatal(err)
	}

	balance, err := shop.Balance(userName)
	if err != nil {
		t.Fatal(err)
	}

	if balance != 960 {
		t.Fatal("incorrect user balance")
	}
}

func TestShop_PlaceOrder_Success_Premium(t *testing.T) {
	var shop Shop = NewShop()

	bananaProduct := "banana"
	err := shop.AddProduct(bananaProduct, &Product{
		"banana",
		10,
		ProductPremium,
	})

	if err != nil {
		t.Fatal(err)
	}

	const userName = "Vasya"

	err = shop.Register(userName, AccountPremium)
	if err != nil {
		t.Fatal(err)
	}

	err = shop.AddBalance(userName, 1000)
	if err != nil {
		t.Fatal(err)
	}

	bananaBundleName := "bananaBundle"

	err = shop.AddBundle(bananaBundleName, bananaProduct, 50, bananaProduct)
	if err != nil {
		t.Fatal(err)
	}

	res, err := shop.CalculateOrder(userName, &Order{
		Products: []string{bananaProduct, bananaProduct, bananaProduct},
		Bundles:  []string{bananaBundleName},
	})
	if err != nil {
		t.Fatal(err)
	}

	if res != 8+8+8+5+5 {
		t.Fatalf("incorrect order calculation. res = %v", res)
	}

	err = shop.PlaceOrder(userName, &Order{
		Products: []string{bananaProduct, bananaProduct, bananaProduct},
		Bundles:  []string{bananaBundleName},
	})
	if err != nil {
		t.Fatal(err)
	}

	balance, err := shop.Balance(userName)
	if err != nil {
		t.Fatal(err)
	}

	if balance != 966 {
		t.Fatal("incorrect user balance")
	}
}

func TestShop_ImportExport_Success(t *testing.T) {
	var shop1, shop2 Shop = NewShop(), NewShop()

	userName := "Vasya"

	err := shop1.Register(userName, AccountNormal)
	if err != nil {
		t.Fatal(err)
	}

	v, err := shop1.Export()
	if err != nil {
		t.Fatal()
	}

	err = shop2.Import(v)
	if err != nil {
		t.Fatal()
	}

	if !reflect.DeepEqual(shop1, shop2) {
		t.Fatal("Shops are not equal")
	}
}
