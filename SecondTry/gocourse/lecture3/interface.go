package lecture3

//Shop - сборный интерфейс магазина. Объект реализующий этот интерфейс будет тестироваться.
//Если реализованы не все методы, оставить заглушки.
type Shop interface {
	ProductModifier
	AccountManager
	BundleManager
	OrderManager
	Importer
	Exporter
}

//ProductModifier - интерфейс для работы со списком продуктов магазина
type ProductModifier interface {
	AddProduct(name string, product *Product) error
	ModifyProduct(name string, product *Product) error
	RemoveProduct(name string) error
}

//AccountManager - интерфейс для работы с пользователями.
type AccountManager interface {
	Register(username string, t AccountType) error
	AddBalance(username string, sum float64) error
	Balance(username string) (float64, error)
	GetAccounts(sort AccountSortType) []*Account
}

//OrderManager - интерфейс для работы заказами. Рассчитать заказ и купить.
type OrderManager interface {
	CreateOrder(username string, products []string, bundles []string) (*Order, error)
	PlaceOrder(username string, order *Order) error
	CalculateOrder(username string, order *Order) (float64, error)
}

//BundleManager - интерфейс для работы с наборами.
type BundleManager interface {
	AddBundle(name string, mainProductName string, discount float64, additionalProductNames ...string) error
	ChangeDiscount(name string, discount float64) error
	GetBundle(name string) (*Bundle, error)
	RemoveBundle(name string) error
}

//Exporter - интерфейс для получения полного состояния магазина.
type Exporter interface {
	Export() ([]byte, error)
}

//Importer - интерфейс для загрузки состояния магазина. Принимает формат который возвращает Exporter.
type Importer interface {
	Import(data []byte) error
}
