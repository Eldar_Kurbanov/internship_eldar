package lecture3

import "encoding/json"

func (s *MyShop) Export() ([]byte, error) {
	return json.Marshal(s)
}

func (s *MyShop) Import(data []byte) error {
	return json.Unmarshal(data, s)
}
