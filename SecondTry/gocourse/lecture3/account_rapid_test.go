package lecture3

import (
	"fmt"
	"testing"

	"pgregory.net/rapid"
)

func GenerateAccountName(t *rapid.T) *rapid.Generator {
	return rapid.String()
}

func GenerateMoney(t *rapid.T) *rapid.Generator {
	return rapid.Float64().Filter(func(f float64) bool {
		return f > 0
	})
}

func GenerateAccountType(t *rapid.T) *rapid.Generator {
	return rapid.Uint8Range(0, 1)
}

func GenerateAccount(t *rapid.T) *rapid.Generator {
	return rapid.Custom(func(t *rapid.T) Account {
		return Account{
			GenerateAccountName(t).Draw(t, "account_name").(string),
			GenerateMoney(t).Draw(t, "account_balance").(float64),
			AccountType(GenerateAccountType(t).Draw(t, "account_type").(uint8)),
		}
	})
}

func GenerateAccountsSlice(t *rapid.T) *rapid.Generator {
	return rapid.SliceOf(GenerateAccount(t))
}

func GenerateAccountsMap(t *rapid.T) map[string]*Account {
	accounts := GenerateAccountsSlice(t).Draw(t, "accounts_slice").([]Account)

	//var accountsMap = map[string]*Account{}
	var accountsMap = make(map[string]*Account)

	for _, a := range accounts {
		a := a
		accountsMap[a.Name] = &a
	}

	for k, v := range accountsMap {
		if k != v.Name {
			fmt.Println("JUST HOW??")
		}
	}

	return accountsMap
}

func TestRapidMyShop_AddBalance(t *testing.T) {
	rapid.Check(t, func(t *rapid.T) {
		accounts := GenerateAccountsMap(t)

		usernameGen := GenerateAccountName(t)

		s := &MyShop{Accounts: accounts}

		userName := usernameGen.Draw(t, "username").(string)
		sum := rapid.Float64().Draw(t, "sum").(float64)

		accountCopyPointer, ok := accounts[userName]
		var accountCopy Account
		if ok {
			accountCopy = *accountCopyPointer
		}

		err := s.AddBalance(
			userName,
			sum,
		)

		_, ok = accounts[userName]
		if ok {
			if err != nil && sum >= 0 {
				t.Fatal(err)
			}
		} else {
			if err == nil {
				t.Fatal(err)
			}
		}


		if err == nil && ok && accountCopy.Balance+sum != accounts[userName].Balance {
			t.Fatalf("money wrong sum. expected %v. got %v", accountCopy.Balance+sum, accounts[userName].Balance)
		}
	})
}
