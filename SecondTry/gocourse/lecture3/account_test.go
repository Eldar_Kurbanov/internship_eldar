package lecture3

import (
	"reflect"
	"testing"
)

func TestMyShop_AddBalance(t *testing.T) {
	type fields struct {
		Accounts map[string]*Account
	}

	type args struct {
		username string
		sum      float64
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Account
	}{
		{
			"Regular add balance",
			fields{
				Accounts: map[string]*Account{"User1": &Account{Name: "User1", Balance: 100}},
			},
			args{username: "User1", sum: 50},
			false,
			map[string]*Account{"User1": &Account{Name: "User1", Balance: 150}},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Accounts: tt.fields.Accounts,
			}

			if err := s.AddBalance(tt.args.username, tt.args.sum); (err != nil) != tt.wantErr {
				t.Errorf("AddBalance() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Accounts, tt.result) {
				t.Fatal(s.Accounts, tt.result)
			}
		})
	}
}

func TestMyShop_Register(t *testing.T) {
	type fields struct {
		Accounts map[string]*Account
	}

	type args struct {
		username string
		t        AccountType
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Account
	}{
		{
			"Register account",
			fields{Accounts: map[string]*Account{}},
			args{username: "User1", t: AccountNormal},
			false,
			map[string]*Account{"User1": &Account{Name: "User1", AccountType: AccountNormal, Balance: 0}},
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Accounts: tt.fields.Accounts,
			}

			if err := s.Register(tt.args.username, tt.args.t); (err != nil) != tt.wantErr {
				t.Errorf("Register() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Accounts, tt.result) {
				t.Fatal()
			}
		})
	}
}

func TestMyShop_Balance(t *testing.T) {
	type fields struct {
		Accounts map[string]*Account
	}

	type args struct {
		username string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    float64
		wantErr bool
	}{
		{
			"Regular balance",
			fields{Accounts: map[string]*Account{"User1": &Account{Name: "User1", Balance: 45}}},
			args{username: "User1"},
			45,
			false,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Accounts: tt.fields.Accounts,
			}

			got, err := s.Balance(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("Balance() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if got != tt.want {
				t.Errorf("Balance() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMyShop_GetAccounts(t *testing.T) {
	aboba := &Account{Name: "Aboba", Balance: 100}
	pupa := &Account{Name: "Pupa", Balance: 50}
	lupa := &Account{Name: "Lupa", Balance: 150}

	type fields struct {
		Accounts map[string]*Account
	}

	type args struct {
		sort AccountSortType
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   []*Account
	}{
		{
			"Sort normal",
			fields{Accounts: map[string]*Account{
				"Aboba": aboba,
				"Pupa":  pupa,
				"Lupa":  lupa,
			}},
			args{SortByName},
			[]*Account{aboba, lupa, pupa},
		},
		{
			"Sort reverse",
			fields{Accounts: map[string]*Account{
				"Aboba": aboba,
				"Pupa":  pupa,
				"Lupa":  lupa,
			}},
			args{SortByNameReverse},
			[]*Account{pupa, lupa, aboba},
		},
		{
			"Sort by money",
			fields{Accounts: map[string]*Account{
				"Aboba": aboba,
				"Pupa":  pupa,
				"Lupa":  lupa,
			}},
			args{SortByBalance},
			[]*Account{pupa, aboba, lupa},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Accounts: tt.fields.Accounts,
			}

			if got := s.GetAccounts(tt.args.sort); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAccounts() = %v, want %v", got, tt.want)
			}
		})
	}
}
