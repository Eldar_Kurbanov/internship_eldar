package lecture3

import (
	"reflect"
	"testing"
)

func TestMyShop_CreateOrder(t *testing.T) {
	type fields struct {
		Products map[string]*Product
		Bundles  map[string]*Bundle
		Accounts map[string]*Account
	}

	type args struct {
		username string
		products []string
		bundles  []string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Order
		wantErr bool
	}{
		{
			"Regular order",
			fields{
				Products: map[string]*Product{"Product": &Product{Name: "Product", Price: 100, Type: ProductNormal}},
				Bundles:  map[string]*Bundle{"Bundle": &Bundle{Name: "Bundle", MainProduct: "Product", AdditionalProducts: []string{"Product", "Product"}, Type: BundleNormal, Discount: 50}},
				Accounts: map[string]*Account{"Account": &Account{Name: "Account", Balance: 200, AccountType: AccountNormal}},
			},
			args{username: "Account", products: []string{"Product", "Product"}, bundles: []string{"Bundle"}},
			&Order{
				Products: []string{"Product", "Product"},
				Bundles:  []string{"Bundle"},
			},
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Products: tt.fields.Products,
				Bundles:  tt.fields.Bundles,
				Accounts: tt.fields.Accounts,
			}

			got, err := s.CreateOrder(tt.args.username, tt.args.products, tt.args.bundles)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateOrder() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateOrder() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMyShop_CalculateOrder(t *testing.T) {
	type fields struct {
		Products map[string]*Product
		Bundles  map[string]*Bundle
		Accounts map[string]*Account
	}

	type args struct {
		username string
		order    *Order
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    float64
		wantErr bool
	}{
		{
			"AccountNormal, ProductNormal, BundleNormal",
			fields{
				Products: map[string]*Product{"Product": &Product{Name: "Product", Price: 100, Type: ProductNormal}},
				Bundles:  map[string]*Bundle{"Bundle": &Bundle{Name: "Bundle", MainProduct: "Product", AdditionalProducts: []string{"Product", "Product"}, Type: BundleNormal, Discount: 0.5}},
				Accounts: map[string]*Account{"Account": &Account{Name: "Account", Balance: 1000, AccountType: AccountNormal}},
			},
			args{username: "Account", order: &Order{Products: []string{"Product"}, Bundles: []string{"Bundle"}}},
			100 + 50 + 50 + 50,
			false,
		},
		{
			"AccountNormal, ProductNormal, Sample and BundleSample",
			fields{
				Products: map[string]*Product{"Product": &Product{Name: "Product", Price: 100, Type: ProductNormal}, "ProductSample": &Product{Name: "ProductSample", Price: 0, Type: ProductSample}},
				Bundles:  map[string]*Bundle{"Bundle": &Bundle{Name: "Bundle", MainProduct: "Product", AdditionalProducts: []string{"ProductSample"}, Type: BundleSample, Discount: 0.5}},
				Accounts: map[string]*Account{"Account": &Account{Name: "Account", Balance: 1000, AccountType: AccountNormal}},
			},
			args{username: "Account", order: &Order{Products: []string{"Product"}, Bundles: []string{"Bundle"}}},
			100 + 50,
			false,
		},
		{
			"AccountNormal, ProductPremium, BundleNormal",
			fields{
				Products: map[string]*Product{"Product": &Product{Name: "Product", Price: 100, Type: ProductPremium}},
				Bundles:  map[string]*Bundle{"Bundle": &Bundle{Name: "Bundle", MainProduct: "Product", AdditionalProducts: []string{"Product", "Product"}, Type: BundleNormal, Discount: 0.5}},
				Accounts: map[string]*Account{"Account": &Account{Name: "Account", Balance: 1000, AccountType: AccountNormal}},
			},
			args{username: "Account", order: &Order{Products: []string{"Product"}, Bundles: []string{"Bundle"}}},
			95 + 50 + 50 + 50,
			false,
		},
		{
			"AccountPremium, ProductPremium, BundleNormal",
			fields{
				Products: map[string]*Product{"Product": &Product{Name: "Product", Price: 100, Type: ProductPremium}},
				Bundles:  map[string]*Bundle{"Bundle": &Bundle{Name: "Bundle", MainProduct: "Product", AdditionalProducts: []string{"Product", "Product"}, Type: BundleNormal, Discount: 0.5}},
				Accounts: map[string]*Account{"Account": &Account{Name: "Account", Balance: 1000, AccountType: AccountPremium}},
			},
			args{username: "Account", order: &Order{Products: []string{"Product"}, Bundles: []string{"Bundle"}}},
			80 + 50 + 50 + 50,
			false,
		},
		{
			"AccountPremium, ProductNormal, BundleNormal",
			fields{
				Products: map[string]*Product{"Product": &Product{Name: "Product", Price: 100, Type: ProductNormal}},
				Bundles:  map[string]*Bundle{"Bundle": &Bundle{Name: "Bundle", MainProduct: "Product", AdditionalProducts: []string{"Product", "Product"}, Type: BundleNormal, Discount: 0.5}},
				Accounts: map[string]*Account{"Account": &Account{Name: "Account", Balance: 1000, AccountType: AccountPremium}},
			},
			args{username: "Account", order: &Order{Products: []string{"Product"}, Bundles: []string{"Bundle"}}},
			150 + 50 + 50 + 50,
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Products: tt.fields.Products,
				Bundles:  tt.fields.Bundles,
				Accounts: tt.fields.Accounts,
			}

			got, err := s.CalculateOrder(tt.args.username, tt.args.order)
			if (err != nil) != tt.wantErr {
				t.Errorf("CalculateOrder() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if got != tt.want {
				t.Errorf("CalculateOrder() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMyShop_PlaceOrder(t *testing.T) {
	type fields struct {
		Products map[string]*Product
		Bundles  map[string]*Bundle
		Accounts map[string]*Account
	}

	type args struct {
		username string
		order    *Order
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Account
	}{
		{
			"Simple place order",
			fields{
				Products: map[string]*Product{"Product": &Product{Name: "Product", Price: 100, Type: ProductNormal}},
				Bundles:  map[string]*Bundle{"Bundle": &Bundle{Name: "Bundle", MainProduct: "Product", AdditionalProducts: []string{"Product", "Product"}, Type: BundleNormal, Discount: 0.5}},
				Accounts: map[string]*Account{"Account": &Account{Name: "Account", Balance: 1000, AccountType: AccountPremium}},
			},
			args{username: "Account", order: &Order{Products: []string{"Product"}, Bundles: []string{"Bundle"}}},
			false,
			map[string]*Account{"Account": &Account{Name: "Account", AccountType: AccountPremium, Balance: 1000 - 150 - 50 - 50 - 50}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Products: tt.fields.Products,
				Bundles:  tt.fields.Bundles,
				Accounts: tt.fields.Accounts,
			}

			if err := s.PlaceOrder(tt.args.username, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("PlaceOrder() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Accounts, tt.result) {
				t.Fatal()
			}
		})
	}
}
