package lecture3

import (
	"reflect"
	"testing"
)

func TestMyShop_AddProduct(t *testing.T) {
	type fields struct {
		Products map[string]*Product
	}

	type args struct {
		name    string
		product *Product
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Product
	}{
		{
			"Simple product",
			fields{map[string]*Product{}},
			args{"Product", &Product{Name: "Product", Price: 100, Type: ProductNormal}},
			false,
			map[string]*Product{"Product": {Name: "Product", Price: 100, Type: ProductNormal}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Products: tt.fields.Products,
			}

			if err := s.AddProduct(tt.args.name, tt.args.product); (err != nil) != tt.wantErr {
				t.Errorf("AddProduct() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMyShop_ModifyProduct(t *testing.T) {
	type fields struct {
		Products map[string]*Product
	}

	type args struct {
		name    string
		product *Product
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Product
	}{
		{
			"Simple modify",
			fields{map[string]*Product{"Product": {Name: "Product", Price: 100, Type: ProductNormal}}},
			args{name: "Product", product: &Product{Name: "Product2", Price: 56, Type: ProductPremium}},
			false,
			map[string]*Product{"Product2": {Name: "Product2", Price: 56, Type: ProductPremium}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Products: tt.fields.Products,
			}

			if err := s.ModifyProduct(tt.args.name, tt.args.product); (err != nil) != tt.wantErr {
				t.Errorf("ModifyProduct() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Products, tt.result) {
				t.Fatal()
			}
		})
	}
}

func TestMyShop_RemoveProduct(t *testing.T) {
	type fields struct {
		Products map[string]*Product
	}

	type args struct {
		name string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Product
	}{
		{
			"Simple remove",
			fields{map[string]*Product{"Product": {Name: "Product", Price: 100, Type: ProductNormal}}},
			args{name: "Product"},
			false,
			map[string]*Product{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Products: tt.fields.Products,
			}

			if err := s.RemoveProduct(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("RemoveProduct() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Products, tt.result) {
				t.Fatal()
			}
		})
	}
}
