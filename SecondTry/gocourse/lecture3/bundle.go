package lecture3

func (s *MyShop) AddBundle(name string, mainProductName string, discount float64, additionalProductNames ...string) error {
	_, exists := s.Bundles[name]
	if exists {
		return ErrBundleExists
	}

	if discount < 1 || discount > 99 {
		return ErrBundleWrongDiscount
	}

	_, exists = s.Products[mainProductName]
	if !exists {
		return ErrProductNotExists
	}

	if len(additionalProductNames) == 0 {
		return ErrNoAdditionalProducts
	}

	bType := BundleUnknownType

	for _, productName := range additionalProductNames {
		product, exists := s.Products[productName]
		if !exists {
			return ErrProductNotExists
		}

		if bType == BundleUnknownType {
			if product.Type == ProductSample {
				bType = BundleSample
			} else {
				bType = BundleNormal
			}
		} else {
			if bType == BundleSample && product.Type != ProductSample {
				return ErrProductWrongType
			} else if bType == BundleNormal && product.Type == ProductSample {
				return ErrProductWrongType
			}
		}
	}

	if bType == BundleSample && len(additionalProductNames) != 1 {
		return ErrBundleTooMuchSamples
	}

	s.Bundles[name] = &Bundle{
		Name:               name,
		MainProduct:        mainProductName,
		AdditionalProducts: additionalProductNames,
		Type:               bType,
		Discount:           discount / 100,
	}

	return nil
}

func (s *MyShop) ChangeDiscount(name string, discount float64) error {
	bundle, exists := s.Bundles[name]
	if !exists {
		return ErrBundleNotExists
	}

	if discount < 1 || discount > 99 {
		return ErrBundleWrongDiscount
	}

	bundle.Discount = discount / 100

	return nil
}

func (s *MyShop) GetBundle(name string) (*Bundle, error) {
	bundle, exists := s.Bundles[name]
	if !exists {
		return nil, ErrBundleNotExists
	}

	return bundle, nil
}

func (s *MyShop) RemoveBundle(name string) error {
	_, exists := s.Bundles[name]
	if !exists {
		return ErrBundleNotExists
	}

	delete(s.Bundles, name)

	return nil
}
