package lecture3

import (
	"fmt"
	"testing"

	"pgregory.net/rapid"
)

type Example struct {
	name string
}

func TestPlayground(t *testing.T) {
	rapid.Check(t, func(t *rapid.T) {
		examplesGen := rapid.Custom(func(t *rapid.T) Example {
			return Example{rapid.String().Draw(t, "example_str").(string)}
		})

		examples := rapid.SliceOf(examplesGen).Draw(t, "examples_slice").([]Example)

		var exampleMap = map[string]Example{}

		for _, e := range examples {
			exampleMap[e.name] = e
		}

		for k, v := range exampleMap {
			if k != v.name {
				fmt.Println("WTF?!")
			}
		}
	})
}
