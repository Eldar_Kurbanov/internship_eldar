package lecture3

import (
	"reflect"
	"testing"
)

func TestMyShop_AddBundle(t *testing.T) {
	type fields struct {
		Products map[string]*Product
		Bundles  map[string]*Bundle
	}

	type args struct {
		name                   string
		mainProductName        string
		discount               float64
		additionalProductNames []string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Bundle
	}{
		{
			"Simple bundle",
			fields{
				Products: map[string]*Product{"Banana": &Product{Name: "Banana", Type: ProductNormal, Price: 100}, "Strawberry": &Product{Name: "Strawberry", Type: ProductNormal, Price: 40}},
				Bundles:  map[string]*Bundle{},
			},
			args{name: "TastyFruits", mainProductName: "Banana", discount: 50, additionalProductNames: []string{"Strawberry", "Strawberry"}},
			false,
			map[string]*Bundle{"TastyFruits": &Bundle{Name: "TastyFruits", MainProduct: "Banana", AdditionalProducts: []string{"Strawberry", "Strawberry"}, Type: BundleNormal, Discount: 0.5}},
		},
		{
			"Sample bundle",
			fields{
				Products: map[string]*Product{"Banana": &Product{Name: "Banana", Type: ProductNormal, Price: 100}, "Strawberry": &Product{Name: "Strawberry", Type: ProductSample, Price: 0}},
				Bundles:  map[string]*Bundle{},
			},
			args{name: "TastyFruits", mainProductName: "Banana", discount: 50, additionalProductNames: []string{"Strawberry"}},
			false,
			map[string]*Bundle{"TastyFruits": &Bundle{Name: "TastyFruits", MainProduct: "Banana", AdditionalProducts: []string{"Strawberry"}, Type: BundleSample, Discount: 0.5}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Products: tt.fields.Products,
				Bundles:  tt.fields.Bundles,
			}

			if err := s.AddBundle(tt.args.name, tt.args.mainProductName, tt.args.discount, tt.args.additionalProductNames...); (err != nil) != tt.wantErr {
				t.Errorf("AddBundle() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Bundles, tt.result) {
				t.Fatal()
			}
		})
	}
}

func TestMyShop_ChangeDiscount(t *testing.T) {
	type fields struct {
		Bundles map[string]*Bundle
	}

	type args struct {
		name     string
		discount float64
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Bundle
	}{
		{
			"Regular change discount",
			fields{Bundles: map[string]*Bundle{"TastyFruits": &Bundle{Name: "TastyFruits", MainProduct: "Banana", AdditionalProducts: []string{"Strawberry"}, Type: BundleNormal, Discount: 0.5}}},
			args{name: "TastyFruits", discount: 10},
			false,
			map[string]*Bundle{"TastyFruits": &Bundle{Name: "TastyFruits", MainProduct: "Banana", AdditionalProducts: []string{"Strawberry"}, Type: BundleNormal, Discount: 0.1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Bundles: tt.fields.Bundles,
			}

			if err := s.ChangeDiscount(tt.args.name, tt.args.discount); (err != nil) != tt.wantErr {
				t.Errorf("ChangeDiscount() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Bundles, tt.result) {
				t.Fatal()
			}
		})
	}
}

func TestMyShop_GetBundle(t *testing.T) {
	type fields struct {
		Bundles map[string]*Bundle
	}

	type args struct {
		name string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Bundle
		wantErr bool
	}{
		{
			"Simple bundle",
			fields{Bundles: map[string]*Bundle{"TastyFruits": &Bundle{Name: "TastyFruits", MainProduct: "Banana", AdditionalProducts: []string{"Strawberry"}, Type: BundleNormal, Discount: 0.5}}},
			args{name: "TastyFruits"},
			&Bundle{Name: "TastyFruits", MainProduct: "Banana", AdditionalProducts: []string{"Strawberry"}, Type: BundleNormal, Discount: 0.5},
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Bundles: tt.fields.Bundles,
			}

			got, err := s.GetBundle(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetBundle() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetBundle() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMyShop_RemoveBundle(t *testing.T) {
	type fields struct {
		Bundles map[string]*Bundle
	}

	type args struct {
		name string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		result  map[string]*Bundle
	}{
		{
			"Simple case",
			fields{Bundles: map[string]*Bundle{"TastyFruits": &Bundle{Name: "TastyFruits", MainProduct: "Banana", AdditionalProducts: []string{"Strawberry"}, Type: BundleNormal, Discount: 0.5}}},
			args{name: "TastyFruits"},
			false,
			map[string]*Bundle{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MyShop{
				Bundles: tt.fields.Bundles,
			}

			if err := s.RemoveBundle(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("RemoveBundle() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(s.Bundles, tt.result) {
				t.Fatal()
			}
		})
	}
}
