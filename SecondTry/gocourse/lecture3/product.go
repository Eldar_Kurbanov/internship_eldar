package lecture3

func (s *MyShop) AddProduct(name string, product *Product) error {
	_, exists := s.Products[name]
	if exists {
		return ErrProductExists
	}

	if product.Type == ProductSample && product.Price != 0 {
		return ErrProductWrongPrice
	}

	if product.Price <= 0 {
		return ErrProductWrongPrice
	}

	s.Products[name] = product

	return nil
}

func (s *MyShop) ModifyProduct(name string, product *Product) error {
	_, exists := s.Products[name]
	if !exists {
		return ErrProductNotExists
	}

	if name != product.Name {
		delete(s.Products, name)
	}

	if product.Type == ProductSample && product.Price != 0 {
		return ErrProductWrongPrice
	}

	if product.Price <= 0 {
		return ErrProductWrongPrice
	}

	s.Products[product.Name] = product

	return nil
}

func (s *MyShop) RemoveProduct(name string) error {
	_, exists := s.Products[name]
	if !exists {
		return ErrProductNotExists
	}

	delete(s.Products, name)

	return nil
}
