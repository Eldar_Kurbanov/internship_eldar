package lecture3

func (s *MyShop) CreateOrder(username string, products []string, bundles []string) (*Order, error) {
	return &Order{Products: products, Bundles: bundles}, nil
}

func (s *MyShop) PlaceOrder(username string, order *Order) error {
	sum, err := s.CalculateOrder(username, order)
	if err != nil {
		return err
	}

	account, exists := s.Accounts[username]
	if !exists {
		return ErrAccountNotExists
	}

	remainder := account.Balance - sum

	if remainder < 0 {
		return ErrAccountNoEnoughMoney
	}

	account.Balance = remainder

	return nil
}

func (s *MyShop) CalculateOrder(username string, order *Order) (float64, error) {
	account, exists := s.Accounts[username]
	if !exists {
		return 0, ErrAccountNotExists
	}

	var sum float64 = 0

	for _, p := range order.Products {
		product, exists := s.Products[p]
		if !exists {
			return 0, ErrProductNotExists
		}

		var k float64

		switch account.AccountType {
		case AccountNormal:
			switch product.Type {
			case ProductNormal:
				k = 1
			case ProductPremium:
				k = 0.95
			default:
				return 0, ErrProductWrongType
			}
		case AccountPremium:
			switch product.Type {
			case ProductNormal:
				k = 1.5
			case ProductPremium:
				k = 0.8
			default:
				return 0, ErrProductWrongType
			}
		default:
			return 0, ErrBundleWrongType
		}

		sum += toMoney(product.Price).multiply(k).float64()
	}

	for _, b := range order.Bundles {
		bundle, exists := s.Bundles[b]
		if !exists {
			return 0, ErrBundleNotExists
		}

		mainPr, exists := s.Products[bundle.MainProduct]
		if !exists {
			return 0, ErrProductNotExists
		}

		sum += toMoney(mainPr.Price).multiply(bundle.Discount).float64()

		for _, ap := range bundle.AdditionalProducts {
			product, exists := s.Products[ap]
			if !exists {
				return 0, ErrProductNotExists
			}

			switch bundle.Type {
			case BundleNormal:
				if !(product.Type == ProductNormal || product.Type == ProductPremium) {
					return 0, ErrProductWrongType
				}

				sum += toMoney(product.Price).multiply(bundle.Discount).float64()
			case BundleSample:
				if product.Type != ProductSample {
					return 0, ErrProductWrongType
				}
			default:
				return 0, ErrBundleWrongType
			}
		}
	}

	return sum, nil
}
