package lecture3

import "internship_eldar/SecondTry/myerror"

const (
	ProductNormal ProductType = iota
	ProductPremium
	ProductSample

	BundleNormal BundleType = iota
	BundleSample
	BundleUnknownType

	AccountNormal AccountType = iota
	AccountPremium

	SortByName AccountSortType = iota
	SortByNameReverse
	SortByBalance

	ErrAccountNotExists     = myerror.MyError("AccountNotExists")
	ErrProductNotExists     = myerror.MyError("ProductNotExists")
	ErrBundleNotExists      = myerror.MyError("BundleNotExists")
	ErrProductWrongType     = myerror.MyError("ProductWrongType")
	ErrBundleWrongType      = myerror.MyError("BundleWrongType")
	ErrAccountNoEnoughMoney = myerror.MyError("AccountNoEnoughMoney")
	ErrAccountExists        = myerror.MyError("AccountExists")
	ErrNegativeMoney        = myerror.MyError("NegativeMoney")
	ErrBundleExists         = myerror.MyError("BundleExists")
	ErrNoAdditionalProducts = myerror.MyError("BundleNoAdditionalProducts")
	ErrBundleWrongDiscount  = myerror.MyError("BundleWrongDiscount")
	ErrBundleTooMuchSamples = myerror.MyError("BundleTooMuchSamples")
	ErrProductExists        = myerror.MyError("ProductExists")
	ErrProductWrongPrice    = myerror.MyError("ProductWrongPrice")
)

//ProductType one of ProductNormal/ProductPremium/ProductSample
type ProductType uint8
type BundleType uint8
type AccountType uint8
type AccountSortType uint8

type Product struct {
	Name  string      `json:"name"`
	Price float64     `json:"price"`
	Type  ProductType `json:"type"`
}

type Order struct {
	Products []string `json:"products"`
	Bundles  []string `json:"bundles"`
}

type Bundle struct {
	Name               string     `json:"name"`
	MainProduct        string     `json:"main_product"`
	AdditionalProducts []string   `json:"additional_products"`
	Type               BundleType `json:"type"`
	Discount           float64    `json:"discount"`
}

type Account struct {
	Name        string  `json:"name"`
	Balance     float64 `json:"balance"`
	AccountType `json:"account_type"`
}

type MyShop struct {
	Shop
	Products map[string]*Product `json:"products"`
	Bundles  map[string]*Bundle  `json:"bundles"`
	Accounts map[string]*Account `json:"accounts"`
}

func NewShop() *MyShop {
	return &MyShop{
		Products: make(map[string]*Product),
		Bundles:  make(map[string]*Bundle),
		Accounts: make(map[string]*Account),
	}
}
