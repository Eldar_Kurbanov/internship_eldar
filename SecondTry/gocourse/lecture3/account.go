package lecture3

import (
	"sort"
)

func (s *MyShop) Register(username string, t AccountType) error {
	_, exists := s.Accounts[username]
	if exists {
		return ErrAccountExists
	}

	s.Accounts[username] = &Account{
		Name:        username,
		Balance:     0,
		AccountType: t,
	}

	return nil
}

func (s *MyShop) AddBalance(username string, sum float64) error {
	if sum < 0 {
		return ErrNegativeMoney
	}

	account, exists := s.Accounts[username]
	if !exists {
		return ErrAccountNotExists
	}

	account.Balance += sum

	return nil
}

func (s *MyShop) Balance(username string) (float64, error) {
	account, exists := s.Accounts[username]
	if !exists {
		return 0, ErrAccountNotExists
	}

	return account.Balance, nil
}

func (s *MyShop) GetAccounts(sort AccountSortType) []*Account {
	switch sort {
	case SortByName:
		return s.sortNormal()
	case SortByNameReverse:
		return s.sortReverse()
	case SortByBalance:
		return s.sortByBalance()
	default:
		return nil
	}
}

func (s *MyShop) sortNormal() []*Account {
	keys := make([]string, 0, len(s.Accounts))
	for k := range s.Accounts {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	result := make([]*Account, len(keys))

	for i, k := range keys {
		result[i] = s.Accounts[k]
	}

	return result
}

func (s *MyShop) sortReverse() []*Account {
	keys := make([]string, 0, len(s.Accounts))
	for k := range s.Accounts {
		keys = append(keys, k)
	}

	sort.Sort(sort.Reverse(sort.StringSlice(keys)))

	result := make([]*Account, len(keys))

	for i, k := range keys {
		result[i] = s.Accounts[k]
	}

	return result
}

func (s *MyShop) sortByBalance() []*Account {
	pl := make(PairList, len(s.Accounts))

	i := 0

	for k, v := range s.Accounts {
		pl[i] = Pair{k, v.Balance}
		i++
	}

	sort.Sort(pl)

	result := make([]*Account, len(s.Accounts))

	var exists bool

	for i, k := range pl {
		result[i], exists = s.Accounts[k.Key]
		if !exists {
			return nil
		}
	}

	return result
}
