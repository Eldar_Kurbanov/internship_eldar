package lecture1

import (
	"sort"
	"strconv"
)

// task 1
func incrementSlice(s []int) {
	for i := range s {
		s[i]++
	}
}

// task 2
func appendNumber5(s []int) []int {
	return append(s, 5)
}

// task 3
func prependNumber5(s []int) []int {
	s = append(s, 0)
	copy(s[1:], s)
	s[0] = 5

	return s
}

// task 4
func popFromSlice(s *[]int) int {
	last := len(*s) - 1
	lastNum := (*s)[last]
	*s = (*s)[:last]

	return lastNum
}

// task 5
func bPopFromSlice(s *[]int) int {
	firstNum := (*s)[0]
	*s = (*s)[1:]

	return firstNum
}

// task 6
func cPopFromSlice(s *[]int, i int) int {
	num := (*s)[i]
	*s = append((*s)[:i], (*s)[i+1:]...)

	return num
}

// task 7
func uniteSlices(s1, s2 []int) []int {
	return append(s1, s2...)
}

// task 8
func complementSlices(s1, s2 []int) []int {
	for _, v := range s2 {
		n := len(s1)
		for i := 0; i < n; i++ {
			if s1[i] == v {
				cPopFromSlice(&s1, i)
				n--
				i--
			}
		}
	}

	return s1
}

// task 9
func shiftSliceL(s []int) []int {
	if s == nil {
		return nil
	}

	if len(s) == 0 {
		return s
	}

	ls := len(s)
	newS := make([]int, ls)
	newS[ls-1] = s[0]

	for i := 0; i < ls-1; i++ {
		newS[i] = s[i+1]
	}

	return newS
}

// task 10
func shiftSliceIterL(s []int, n int) []int {
	for i := 0; i < n; i++ {
		s = shiftSliceL(s)
	}

	return s
}

// task 11
func shiftSliceR(s []int) []int {
	if s == nil {
		return nil
	}

	if len(s) == 0 {
		return s
	}

	ls := len(s)
	newS := make([]int, ls)
	newS[0] = s[ls-1]

	for i := 1; i < ls; i++ {
		newS[i] = s[i-1]
	}

	return newS
}

// task 12
func shiftSliceIterR(s []int, n int) []int {
	for i := 0; i < n; i++ {
		s = shiftSliceR(s)
	}

	return s
}

// task 13
func copySlice(s []int) []int {
	r := make([]int, len(s))
	copy(r, s)

	return r
}

// task 14
func swapEvenOddSlice(s []int) []int {
	if len(s) == 1 {
		return s
	}

	newS := make([]int, len(s))
	for i := 1; i < len(s); i += 2 {
		newS[i] = s[i-1]
		newS[i-1] = s[i]
	}

	if len(s)%2 != 0 {
		newS[len(s)-1] = s[len(s)-1]
	}

	return newS
}

func lexOrder(in []int) []int {
	n := len(in)
	first, last, k := 1, n, n

	if n == 0 {
		return in
	}

	if n < 1 {
		first, last, k = n, 1, 2-n
	}

	s := make([]string, k)

	for i := first; i < last; i++ {
		s[i-first] = strconv.Itoa(in[i])
	}

	sort.Strings(s)

	i := make([]int, k)
	for j := 0; j < k; j++ {
		i[j], _ = strconv.Atoi(s[j])
	}

	return i
}

// task 15
func sortSlice(s []int) ([]int, []int, []int) {
	if s == nil {
		return nil, nil, nil
	}

	general := copySlice(s)
	sort.Ints(general)

	reverse := copySlice(s)
	sort.Sort(sort.Reverse(sort.IntSlice(reverse)))

	lex := lexOrder(s)

	return general, reverse, lex
}
