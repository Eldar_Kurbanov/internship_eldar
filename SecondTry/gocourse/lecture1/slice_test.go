package lecture1

import (
	"reflect"
	"testing"
)

// task 1
func TestIncrementSlice(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, 1}},
			[]int{1, 2},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if incrementSlice(tt.args.s); !reflect.DeepEqual(tt.args.s, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 2
func TestAppendNumber5(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, 1}},
			[]int{0, 1, 5},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := appendNumber5(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 3
func TestPrependNumber5(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, 1}},
			[]int{5, 0, 1},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := prependNumber5(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 4
func TestPopFromSlice(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name   string
		args   args
		want   []int
		result int
	}{
		{
			"example",
			args{s: []int{10, 1, 2, 3, 4, 128}},
			[]int{10, 1, 2, 3, 4},
			128,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := popFromSlice(&tt.args.s); !reflect.DeepEqual(tt.args.s, tt.want) || got != tt.result {
				t.Fatal()
			}
		})
	}
}

// task 5
func TestBPopFromSlice(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name   string
		args   args
		want   []int
		result int
	}{
		{
			"example",
			args{s: []int{10, 1, 2, 3, 4, 128}},
			[]int{1, 2, 3, 4, 128},
			10,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := bPopFromSlice(&tt.args.s); !reflect.DeepEqual(tt.args.s, tt.want) || got != tt.result {
				t.Fatal()
			}
		})
	}
}

// task 6
func TestCPopFromSlice(t *testing.T) {
	type args struct {
		s []int
		i int
	}

	tests := []struct {
		name   string
		args   args
		want   []int
		result int
	}{
		{
			"example",
			args{s: []int{10, 1, 2, 265, 4, 128}, i: 3},
			[]int{10, 1, 2, 4, 128},
			265,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := cPopFromSlice(&tt.args.s, tt.args.i); !reflect.DeepEqual(tt.args.s, tt.want) || got != tt.result {
				t.Fatal()
			}
		})
	}
}

// task 7
func TestUniteSlices(t *testing.T) {
	type args struct {
		s1, s2 []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s1: []int{0, -1}, s2: []int{25, 64}},
			[]int{0, -1, 25, 64},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := uniteSlices(tt.args.s1, tt.args.s2); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 8
func TestComplementSlices(t *testing.T) {
	type args struct {
		s1, s2 []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s1: []int{0, -1, 34, 78, 126, 228}, s2: []int{25, 34, 68, 228}},
			[]int{0, -1, 78, 126},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := complementSlices(tt.args.s1, tt.args.s2); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 9
func TestShiftSliceL(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, -1, 34, 78, 126, 228}},
			[]int{-1, 34, 78, 126, 228, 0},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shiftSliceL(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 10
func TestShiftSliceIterL(t *testing.T) {
	type args struct {
		s []int
		n int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, -1, 34, 78, 126, 228}, n: 3},
			[]int{78, 126, 228, 0, -1, 34},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shiftSliceIterL(tt.args.s, tt.args.n); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 11
func TestShiftSliceR(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, -1, 34, 78, 126, 228}},
			[]int{228, 0, -1, 34, 78, 126},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shiftSliceR(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 12
func TestShiftSliceIterR(t *testing.T) {
	type args struct {
		s []int
		n int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, -1, 34, 78, 126, 228}, n: 3},
			[]int{78, 126, 228, 0, -1, 34},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shiftSliceIterR(tt.args.s, tt.args.n); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 13
func TestCopySlice(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"example",
			args{s: []int{0, -1, 34, 78, 126, 228}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.args.s[0] == -100 {
				t.Fatal()
			}

			got := copySlice(tt.args.s)
			got[0] = -100

			if tt.args.s[0] == -100 {
				t.Fatal("This is not a copy")
			}
		})
	}
}

// task 14
func TestSwapEvenOddSlice(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{0, -1, 34, 78, 126, 228}},
			[]int{-1, 0, 78, 34, 228, 126},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := swapEvenOddSlice(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Fatal()
			}
		})
	}
}

// task 15
func TestSortSlice(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name         string
		args         args
		general      []int
		reverse      []int
		lexicography []int
	}{
		{
			"example",
			args{s: []int{0, -1, 34, 228, 78, 126}},
			[]int{-1, 0, 34, 78, 126, 228},
			[]int{228, 126, 78, 34, 0, -1},
			[]int{0, -1, 126, 228, 34, 78},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotG, gotR, gotL := sortSlice(tt.args.s); !reflect.DeepEqual(gotG, tt.general) ||
				!reflect.DeepEqual(gotR, tt.reverse) || !reflect.DeepEqual(gotL, tt.lexicography) {
				t.Fatal()
			}
		})
	}
}
