package lecture1

import (
	"reflect"
	"testing"
)

func Test_calculateWords(t *testing.T) {
	type args struct {
		s string
	}

	tests := []struct {
		name string
		args args
		want map[string]int
	}{
		{
			"example",
			args{s: "a ab abc pc ab kl pc ab"},
			map[string]int{"a": 1, "ab": 3, "abc": 1, "pc": 2, "kl": 1},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := calculateWords(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("calculateWords() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_uniqueElem(t *testing.T) {
	type args struct {
		s []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{s: []int{1, 2, 3, 4, 5, 6, 2, 5}},
			[]int{1, 2, 3, 4, 5, 6},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := uniqueElem(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("uniqueElem() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_similarElements(t *testing.T) {
	type args struct {
		s1 []int
		s2 []int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"example",
			args{
				[]int{1, 2, 3, 4, 5, 6, 7, 8, 9},
				[]int{15, 20, 3, 4, 122, 5, 6, 7, 69, 41},
			},
			[]int{3, 4, 5, 6, 7},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := similarElements(tt.args.s1, tt.args.s2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("similarElements() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_fibonacciMemoization(t *testing.T) {
	fib := fibonacciMemoization()

	type args struct {
		n int
	}

	tests := []struct {
		name string
		args args
		want int64
	}{
		{
			"example",
			args{4},
			3,
		},
		{
			"example",
			args{2},
			1,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			got, err := fib(tt.args.n)
			if err != nil {
				t.Fatal(err)
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("fibonacciMemoization() = %v, want %v", got, tt.want)
			}
		})
	}
}
