package lecture1

import (
	"errors"
	"internship_eldar/SecondTry/gotour/moretypes"
	"sort"
	"strings"
)

// task 1
func calculateWords(s string) map[string]int {
	result := make(map[string]int)

	words := strings.Fields(s)
	for _, word := range words {
		result[word]++
	}

	return result
}

func sliceToSet(s []int) map[int]bool {
	set := make(map[int]bool)

	for _, v := range s {
		set[v] = true
	}

	return set
}

func setToSlice(set map[int]bool) []int {
	result := make([]int, 0, len(set))

	for k := range set {
		result = append(result, k)
	}

	return result
}

// task 2
func uniqueElem(s []int) []int {
	set := sliceToSet(s)

	result := setToSlice(set)
	sort.Ints(result)

	return result
}

// task 3
func similarElements(s1 []int, s2 []int) []int {
	set1 := sliceToSet(s1)
	set2 := sliceToSet(s2)

	similarElements := make(map[int]bool)

	for k := range set1 {
		if set2[k] {
			similarElements[k] = true
		}
	}

	result := setToSlice(similarElements)
	sort.Ints(result)

	return result
}

var ErrOverflow = errors.New("uint64 overflow")

// task 4
func fibonacciMemoization() func(int) (int64, error) {
	memory := make(map[int]int64)
	fib := moretypes.Fibonacci()

	return func(n int) (int64, error) {
		for i := len(memory); i <= n; i++ {
			memory[i] = fib()
			if memory[i] < memory[i-1] {
				return 0, ErrOverflow
			}
		}

		return memory[n], nil
	}
}
