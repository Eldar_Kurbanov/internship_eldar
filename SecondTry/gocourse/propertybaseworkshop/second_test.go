package propertybaseworkshop

import (
	"fmt"
	"testing"

	"pgregory.net/rapid"
)

type point struct {
	x int
	y int
}

type circle struct {
	center point
	radius int
}

type line struct {
	a point
	b point
}

type shape interface {
	Print()
}

func (c circle) Print() {
	fmt.Printf("center: %v, radius: %v\n", c.center, c.radius)
}

func (l line) Print() {
	fmt.Printf("a: %v, b: %v\n", l.a, l.b)
}

func GeneratePoint() *rapid.Generator {
	return rapid.Custom(func(t *rapid.T) point {
		return point{
			x: rapid.Int().Draw(t, "point_x").(int),
			y: rapid.Int().Draw(t, "point_y").(int),
		}
	})
}

func GenerateCircle() *rapid.Generator {
	return rapid.Custom(func(t *rapid.T) circle {
		return circle{
			center: GeneratePoint().Draw(t, "center").(point),
			radius: rapid.Int().Draw(t, "radius").(int),
		}
	})
}

func GenerateLine() *rapid.Generator {
	return rapid.Custom(func(t *rapid.T) line {
		return line{
			a: GeneratePoint().Draw(t, "point_a").(point),
			b: GeneratePoint().Draw(t, "point_b").(point),
		}
	})
}

func GenerateShape() *rapid.Generator {
	return rapid.Custom(func(t *rapid.T) shape {
		return rapid.OneOf(GenerateCircle(), GenerateLine()).Draw(t, "shape").(shape)
	})
}

func TestCustomStruct(t *testing.T) {
	rapid.Check(t, func(t *rapid.T) {
		sp := rapid.SliceOf(GenerateShape()).Draw(t, "slice of shapes").([]shape)
		for i := range sp {
			sp[i].Print()
		}
	})
}
