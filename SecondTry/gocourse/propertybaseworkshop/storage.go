package propertybaseworkshop

import "errors"

var (
	ErrNotFound = errors.New("not found")
	ErrOldGen   = errors.New("old gen")
)

type Key []byte

type Object struct {
	Value []byte
	Gen   uint64
}

type KV interface {
	Get(Key) (*Object, error)
	Set(Key, *Object) error
	Close() error
}
