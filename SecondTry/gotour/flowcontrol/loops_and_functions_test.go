package flowcontrol

import (
	"math"
	"testing"
)

func TestSqrt(t *testing.T) {
	type args struct {
		x float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			"two",
			args{x: 2},
			1.41,
		},
		{
			"one",
			args{x: 1},
			1,
		},
		{
			"zero",
			args{x: 0},
			0,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := math.Round(Sqrt(tt.args.x)*100) / 100; got != tt.want {
				t.Errorf("Sqrt() = %v, want %v", got, tt.want)
			}
		})
	}
}
