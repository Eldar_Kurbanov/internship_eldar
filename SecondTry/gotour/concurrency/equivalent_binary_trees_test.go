package concurrency

import (
	"fmt"
	"testing"

	"golang.org/x/tour/tree"
)

func TestWalk(t *testing.T) {
	type args struct {
		t  *tree.Tree
		ch chan int
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"tree",
			args{ch: make(chan int), t: tree.New(1)},
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			go Walk(tt.args.t, tt.args.ch)

			for i := range tt.args.ch {
				fmt.Println(i)
			}
		})
	}
}

func TestSame(t *testing.T) {
	type args struct {
		t1 *tree.Tree
		t2 *tree.Tree
	}

	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Same trees",
			args{t1: tree.New(1), t2: tree.New(1)},
			true,
		},
		{
			"Not same trees",
			args{t1: tree.New(1), t2: tree.New(2)},
			false,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if got := Same(tt.args.t1, tt.args.t2); got != tt.want {
				t.Errorf("Same() = %v, want %v", got, tt.want)
			}
		})
	}
}
