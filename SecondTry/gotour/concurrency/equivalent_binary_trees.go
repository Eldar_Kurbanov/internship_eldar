package concurrency

import (
	"golang.org/x/tour/tree"

	"reflect"
	"sort"
)

func walk(t *tree.Tree, ch chan int) {
	ch <- t.Value
	if t.Left != nil {
		walk(t.Left, ch)
	}

	if t.Right != nil {
		walk(t.Right, ch)
	}
}

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
	walk(t, ch)
	close(ch)
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
	arr1 := make([]int, 0)
	arr2 := make([]int, 0)

	ch1 := make(chan int)
	ch2 := make(chan int)

	go Walk(t1, ch1)
	go Walk(t2, ch2)

	for i := range ch1 {
		arr1 = append(arr1, i)
	}

	for i := range ch2 {
		arr2 = append(arr2, i)
	}

	sort.Ints(arr1)
	sort.Ints(arr2)

	return reflect.DeepEqual(arr1, arr2)
}
