package concurrency

import (
	"fmt"
	"internship_eldar/SecondTry/myerror"
	"sync"
)

var ErrNotFound = myerror.MyError("Not found")

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

func Crawl(url string, depth int, fetcher Fetcher) {
	cache := &fetchResults{results: make(map[string]*fakeResult)}

	var wg sync.WaitGroup

	wg.Add(1)
	crawl(url, depth, fetcher, cache, &wg)
	wg.Wait()
}

func crawl(url string, depth int, fetcher Fetcher, cache *fetchResults, wgIn *sync.WaitGroup) {
	defer wgIn.Done()

	if depth <= 0 {
		return
	}

	var (
		body string
		urls []string
		err  error
	)

	cache.Lock()

	res, ok := cache.results[url]
	if ok {
		body, urls = res.body, res.urls
		fmt.Printf("found in cache: %s %q\n", url, body)
	} else {
		body, urls, err = fetcher.Fetch(url)
		cache.results[url] = &fakeResult{body: body, urls: urls}

		if err != nil {
			fmt.Println(err)
			cache.Unlock()
			return
		}

		fmt.Printf("found: %s %q\n", url, body)
	}
	cache.Unlock()

	var wg sync.WaitGroup

	wg.Add(len(urls))

	for _, u := range urls {
		go crawl(u, depth-1, fetcher, cache, &wg)
	}

	wg.Wait()
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func CrawlBad(url string, depth int, fetcher Fetcher) {
	// TODO: Fetch URLs in parallel.
	// TODO: Don't fetch the same URL twice.
	// This implementation doesn't do either:
	if depth <= 0 {
		return
	}

	body, urls, err := fetcher.Fetch(url)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("found: %s %q\n", url, body)

	for _, u := range urls {
		CrawlBad(u, depth-1, fetcher)
	}
}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fetchResults struct {
	sync.Mutex
	results map[string]*fakeResult
}

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}

	return "", nil, fmt.Errorf("%w: %s", ErrNotFound, url)
}

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"https://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"https://golang.org/pkg/",
			"https://golang.org/cmd/",
		},
	},
	"https://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"https://golang.org/",
			"https://golang.org/cmd/",
			"https://golang.org/pkg/fmt/",
			"https://golang.org/pkg/os/",
		},
	},
	"https://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
	"https://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
}
