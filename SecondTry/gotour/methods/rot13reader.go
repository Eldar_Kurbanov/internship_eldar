package methods

import (
	"internship_eldar/SecondTry/myerror"
	"io"
)

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz! "
const rot13 = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm! "

const ErrNoCharInAlphabet = myerror.MyError("No character in alphabet")

type rot13Reader struct {
	r io.Reader
}

func (r rot13Reader) Read(b []byte) (int, error) {
	n, err := r.r.Read(b)
	if err != nil {
		return 0, err
	}

	for i := 0; i < n; i++ {
		index, err := getIndexInAlphabet(int(b[i]))
		if err != nil {
			return 0, err
		}

		b[i] = rot13[index]
	}

	return len(b), nil
}

func getIndexInAlphabet(u int) (int, error) {
	for i, v := range alphabet {
		if int(v) == u {
			return i, nil
		}
	}

	return 0, ErrNoCharInAlphabet
}
