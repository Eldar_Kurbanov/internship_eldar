package methods

import (
	"errors"
	"testing"
)

func TestSqrt(t *testing.T) {
	type args struct {
		x float64
	}

	tests := []struct {
		name string
		args args
		want float64
		err  error
	}{
		{
			"no error",
			args{x: 4},
			2,
			nil,
		},
		{
			"error",
			args{x: -4},
			0,
			ErrNegativeSqrt(-4),
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			got, err := Sqrt(tt.args.x)
			if !errors.Is(err, tt.err) {
				t.Errorf("Sqrt() error = %v, wantErr %v", err, tt.err)
				return
			}

			if got != tt.want {
				t.Errorf("Sqrt() got = %v, want %v", got, tt.want)
			}
		})
	}
}
