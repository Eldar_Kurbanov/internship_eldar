package methods

import (
	"io"
	"os"
	"strings"
	"testing"
)

func Test_rot13Reader_Read(t *testing.T) {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}

	_, err := io.Copy(os.Stdout, &r)
	if err != nil {
		t.Fatal()
	}
}
