package methods

import "testing"

func TestIPAddr_String(t *testing.T) {
	tests := []struct {
		name string
		ip   IPAddr
		want string
	}{
		{
			"loopback",
			IPAddr{127, 0, 0, 1},
			"127.0.0.1",
		},
		{
			"googleDNS",
			IPAddr{8, 8, 8, 8},
			"8.8.8.8",
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ip.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
