package methods

import (
	"image"
	"image/color"
)

type Image struct {
	w int
	h int
}

func (i Image) ColorModel() color.Model {
	return color.RGBAModel
}

func (i Image) Bounds() image.Rectangle {
	return image.Rect(0, 0, i.w, i.h)
}

func (i Image) At(x, y int) color.Color {
	return color.RGBA{R: uint8(x ^ y), G: uint8(x ^ y), B: 255, A: 255}
}
