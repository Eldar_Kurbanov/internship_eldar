package methods

import (
	"golang.org/x/tour/pic"

	"testing"
)

func TestImages(t *testing.T) {
	m := Image{h: 800, w: 1200}
	pic.ShowImage(m)
}
