package moretypes

import (
	"testing"
)

func Test_fibonacci(t *testing.T) {
	tests := []struct {
		name string
		iter int
		want int
	}{
		{
			"one",
			1,
			0,
		},
		{
			"two",
			2,
			1,
		},
		{
			"three",
			3,
			1,
		},
		{
			"four",
			4,
			2,
		},
		{
			"five",
			5,
			3,
		},
		{
			"six",
			6,
			5,
		},
		{
			"seven",
			7,
			8,
		},
		{
			"eight",
			8,
			13,
		},
		{
			"nine",
			9,
			21,
		},
		{
			"ten",
			10,
			34,
		},
		{
			"eleven",
			11,
			55,
		},
		{
			"twelve",
			12,
			89,
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			fib := Fibonacci()

			var got = 1

			for i := 0; i < tt.iter; i++ {
				got = int(fib())
			}

			if got != tt.want {
				t.Errorf("Fibonacci() = %v, want %v", got, tt.want)
			}
		})
	}
}
