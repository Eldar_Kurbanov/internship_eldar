package moretypes

import "strings"

func WordCount(s string) map[string]int {
	resultMap := make(map[string]int)

	words := strings.Fields(s)
	for _, v := range words {
		resultMap[v]++
	}

	return resultMap
}
