package moretypes

func Fibonacci() func() int64 {
	var prev int64 = -1

	var last int64 = 1

	return func() int64 {
		result := prev + last
		prev = last
		last = result

		return result
	}
}
