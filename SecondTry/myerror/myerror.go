package myerror

type MyError string

func (err MyError) Error() string {
	return string(err)
}
