package PicturePipeline

import (
	"testing"
	"time"
)

func TestProcess(t *testing.T) {
	type args struct {
		picturesUrls          []string
		resolutions           []PictureResolution
		needToSaveAspectRatio bool
		outputFormats         []string
	}
	tests := []struct {
		name string
		args args
	}{
		{"test", args{[]string{
			"https://raw.githubusercontent.com/disintegration/imaging/master/testdata/branches.jpg",
		}, []PictureResolution{{800, 600}}, true, []string{
			"jpg",
		}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			picturesPaths, err := Process(tt.args.picturesUrls, tt.args.resolutions, tt.args.needToSaveAspectRatio, tt.args.outputFormats, make(chan struct{}))
			if err != nil {
				t.Error("Process failed!")
			}
			if len(picturesPaths) != 1 {
				t.Error("Not all pictures was processed!")
			}
		})
	}
}

func getSliceUrl(length int) []string {
	result := make([]string, length)
	for i := 0; i < length; i++ {
		result[i] = "https://picsum.photos/1920/1080"
	}
	return result
}

func TestLoremPicsum(t *testing.T) {
	type args struct {
		picturesUrls          []string
		resolutions           []PictureResolution
		needToSaveAspectRatio bool
		outputFormats         []string
	}
	tests := []struct {
		name string
		args args
	}{
		{"test", args{getSliceUrl(10), []PictureResolution{
			{800, 600}, {1920, 1080}, {1280, 720},
		}, true, []string{
			"jpg", "png", "jpeg",
		}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			picturesPaths, err := Process(tt.args.picturesUrls, tt.args.resolutions, tt.args.needToSaveAspectRatio, tt.args.outputFormats, make(chan struct{}))
			if err != nil {
				t.Error("Process failed!")
			}
			if len(picturesPaths) != 90 {
				t.Error("Not all pictures was processed!")
			}
		})
	}
}

func TestLoremPicsumWithTimer(t *testing.T) {
	type args struct {
		picturesUrls          []string
		resolutions           []PictureResolution
		needToSaveAspectRatio bool
		outputFormats         []string
	}
	tests := []struct {
		name string
		args args
	}{
		{"test", args{getSliceUrl(10), []PictureResolution{
			{800, 600}, {1920, 1080}, {1280, 720},
		}, true, []string{
			"jpg", "png", "jpeg",
		}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			stopChan := make(chan struct{})
			go func() {
				time.Sleep(time.Second)
				stopChan <- struct{}{}
			}()
			picturesPaths, err := Process(tt.args.picturesUrls, tt.args.resolutions, tt.args.needToSaveAspectRatio, tt.args.outputFormats, stopChan)
			if err.Error() != "ProcessInterrupted" {
				t.Error("No interrupt error on stop!")
			}
			if len(picturesPaths) == 90 {
				t.Error("All pictures are passed, stop doesn't work!")
			}
		})
	}
}
