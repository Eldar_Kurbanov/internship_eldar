package PicturePipeline

import (
	"bytes"
	"errors"
	"github.com/disintegration/imaging"
	"github.com/tevino/abool"
	"image"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"sync"
	"sync/atomic"
)

const BufferSize = 10

type PictureResolution struct {
	width  uint
	height uint
}

type AllChannels struct {
	errorChannel        chan error
	picturesUrlChannel  chan string
	picturesPathChannel chan string
	//finishProgramErrorChannel chan error
	isErrorHappens       *abool.AtomicBool
	picturesChannel      chan image.Image
	savedPicturesPaths   chan string
	allGoroutinesAreDone *sync.WaitGroup
	errorOnce            *sync.Once

	downloadPictureCompleteChannel chan struct{}
	resizePictureCompleteChannel   chan struct{}
	savePictureCompleteChannel     chan struct{}
}

func getGID() uint64 {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64)
	return n
}

/*func errorHandler(channels AllChannels) {
	//err := <- channels.errorChannel
	//log.Println(err)
	//channels.finishProgramErrorChannel <- err
	channels.isErrorHappens.Set()
}*/

func errorHappens(channels AllChannels, err error) {
	channels.errorOnce.Do(func() {
		log.Println(err)
		channels.isErrorHappens.Set()
		channels.errorChannel <- err
		//close(channels.savedPicturesPaths)
		channels.allGoroutinesAreDone.Done()
	})
}

// https://golangcode.com/download-a-file-from-a-url/
func DownloadFile(file *os.File, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(file, resp.Body)
	return err
}

func downloadPicture(channels AllChannels) {
	tempDirectory, err := ioutil.TempDir("", "imgProcessing-")
	if err != nil {
		//channels.errorChannel <- err
		//close(channels.closeChannel)
		errorHappens(channels, err)
		close(channels.picturesPathChannel)
		return
	}
	defer func() { channels.downloadPictureCompleteChannel <- struct{}{} }()
	for pictureUrl := range channels.picturesUrlChannel {
		if channels.isErrorHappens.IsSet() {
			return
		}
		pictureFile, err := ioutil.TempFile(tempDirectory, "picture-")
		if err != nil {
			/*channels.errorChannel <- err
			close(channels.closeChannel)*/
			errorHappens(channels, err)
			//close(channels.picturesPathChannel)
			return
		}
		defer pictureFile.Close()
		err = DownloadFile(pictureFile, pictureUrl)
		if err != nil {
			/*channels.errorChannel <- err
			_ = pictureFile.Close()
			close(channels.closeChannel)
			close(channels.picturesPathChannel)*/
			errorHappens(channels, err)
			return
		}
		/*err = pictureFile.Close()
		if err != nil {
			/*channels.errorChannel <- err
			close(channels.closeChannel)
			close(channels.picturesPathChannel)
			errorHappens(channels, err)
			return
		}*/
		channels.picturesPathChannel <- pictureFile.Name()
	}
}

func resizePicture(channels AllChannels, resolutions []PictureResolution, needToSaveAspectRatio bool) {
	//resizeCounter := make(chan struct{}, len(resolutions))
	defer func() { channels.resizePictureCompleteChannel <- struct{}{} }()
	var resizeWaitGroup sync.WaitGroup
	for picturePath := range channels.picturesPathChannel {
		picture, err := imaging.Open(picturePath)
		if err != nil {
			/*channels.errorChannel <- err
			close(channels.closeChannel)
			close(channels.picturesChannel)*/
			break
		}
		resizeWaitGroup.Add(len(resolutions))
		for _, resolution := range resolutions {
			resolution := resolution
			/*select {
			case <- channels.closeChannel:
				close(channels.picturesChannel)
				return
			default:

			}*/
			if channels.isErrorHappens.IsSet() {
				resizeWaitGroup.Done()
				break
			}
			go func() {
				if needToSaveAspectRatio {
					picture = imaging.Resize(picture, int(resolution.width), 0, imaging.Lanczos)
				} else {
					picture = imaging.Resize(picture, int(resolution.width), int(resolution.height), imaging.Lanczos)
				}
				channels.picturesChannel <- picture
				resizeWaitGroup.Done()
			}()
		}
		// Waiting for resize
		//for i := 0; i < len(resolutions); i++ {
		//	_ = <- resizeCounter
		//}
	}
	//close(channels.picturesChannel)
	resizeWaitGroup.Wait()
}

func savePicture(channels AllChannels, outputFormats []string) {
	//saveCounter := make(chan struct{}, len(outputFormats))
	defer func() { channels.savePictureCompleteChannel <- struct{}{} }()
	var saveWaitGroup sync.WaitGroup
	var i uint32
	for picture := range channels.picturesChannel {
		saveWaitGroup.Add(len(outputFormats))
		iLocal := atomic.LoadUint32(&i)
		atomic.AddUint32(&i, 1)
		for _, outputFormat := range outputFormats {
			outputFormat := outputFormat
			/*select {
			case <- channels.closeChannel:
				close(channels.savedPicturesPaths)
				return
			default:

			}*/
			if channels.isErrorHappens.IsSet() {
				saveWaitGroup.Done()
				return
			}
			goroutineId := strconv.Itoa(int(getGID()))
			go func() {
				defer saveWaitGroup.Done()
				picturePath := filepath.Join("output", goroutineId+"-"+strconv.Itoa(int(iLocal))+"."+outputFormat)
				err := imaging.Save(picture, picturePath)
				if err != nil {
					/*channels.errorChannel <- err
					close(channels.closeChannel)
					close(channels.savedPicturesPaths)*/
					//log.Println("kek")
					errorHappens(channels, err)
					//log.Println("kek")
					//saveWaitGroup.Done()
					return
				}
				if channels.isErrorHappens.IsSet() {
					return
				}
				channels.savedPicturesPaths <- picturePath
				//saveCounter <- struct{}{}
				//saveWaitGroup.Done()
			}()
		}
		// Waiting for save goroutines complete
		//for i := 0; i < len(outputFormats); i++ {
		//	_ = <- saveCounter
		//}
	}
	saveWaitGroup.Wait()
	//log.Println("hehe")
	//close(channels.savedPicturesPaths)

}

func Process(picturesUrls []string, resolutions []PictureResolution, needToSaveAspectRatio bool,
	outputFormats []string, itsTimeToStop chan struct{}) ([]string, error) {
	if len(picturesUrls) == 0 {
		return nil, errors.New("NoPicturesForProcess")
	}
	if len(resolutions) == 0 {
		return nil, errors.New("NoResolutionsForProcess")
	}
	if len(outputFormats) == 0 {
		return nil, errors.New("NoOutputFormats")
	}

	var channels AllChannels
	channels.errorChannel = make(chan error, 1)
	//channels.finishProgramErrorChannel = make(chan error)
	channels.isErrorHappens = abool.New()
	channels.picturesUrlChannel = make(chan string, BufferSize)
	channels.picturesPathChannel = make(chan string, BufferSize)
	channels.picturesChannel = make(chan image.Image, BufferSize)
	channels.savedPicturesPaths = make(chan string, BufferSize)
	//channels.closeChannel = make(chan struct{})
	channels.downloadPictureCompleteChannel = make(chan struct{})
	channels.resizePictureCompleteChannel = make(chan struct{})
	channels.savePictureCompleteChannel = make(chan struct{})
	channels.allGoroutinesAreDone = new(sync.WaitGroup)
	channels.errorOnce = new(sync.Once)

	channels.allGoroutinesAreDone.Add(1)

	go func() {
		_ = <-itsTimeToStop
		errorHappens(channels, errors.New("ProcessInterrupted"))
	}()

	//go errorHandler(channels)
	go func() {
		for counter := 0; counter < BufferSize; counter++ {
			_ = <-channels.downloadPictureCompleteChannel
		}
		close(channels.picturesPathChannel)
	}()
	for i := 0; i < BufferSize; i++ {
		go downloadPicture(channels)
	}
	go func() {
		for _, pictureUrl := range picturesUrls {
			/*select {
			case <- channels.closeChannel:
				close(channels.picturesUrlChannel)
				return
			default:

			}*/
			if channels.isErrorHappens.IsNotSet() {
				channels.picturesUrlChannel <- pictureUrl
			}
		}
		close(channels.picturesUrlChannel)
	}()
	go func() {
		for counter := 0; counter < BufferSize; counter++ {
			_ = <-channels.resizePictureCompleteChannel
		}
		close(channels.picturesChannel)
	}()
	for i := 0; i < BufferSize; i++ {
		go resizePicture(channels, resolutions, needToSaveAspectRatio)
	}
	go func() {
		for counter := 0; counter < BufferSize; counter++ {
			_ = <-channels.savePictureCompleteChannel
		}
		close(channels.savedPicturesPaths)
	}()
	for i := 0; i < BufferSize; i++ {
		go savePicture(channels, outputFormats)
	}
	savedPicturesPath := make([]string, 0)
	go func() {
		for savedPicturePath := range channels.savedPicturesPaths {
			savedPicturesPath = append(savedPicturesPath, savedPicturePath)
			//log.Println(savedPicturePath)
		}
		/*select {
		case <- channels.closeChannel:

		default:
			//channels.finishProgramErrorChannel <- errors.New("NoError")
		}*/
		channels.allGoroutinesAreDone.Done()
	}()
	channels.allGoroutinesAreDone.Wait()
	if channels.isErrorHappens.IsSet() {
		return nil, <-channels.errorChannel
	} else {
		return savedPicturesPath, nil
	}
}
